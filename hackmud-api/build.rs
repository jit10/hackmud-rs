extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    if cfg!(feature = "no_steam_auth") {
        return;
    }

    let mut sdk_path = PathBuf::new();
    if let Ok(env) = env::var("STEAMWORKS_SDK") {
        sdk_path = PathBuf::from(env);
        let bindings = bindgen::Builder::default()
            .header(sdk_path.join("public/steam/steam_api.h").to_str().unwrap())
            .whitelist_var("STEAM.*_INTERFACE_VERSION")
            .whitelist_function("(SteamInternal_.*|SteamAPI_GetHSteam.*|SteamAPI_(Init|Shutdown))")
            .trust_clang_mangling(false)
            .clang_args(["-x", "c++"].iter())
            .generate()
            .expect("Unable to generate bindings");

        let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
        bindings
            .write_to_file(out_path.join("bindings.rs"))
            .expect("Couldn't write bindings!");
    }

    let target_arch = env::var("CARGO_CFG_TARGET_ARCH").unwrap();
    let target_os = env::var("CARGO_CFG_TARGET_OS").unwrap();
    let sdk_libpath = if target_os.contains("linux") {
        match target_arch.as_ref() {
            "x86_64" => Some("linux64"),
            "x86" => Some("linux32"),
            _ => None,
        }
    } else if target_os.contains("windows") {
        match target_arch.as_ref() {
            "x86_64" => Some("win64"),
            "x86" => Some(""),
            _ => None,
        }
    } else if target_os.contains("macos") {
        Some("osx32")
    } else {
        None
    };

    if let Some(sdk_libpath) = sdk_libpath {
        if sdk_path.as_os_str().is_empty() {
            panic!(
                "Please specify STEAMWORKS_SDK environment variable to cargo OR specify feature \
                 `no_steam_auth` to disable support for steam authentication."
            );
        }
        println!("cargo:rustc-link-lib=steam_api");
        println!(
            "cargo:rustc-link-search=native={}",
            sdk_path
                .join("redistributable_bin/")
                .join(sdk_libpath)
                .to_str()
                .unwrap()
        );
    } else {
        println!("cargo:rustc-cfg=feature=\"no_steam_auth\"");
    };
}
