#![feature(conservative_impl_trait)]
#![cfg(not(feature = "no_steam_auth"))]
extern crate env_logger;
extern crate failure;
extern crate hackmud_api;
extern crate tokio_core;

use std::path::PathBuf;
use std::process;
use tokio_core::reactor::Core;
use failure::{Fail, ResultExt};
use hackmud_api::steam_auth::Authenticator;

fn run() -> Result<String, impl Fail> {
    let cert_path = std::env::var("DEBUG_CERT_PATH")
        .map(PathBuf::from)
        .or_else(|e| match e {
            std::env::VarError::NotUnicode(s) => Ok(PathBuf::from(s)),
            _ => Err(e),
        })
        .ok();
    let mut core =
        Core::new().with_context(|e| format!("Failed to create tokio event loop: {}", e))?;
    let mut auth_client =
        if let Some(cert_path) = cert_path {
            Authenticator::debug_new(&core.handle(), cert_path)
        } else {
            Authenticator::new(&core.handle())
        }.with_context(|e| format!("Failed to create Authentication client: {}", e))?;

    core.run(auth_client.authenticate())
        .with_context(|e| format!("Failed to authenticate to hackmud server: {}", e))
}

fn main() {
    env_logger::init();
    match run() {
        Ok(token) => {
            println!("{}", token);
            process::exit(0);
        }
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    }
}
