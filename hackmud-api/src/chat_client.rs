use std::str::FromStr;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use futures::{Future, Stream};
use hyper::{Method, Uri, client::{Client as HyperClient, Request as HttpRequest},
            header::{ContentType, UserAgent}};
use hyper_rustls::HttpsConnector;
use serde_json::{from_slice as from_json_slice, to_vec as json_to_vec, Map, Number as JsonNumber,
                 Value as JsonValue};
use tokio_core::reactor::Core;

use config::HACKMUD_SERVER_URI;
use types::*;
use errors::Error;

fn unix_secs_as_float(t: SystemTime) -> f64 {
    let d = t.duration_since(UNIX_EPOCH)
        .unwrap_or_else(|_| Duration::from_secs(0));
    d.as_secs() as f64 + f64::from(d.subsec_nanos()) * 1e-9
}

pub struct Client {
    chat_token: String,
    http_client: HyperClient<HttpsConnector>,
}

impl Client {
    pub fn new<T: AsRef<str>>(chat_pass: T, core: &mut Core) -> Result<Self, Error> {
        let http_client = HyperClient::configure()
            .connector(HttpsConnector::new(4, &core.handle()))
            .build(&core.handle());
        let mut client = Self {
            chat_token: "".into(),
            http_client,
        };
        info!("Requesting chat_pass from server");
        let token = core.run(client.get_chat_token(chat_pass))?;
        if !token.ok {
            Err(Error::RemoteError("Failed to get chat_token".into()))
        } else {
            debug!("Received chat_pass: {}", token.chat_token);
            info!("Chat client initialization successful");
            client.chat_token = token.chat_token;
            Ok(client)
        }
    }

    fn post<T>(
        &self,
        endpoint: &'static str,
        data: &Map<String, JsonValue>,
    ) -> impl Future<Item = T, Error = Error>
    where
        T: ::serde::de::DeserializeOwned + ::std::fmt::Debug,
    {
        let mut req = HttpRequest::new(
            Method::Post,
            Uri::from_str(&format!("{}/{}", HACKMUD_SERVER_URI, endpoint))
                .expect("Failed to construct uri from endpoint"),
        );
        {
            let headers = req.headers_mut();
            headers.set(UserAgent::new("hackmud-rs"));
            headers.set(ContentType::json());
        }
        req.set_body(json_to_vec(&data).expect("Failed to serialize Request to json"));
        if log_enabled!(::log::Level::Trace) {
            trace!(
                "Making http request: {:?} with message: {}",
                req,
                ::serde_json::to_string(&data)
                    .unwrap_or_else(|_| "Failed to display request body".into())
            );
        }
        self.http_client
            .request(req)
            .map_err(Error::HttpError)
            .and_then(|resp| {
                resp.body()
                    .concat2()
                    .map_err(Error::HttpError)
                    .and_then(move |b| {
                        let val = from_json_slice::<T>(&b).map_err(Error::JsonError)?;
                        trace!("Received hackmud json response: {:?}", val);
                        Ok(val)
                    })
            })
    }

    fn get_chat_token<T: AsRef<str>>(
        &self,
        chat_pass: T,
    ) -> impl Future<Item = ChatToken, Error = Error> {
        let mut req = Map::new();
        req.insert("pass".into(), JsonValue::String(chat_pass.as_ref().into()));
        self.post("mobile/get_token.json", &req)
    }

    pub fn get_account_data(&self) -> impl Future<Item = AccountData, Error = Error> {
        let mut req = Map::new();
        req.insert(
            "chat_token".into(),
            JsonValue::String(self.chat_token.clone()),
        );
        self.post("mobile/account_data.json", &req)
    }

    pub fn get_all_chats<V: IntoIterator, F: Into<Option<SystemTime>>>(
        &self,
        user_names: V,
        before: F,
        after: F,
    ) -> impl Future<Item = Chats, Error = Error>
    where
        V::Item: AsRef<str>,
    {
        let mut req = Map::new();
        req.insert(
            "chat_token".into(),
            JsonValue::String(self.chat_token.clone()),
        );
        req.insert(
            "usernames".into(),
            JsonValue::Array(
                user_names
                    .into_iter()
                    .map(|s| JsonValue::String(s.as_ref().to_owned()))
                    .collect(),
            ),
        );
        before.into().map(|t| {
            req.insert(
                "before".into(),
                JsonValue::Number(
                    JsonNumber::from_f64(unix_secs_as_float(t))
                        .expect("Failed to convert system time to float secs"),
                ),
            );
        });
        after.into().map(|t| {
            req.insert(
                "after".into(),
                JsonValue::Number(
                    JsonNumber::from_f64(unix_secs_as_float(t))
                        .expect("Failed to convert system time to float secs"),
                ),
            );
        });
        self.post("mobile/chats.json", &req)
    }

    fn create_chat<U, D, M>(
        &self,
        is_chan: bool,
        user_name: U,
        to: D,
        message: M,
    ) -> impl Future<Item = ChatResponseOk, Error = Error>
    where
        U: AsRef<str>,
        D: AsRef<str>,
        M: AsRef<str>,
    {
        let mut req = Map::new();
        req.insert(
            "chat_token".into(),
            JsonValue::String(self.chat_token.clone()),
        );
        req.insert(
            "username".into(),
            JsonValue::String(user_name.as_ref().into()),
        );
        let k = if is_chan {
            "channel".into()
        } else {
            "tell".into()
        };
        req.insert(k, JsonValue::String(to.as_ref().into()));
        req.insert("msg".into(), JsonValue::String(message.as_ref().into()));
        self.post("mobile/create_chat.json", &req)
    }

    pub fn send_chan_msg<U, C, M>(
        &self,
        user_name: U,
        channel: C,
        message: M,
    ) -> impl Future<Item = ChatResponseOk, Error = Error>
    where
        U: AsRef<str>,
        C: AsRef<str>,
        M: AsRef<str>,
    {
        self.create_chat(true, user_name, channel, message)
    }

    pub fn send_priv_msg<U, D, M>(
        &self,
        user_name: U,
        to_user: D,
        message: M,
    ) -> impl Future<Item = ChatResponseOk, Error = Error>
    where
        U: AsRef<str>,
        D: AsRef<str>,
        M: AsRef<str>,
    {
        self.create_chat(false, user_name, to_user, message)
    }
}
