use std::collections::BTreeMap;
use std::fmt::{Display, Error as FmtError, Formatter};
use serde::{Deserialize, Deserializer, Serializer, de::{Error, MapAccess, Visitor}};
use serde_json::{from_str as from_json_str, to_string as to_json_string, Map, Value as JsonValue};
use percent_encoding::percent_decode;

#[derive(Clone, Serialize, Deserialize, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Version {
    pub major: u32,
    pub minor: u32,
    pub build: u32,
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        write!(f, "{}.{}.{}", self.major, self.minor, self.build)
    }
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
pub(crate) enum Response<T> {
    Success(T),
    Failure(Map<String, JsonValue>),
}

#[derive(Clone, Default, Debug, Deserialize)]
pub(crate) struct AuthResponse {
    pub authentication_token: String,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct UserStatus {
    pub available: bool,
    pub msg: String,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct UserInfo {
    pub max_available: u32,
    pub available: Vec<String>,
    pub max_retired: u32,
    pub retired: Vec<String>,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct RetireResponse {
    pub success: bool,
    pub msg: String,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct AutoComplete {
    #[serde(rename = "userAutos")]
    pub user_autos: Map<String, JsonValue>,
    #[serde(rename = "Count")]
    pub count: u32,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct ClientData {
    pub gui_bgm: u32,
    pub gui_sfx: u32,
    pub gui_bloom: u32,
    pub gui_noise: u32,
    pub gui_scan: u32,
    pub gui_bend: u32,
    pub gui_chats_shell: bool,
    pub gui_chats_chat: bool,
    pub gui_quiet: Vec<String>,
    #[serde(serialize_with = "auto_complete_to_str", deserialize_with = "auto_complete_from_str")]
    pub autocomplete: AutoComplete,
}

fn auto_complete_from_str<'de, D>(deserializer: D) -> Result<AutoComplete, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    from_json_str(&s).map_err(D::Error::custom)
}

fn auto_complete_to_str<S>(
    autos: &AutoComplete,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = to_json_string(autos).unwrap();
    serializer.serialize_str(&s)
}

#[derive(Clone, Default, Debug, Deserialize)]
pub struct RunStatus {
    pub corruption_level: u32,
    pub run_id: String,
    pub success: bool,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Notification {
    pub script_name: String,
    #[serde(rename = "t")]
    pub timestamp: u64,
    #[serde(rename = "id")]
    pub run_id: String,
    #[serde(rename = "data", deserialize_with = "script_result_from_str")]
    pub result: ScriptResult,
}

fn script_result_from_str<'de, D>(deserializer: D) -> Result<ScriptResult, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    from_json_str(&s).map_err(D::Error::custom)
}

#[derive(Clone, Debug)]
pub struct ScriptResult {
    pub success: bool,
    pub corruption_level: u32,
    pub data: ScriptData,
}

#[derive(Clone, Debug)]
pub enum ScriptData {
    NotificationData(Map<String, JsonValue>),
    RetValue(JsonValue),
}

impl<'de> Deserialize<'de> for ScriptResult {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ScriptResultVisitor;
        impl<'de> Visitor<'de> for ScriptResultVisitor {
            type Value = ScriptResult;

            fn expecting(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                formatter.write_str("struct ScriptResult")
            }

            fn visit_map<V>(self, mut map: V) -> Result<ScriptResult, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut success = false;
                let mut corruption_level = 0u32;
                let mut is_notification = false;
                let mut retval = None;
                let mut rest = Map::new();
                let one_of_err = "Shouldn't have `is_notification` set to true and `retval` field";
                while let Some(key) = map.next_key()? {
                    match key {
                        "success" => {
                            success = map.next_value()?;
                        }
                        "corruption_level" => {
                            corruption_level = map.next_value()?;
                        }
                        "is_notification" => {
                            is_notification = map.next_value()?;
                            if is_notification && retval.is_some() {
                                return Err(V::Error::custom(one_of_err));
                            }
                        }
                        "retval" => {
                            if is_notification {
                                return Err(V::Error::custom(one_of_err));
                            }
                            retval = Some(map.next_value()?);
                        }
                        s => {
                            rest.insert(s.into(), map.next_value()?);
                        }
                    }
                }
                let data = if is_notification {
                    ScriptData::NotificationData(rest)
                } else if let Some(r) = retval {
                    ScriptData::RetValue(r)
                } else {
                    let err_str =
                        "missing one of required fields: [\"is_notification\", \"retval\"]";
                    error!("Failed to deserialize json into ScriptResult: {}", err_str);
                    Err(V::Error::custom(err_str))?
                };
                Ok(ScriptResult {
                    success,
                    corruption_level,
                    data,
                })
            }
        }
        const FIELDS: &[&str] = &["success", "corruption_level", "data"];
        deserializer.deserialize_struct("ScriptResult", FIELDS, ScriptResultVisitor)
    }
}

#[derive(Clone, Default, Debug)]
pub struct ScriptProperties {
    pub is_dry: bool,
    pub is_shift: bool,
    pub run_on_upload: bool,
    pub set_visibility: Option<ScriptVisibility>,
}

#[derive(Clone, Debug)]
pub enum ScriptVisibility {
    Private,
    Public,
}

#[derive(Clone, Debug, Serialize)]
pub struct ScriptCharLimit {
    pub used: u32,
    pub max: u32,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UpdateResult {
    pub success: bool,
    pub security_level: u32,
    #[serde(deserialize_with = "parse_script_limit", serialize_with = "script_limit_string")]
    pub length: ScriptCharLimit,
    #[serde(rename = "is_public", serialize_with = "script_visibility_to_bool",
            deserialize_with = "bool_to_script_visibility")]
    pub visibility: ScriptVisibility,
    #[serde(default)]
    pub dry: bool,
    pub sector: Option<String>,
}

fn parse_script_limit<'de, D>(deserializer: D) -> Result<ScriptCharLimit, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    let v = s.split('/')
        .map(|s| s.parse())
        .collect::<Result<Vec<u32>, _>>()
        .map_err(D::Error::custom)?;
    if v.len() != 2 {
        return Err(D::Error::custom(
            "Failed to parse: expected string `used/max`",
        ));
    }
    Ok(ScriptCharLimit {
        used: v[0],
        max: v[1],
    })
}

fn script_limit_string<S>(
    limit: &ScriptCharLimit,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&format!("{}/{}", limit.used, limit.max))
}

fn bool_to_script_visibility<'de, D>(deserializer: D) -> Result<ScriptVisibility, D::Error>
where
    D: Deserializer<'de>,
{
    let b = bool::deserialize(deserializer)?;
    Ok(if b {
        ScriptVisibility::Public
    } else {
        ScriptVisibility::Private
    })
}

fn script_visibility_to_bool<S>(
    visibility: &ScriptVisibility,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let b = match *visibility {
        ScriptVisibility::Private => false,
        ScriptVisibility::Public => true,
    };
    serializer.serialize_bool(b)
}

#[derive(Clone, Debug, Deserialize)]
pub struct UpdateStatus {
    pub success: bool,
    pub msg: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
pub enum UpdateResponse {
    UpdateStatus(UpdateStatus),
    UpdateResult(UpdateResult),
    RunStatus(RunStatus),
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct DownloadResponse {
    #[serde(rename = "code")]
    #[serde(deserialize_with = "uri_unescape")]
    pub js_prog: String,
}

fn uri_unescape<'de, D>(deserializer: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    percent_decode(s.into_bytes().as_ref())
        .decode_utf8()
        .map(|s| s.into())
        .map_err(D::Error::custom)
}

#[derive(Clone, Default, Debug, Deserialize)]
pub struct ChatPass {
    pub pass: String,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct ChatToken {
    pub ok: bool,
    pub chat_token: String,
}

pub type ChannelData = BTreeMap<String, Vec<String>>;

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct AccountData {
    pub ok: bool,
    pub users: BTreeMap<String, ChannelData>,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct ChatMessage {
    pub id: String,
    pub channel: String,
    #[serde(rename = "t")]
    pub timestamp: f64,
    pub from_user: Option<String>,
    pub msg: String,
    pub to_user: Option<String>,
    pub is_join: bool,
    pub is_leave: bool,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct Chats {
    pub ok: bool,
    pub chats: BTreeMap<String, Vec<ChatMessage>>,
    #[serde(rename = "invalidUsernames")]
    pub invalid_users: Vec<String>,
}

#[derive(Clone, Default, Debug, Deserialize)]
#[serde(default, deny_unknown_fields)]
pub struct ChatResponseOk {
    pub ok: bool,
}
