use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering, ATOMIC_BOOL_INIT};
use futures::Future;
use serde_json::{Map, Value as JsonValue};
use tokio_core::reactor::Handle;
use steamworks::{steam_api_init, steam_api_shutdown, AuthTicketHandle};
use steamworks::steam_user::{cancel_auth_ticket, get_auth_session_ticket};

use config::HACKMUD_SECRET_KEY;
use types::AuthResponse;
use errors::{Error, InitError};
use client::{bytes_to_hex_string, Inner};

static STEAM_INITIALISED: AtomicBool = ATOMIC_BOOL_INIT;

#[derive(Debug)]
pub struct Authenticator {
    h_steam_auth_ticket: Option<AuthTicketHandle>,
    inner: Inner,
}

impl Authenticator {
    #[cfg(feature = "imitate")]
    pub fn debug_new(handle: &Handle, ca_file: PathBuf) -> Result<Self, InitError> {
        Authenticator::with_secret(handle, HACKMUD_SECRET_KEY, Some(ca_file))
    }

    #[cfg(feature = "imitate")]
    pub fn new(handle: &Handle) -> Result<Self, InitError> {
        Authenticator::with_secret(handle, HACKMUD_SECRET_KEY, None)
    }

    pub fn with_secret<T: AsRef<str>>(
        handle: &Handle,
        signing_key: T,
        ca_file: Option<PathBuf>,
    ) -> Result<Self, InitError> {
        // Make sure only one client is initialized in current lifetime.
        if !STEAM_INITIALISED.compare_and_swap(false, true, Ordering::SeqCst) {
            // steam_api authentication.
            info!("Initializing steam api");
            if !steam_api_init() {
                return Err(InitError::SteamAPIError);
            }
            let inner = Inner::with_secret(handle, signing_key, ca_file)
                .map_err(|e| InitError::CertError(e.0))?;
            Ok(Self {
                h_steam_auth_ticket: None,
                inner,
            })
        } else {
            Err(InitError::SingleTonError)
        }
    }

    pub fn authenticate(&mut self) -> impl Future<Item = String, Error = Error> {
        let mut b = vec![0; 1024];
        let (h_steam_auth_ticket, olen) = get_auth_session_ticket(&mut b);
        let steam_session_ticket = bytes_to_hex_string(&b[..olen as usize], true);
        debug!(
            "Acquired steam authentication session ticket: {}",
            steam_session_ticket
        );
        self.h_steam_auth_ticket = Some(h_steam_auth_ticket);

        let mut req = Map::new();
        req.insert(
            "steam_session_key".into(),
            JsonValue::String(steam_session_ticket.clone()),
        );
        let fut = self.inner
            .post::<AuthResponse>("accounts/sign_in_steam_session.json", req);
        fut.map(|resp| {
            info!("Authentication to hackmud server successful!");
            debug!("Authentication token: {}", resp.authentication_token);
            resp.authentication_token
        }).map_err(move |e| {
            cancel_auth_ticket(h_steam_auth_ticket);
            e
        })
    }
}

impl Drop for Authenticator {
    fn drop(&mut self) {
        info!("Releasing steam resources");
        self.h_steam_auth_ticket.map(cancel_auth_ticket);
        steam_api_shutdown();
        STEAM_INITIALISED.store(false, Ordering::SeqCst);
    }
}
