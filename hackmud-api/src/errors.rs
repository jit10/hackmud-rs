use types::Version;

#[derive(Fail, Debug)]
pub enum InitError {
    #[fail(display = "Only one Client instance is allowed during the lifetime of program")]
    SingleTonError,
    #[fail(display = "Certificate error: _0")]
    CertError(String),
    #[fail(display = "SteamAPI Initialisation Failed. Please make sure steam is running and the \
                      file `steam_appid.txt` with content `469920` is present in current \
                      working directory.")]
    SteamAPIError,
}

#[derive(Fail, Debug)]
#[fail(display = "Certificate error: _0")]
pub struct CertError(pub(crate) String);

#[derive(Fail, Debug)]
pub enum Error {
    #[fail(display = "Http error: {}", _0)]
    HttpError(#[cause] ::hyper::error::Error),
    #[fail(display = "Json parse error: {}", _0)]
    JsonError(#[cause] ::serde_json::error::Error),
    #[fail(display = "Authentication error: {}", _0)]
    AuthError(String),
    #[fail(display = "Client version is out of date, expected version: {}", _0)]
    VersionError(Version),
    #[fail(display = "{}", _0)]
    RemoteError(String),
    #[fail(display = "Unrecognized Json Message: {}", _0)]
    UnknownError(::serde_json::Value),
}
