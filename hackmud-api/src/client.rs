use std::path::PathBuf;
use std::str::FromStr;
use std::time::{SystemTime, UNIX_EPOCH};
use futures::{future, Future, Stream};
use hyper::{Method, Uri, error::Error as HyperError};
use hyper::client::{Client as HyperClient, HttpConnector, Request as HttpRequest};
use hyper::header::{qitem, AcceptEncoding, Authorization, ContentLength, ContentType, Encoding,
                    Scheme, UserAgent};
use hyper_rustls::HttpsConnector;
use percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};
use ring::hmac::{sign as hmac_sign, SigningKey};
use tokio_core::reactor::Handle;
use uuid::Uuid;
use serde_json::{from_slice as from_json_slice, from_value as from_json_value,
                 to_vec as json_to_vec, Map, Value as JsonValue};

use config::*;
use types::*;
use errors::{CertError, Error};

named!(user<&str, String>,
    map!(
        delimited!(
            tag!("`"),
            preceded!(take_s!(1), take_until_s!("`")),
            tag!("`")
        ),
        String::from
    )
);

use nom::digit;
named!(user_info<&str, UserInfo>,
    do_parse!(
        tag!("Your users:") >>
            available: ws!(separated_list_complete!(ws!(tag!(",")), user)) >>
            max_available: ws!(delimited!(tag!("("), separated_pair!(digit, tag!("/"), digit), tag!(")"))) >>
            ws!(tag!("Retired users:")) >>
            retired: ws!(separated_list_complete!(ws!(tag!(",")), user)) >>
            max_retired: ws!(preceded!(tag!("("), separated_pair!(digit, tag!("/"), digit))) >>
            tag!(")") >>
            (UserInfo {
                available,
                max_available: str::parse(max_available.1).unwrap_or(0),
                retired,
                max_retired: str::parse(max_retired.1).unwrap_or(0),
            })
    )
);

// TODO: when trait alias lands
// pub trait HackmudFuture<T> = Future<Item = T, Error = Error>;

#[derive(Debug, PartialEq, Clone)]
struct HackmudToken {
    token: String,
}

impl FromStr for HackmudToken {
    type Err = HyperError;

    fn from_str(s: &str) -> ::hyper::Result<HackmudToken> {
        Ok(HackmudToken { token: s.into() })
    }
}

impl Scheme for HackmudToken {
    fn scheme() -> Option<&'static str> {
        Some("Token")
    }

    fn fmt_scheme(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "token=\"{}\"", self.token)
    }
}

pub(crate) fn bytes_to_hex_string<T: AsRef<[u8]>>(bs: T, capitalize: bool) -> String {
    let chars = if capitalize {
        b"0123456789ABCDEF"
    } else {
        b"0123456789abcdef"
    };
    let mut out = Vec::with_capacity(2 * bs.as_ref().len());
    for &b in bs.as_ref().iter() {
        out.push(chars[(b >> 4) as usize]);
        out.push(chars[(b & 0x0F) as usize]);
    }
    unsafe { String::from_utf8_unchecked(out) }
}

fn process_error(response: &Map<String, JsonValue>) -> Option<Error> {
    let success = response.get("success").and_then(|v| v.as_bool());
    if success.map_or(true, |v| !v) {
        let version = response
            .get("current_version")
            .and_then(|v| from_json_value::<Version>(v.clone()).ok());
        if let Some(v) = version {
            error!(
                "Version mismatch, expected version: {}, client version: {}",
                v, HACKMUD_CLIENT_VERSION
            );
            return Some(Error::VersionError(v));
        }
        let msg = response.get("msg").and_then(|s| s.as_str());
        if let Some(s) = msg {
            error!("Unsuccessful response with message: {}", s);
            return Some(Error::RemoteError(s.to_owned()));
        }
        let retval = response.get("retval").and_then(|s| s.as_str());
        if let Some(s) = retval {
            error!("Unsuccessful response with retval: {}", s);
            return Some(Error::RemoteError(s.to_owned()));
        }
    }

    if let Some(auth_err) = response.get("error").and_then(|v| v.as_str()) {
        error!("Received fatal error: {}", auth_err);
        return Some(Error::AuthError(auth_err.into()));
    }

    error!("Failed to process json message: {:?}", response);
    None
}

pub(crate) struct Inner {
    secret: SigningKey,
    http_client: HyperClient<HttpsConnector>,
}

impl ::std::fmt::Debug for Inner {
    fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        fmt.debug_struct("Inner")
            .field("http_client", &self.http_client)
            .finish()
    }
}

impl Inner {
    pub(crate) fn with_secret<T: AsRef<str>>(
        handle: &Handle,
        signing_key: T,
        ca_file: Option<PathBuf>,
    ) -> Result<Self, CertError> {
        // setup https with user provided CA cert.
        let mut tls = ::rustls::ClientConfig::new();
        tls.root_store
            .add_server_trust_anchors(&::webpki_roots::TLS_SERVER_ROOTS);
        if let Some(path) = ca_file {
            let f = ::std::fs::File::open(path.as_path())
                .map_err(|_| CertError(format!("Failed to open cert file: {}", path.display())))?;
            tls.root_store
                .add_pem_file(&mut ::std::io::BufReader::new(f))
                .map_err(|_| CertError("Failed to add PEM file to root CA store".into()))?;
        };
        let mut http = HttpConnector::new(2, handle);
        http.enforce_http(false);
        let http_client = HyperClient::configure()
            .connector(HttpsConnector::from((http, tls)))
            .build(handle);

        let secret = SigningKey::new(&::ring::digest::SHA256, signing_key.as_ref().as_bytes());

        Ok(Self {
            secret,
            http_client,
        })
    }

    fn serialize_sign(&self, mut request: Map<String, JsonValue>) -> (String, Vec<u8>) {
        request.insert("version".into(), HACKMUD_CLIENT_VERSION_JSON.clone());
        request.insert(
            "n".into(),
            JsonValue::String(Uuid::new_v4().simple().to_string()),
        );
        request.insert(
            "t".into(),
            JsonValue::Number(
                SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .expect("SystemTime::duration_since failed")
                    .as_secs()
                    .into(),
            ),
        );
        let v = json_to_vec(&request).expect("Failed to serialize Request to json");
        let sig = bytes_to_hex_string(hmac_sign(&self.secret, v.as_slice()).as_ref(), false);
        (sig, v)
    }

    pub(crate) fn post<T>(
        &self,
        endpoint: &'static str,
        data: Map<String, JsonValue>,
    ) -> impl Future<Item = T, Error = Error>
    where
        T: ::serde::de::DeserializeOwned + ::std::fmt::Debug,
    {
        let mut req = HttpRequest::new(
            Method::Post,
            Uri::from_str(&format!("{}/{}", HACKMUD_SERVER_URI, endpoint))
                .expect("Failed to construct uri from endpoint"),
        );
        let (sig, body) = self.serialize_sign(data);
        {
            let headers = req.headers_mut();
            headers.set(Authorization(HackmudToken { token: sig }));
            headers.set(AcceptEncoding(vec![qitem(Encoding::Identity)]));
            headers.set(UserAgent::new(*HACKMUD_USER_AGENT));
            headers.set(ContentType::json());
            headers.set(ContentLength(body.len() as u64));
            for &(k, v) in *EXTRA_HEADERS {
                headers.set_raw(k, v);
            }
        }
        if log_enabled!(::log::Level::Trace) {
            trace!(
                "Building http request: {:?} with message: {}",
                req,
                ::std::str::from_utf8(&body).unwrap_or("Failed to display request body")
            );
        }
        req.set_body(body);
        self.http_client
            .request(req)
            .map_err(Error::HttpError)
            .and_then(|resp| {
                resp.body()
                    .concat2()
                    .map_err(Error::HttpError)
                    .and_then(move |b| {
                        // TODO: log warn! all the non-ok status code.
                        let val = from_json_slice::<Response<T>>(&b).map_err(|e| {
                            error!(
                                "Json error: {} while parsing {}",
                                e,
                                ::std::str::from_utf8(b.as_ref()).unwrap_or("")
                            );
                            Error::JsonError(e)
                        })?;
                        trace!("Received hackmud json response: {:?}", val);
                        match val {
                            Response::Success(v) => Ok(v),
                            Response::Failure(e) => Err(process_error(&e)
                                .unwrap_or_else(|| Error::UnknownError(JsonValue::Object(e)))),
                        }
                    })
            })
    }
}

#[derive(Debug)]
pub struct Client {
    auth_token: String,
    inner: Inner,
}

impl Client {
    #[cfg(feature = "imitate")]
    pub fn debug_new(handle: &Handle, ca_file: PathBuf) -> Result<Self, CertError> {
        Self::with_secret(handle, HACKMUD_SECRET_KEY, Some(ca_file))
    }

    #[cfg(feature = "imitate")]
    pub fn new(handle: &Handle) -> Self {
        Self::with_secret(handle, HACKMUD_SECRET_KEY, None)
            .expect("Shouldn't fail without a certificate")
    }

    pub fn with_secret<T: AsRef<str>>(
        handle: &Handle,
        signing_key: T,
        ca_file: Option<PathBuf>,
    ) -> Result<Self, CertError> {
        let inner = Inner::with_secret(handle, signing_key, ca_file)?;
        Ok(Self {
            auth_token: "".into(),
            inner,
        })
    }

    pub fn set_auth_token<T: Into<String>>(&mut self, token: T) {
        self.auth_token = token.into();
    }

    fn post<T>(
        &self,
        endpoint: &'static str,
        mut data: Map<String, JsonValue>,
    ) -> impl Future<Item = T, Error = Error>
    where
        T: ::serde::de::DeserializeOwned + ::std::fmt::Debug,
    {
        data.insert(
            "account_token".into(),
            JsonValue::String(self.auth_token.clone()),
        );
        self.inner.post(endpoint, data)
    }

    pub fn change_user<T: Into<String>>(
        &self,
        user_name: T,
        create: bool,
    ) -> impl Future<Item = UserStatus, Error = Error> {
        let mut req = Map::new();
        req.insert("username".into(), JsonValue::String(user_name.into()));
        if create {
            req.insert("confirmed".into(), JsonValue::Bool(true));
        }
        self.post("check_username.json", req)
    }

    pub fn list_users(&self) -> impl Future<Item = UserInfo, Error = Error> {
        self.change_user("", false).and_then(|status| {
            match user_info(&status.msg).to_full_result() {
                Ok(u) => future::ok(u),
                Err(_) => future::err(Error::UnknownError(JsonValue::String(status.msg))),
            }
        })
    }

    pub fn retire_user<T: Into<String>>(
        &self,
        user_name: T,
    ) -> impl Future<Item = RetireResponse, Error = Error> {
        let mut req = Map::new();
        req.insert("username".into(), JsonValue::String(user_name.into()));
        self.post("retire.json", req)
    }

    pub fn client_data(&self) -> impl Future<Item = ClientData, Error = Error> {
        self.post("client_data.json", Map::new())
    }

    pub fn poll<T>(&self, user_name: T) -> impl Future<Item = Vec<Option<Notification>>, Error = Error>
    where
        T: Into<String>,
    {
        let mut req = Map::new();
        req.insert("username".into(), JsonValue::String(user_name.into()));
        self.post("notifications.json", req)
    }

    pub fn run_script<U, S, A>(
        &self,
        user_name: U,
        script_name: S,
        args: A,
        cols: Option<u32>,
    ) -> impl Future<Item = RunStatus, Error = Error> + 'static
    where
        U: Into<String>,
        S: Into<String>,
        A: Into<String>,
    {
        let mut req = Map::new();
        let user_name = user_name.into();
        let script_name = script_name.into();
        let script_name = if !script_name.contains('.') {
            user_name.clone() + "." + &script_name
        } else {
            script_name
        };
        req.insert("username".into(), JsonValue::String(user_name));
        req.insert("script_name".into(), JsonValue::String(script_name));
        let args = args.into();
        if !args.is_empty() {
            req.insert(
                "args".into(),
                JsonValue::String(utf8_percent_encode(&args, DEFAULT_ENCODE_SET).to_string()),
            );
        }
        cols.map(|c| req.insert("cols".into(), JsonValue::Number(c.into())));
        self.post("run.json", req)
    }

    fn raw_update_script<U, S, A, C>(
        &self,
        user_name: U,
        script_name: S,
        script_args: A,
        script_code: Option<C>,
        prop: ScriptProperties,
    ) -> impl Future<Item = UpdateResponse, Error = Error>
    where
        U: Into<String>,
        S: Into<String>,
        A: Into<String>,
        C: Into<String>,
    {
        let mut req = Map::new();
        req.insert("username".into(), JsonValue::String(user_name.into()));
        req.insert("script_name".into(), JsonValue::String(script_name.into()));
        let script_args = script_args.into();
        if !script_args.is_empty() {
            req.insert(
                "args".into(),
                JsonValue::String(
                    utf8_percent_encode(&script_args, DEFAULT_ENCODE_SET).to_string(),
                ),
            );
        }
        script_code.map(|s| {
            req.insert(
                "script_code".into(),
                JsonValue::String(utf8_percent_encode(&s.into(), DEFAULT_ENCODE_SET).to_string()),
            );
        });
        for &(s, b) in &[
            ("dry", prop.is_dry),
            ("shift", prop.is_shift),
            ("run", prop.run_on_upload),
        ] {
            if b {
                req.insert(s.into(), JsonValue::Bool(b));
            }
        }
        prop.set_visibility.map(|v| {
            req.insert(
                "script_is_public".into(),
                JsonValue::Bool(match v {
                    ScriptVisibility::Public => true,
                    ScriptVisibility::Private => false,
                }),
            );
        });
        self.post("update.json", req)
    }

    pub fn update_script<U, S, A, C>(
        &self,
        user_name: U,
        script_name: S,
        script_args: A,
        script_code: C,
        properties: ScriptProperties,
    ) -> impl Future<Item = UpdateResponse, Error = Error>
    where
        U: Into<String>,
        S: Into<String>,
        A: Into<String>,
        C: Into<String>,
    {
        self.raw_update_script(
            user_name,
            script_name,
            script_args,
            Some(script_code),
            properties,
        )
    }

    pub fn delete_script<U, S>(
        &self,
        user_name: U,
        script_name: S,
    ) -> impl Future<Item = UpdateStatus, Error = Error>
    where
        U: Into<String>,
        S: Into<String>,
    {
        self.raw_update_script(
            user_name,
            script_name,
            "",
            None::<&'static str>,
            ScriptProperties::default(),
        ).and_then(|r| match r {
            UpdateResponse::UpdateStatus(status) => Ok(status),
            _ => Err(Error::UnknownError(JsonValue::String(
                "expected delete status, found variant UpdateResult/RunStatus".into(),
            ))),
        })
    }

    pub fn download_script<U, S>(
        &self,
        user_name: U,
        script_name: S,
    ) -> impl Future<Item = DownloadResponse, Error = Error>
    where
        U: Into<String>,
        S: Into<String>,
    {
        let mut req = Map::new();
        req.insert("username".into(), JsonValue::String(user_name.into()));
        req.insert("script_name".into(), JsonValue::String(script_name.into()));
        self.post("download.json", req)
    }

    pub fn get_chat_pass(&self) -> impl Future<Item = ChatPass, Error = Error> {
        self.post("mobile/gen_pass.json", Map::new())
    }

    pub fn revoke_chat_token(&self) -> impl Future<Item = ChatResponseOk, Error = Error> {
        self.post("mobile/revoke.json", Map::new())
    }
}
