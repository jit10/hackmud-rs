#![allow(dead_code, non_snake_case, non_camel_case_types, non_upper_case_globals)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::os::raw::{c_char, c_void};
use std::ptr::null_mut;
use std::mem::size_of;

type uint32 = ::std::os::raw::c_uint;
pub(crate) type AuthTicketHandle = uint32;

extern "C" {
    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamUser"]
    pub fn ISteamClient_GetISteamUser(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamFriends"]
    pub fn ISteamClient_GetISteamFriends(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamUtils"]
    pub fn ISteamClient_GetISteamUtils(
        this: *mut c_void,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamMatchmaking"]
    pub fn ISteamClient_GetISteamMatchmaking(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamMatchmakingServers"]
    pub fn ISteamClient_GetISteamMatchmakingServers(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamUserStats"]
    pub fn ISteamClient_GetISteamUserStats(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamApps"]
    pub fn ISteamClient_GetISteamApps(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamNetworking"]
    pub fn ISteamClient_GetISteamNetworking(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamRemoteStorage"]
    pub fn ISteamClient_GetISteamRemoteStorage(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamScreenshots"]
    pub fn ISteamClient_GetISteamScreenshots(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamHTTP"]
    pub fn ISteamClient_GetISteamHTTP(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamUnifiedMessages"]
    pub fn ISteamClient_GetISteamUnifiedMessages(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamController"]
    pub fn ISteamClient_GetISteamController(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamUGC"]
    pub fn ISteamClient_GetISteamUGC(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamAppList"]
    pub fn ISteamClient_GetISteamAppList(
        this: *mut c_void,
        hSteamUser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamMusic"]
    pub fn ISteamClient_GetISteamMusic(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamMusicRemote"]
    pub fn ISteamClient_GetISteamMusicRemote(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamHTMLSurface"]
    pub fn ISteamClient_GetISteamHTMLSurface(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamInventory"]
    pub fn ISteamClient_GetISteamInventory(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamVideo"]
    pub fn ISteamClient_GetISteamVideo(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamClient_GetISteamParentalSettings"]
    pub fn ISteamClient_GetISteamParentalSettings(
        this: *mut c_void,
        hSteamuser: HSteamUser,
        hSteamPipe: HSteamPipe,
        pchVersion: *const c_char,
    ) -> *mut c_void;

    #[link_name = "\u{1}SteamAPI_ISteamUser_GetAuthSessionTicket"]
    pub fn ISteamUser_GetAuthSessionTicket(
        this: *mut c_void,
        pTicket: *mut c_void,
        cbMaxTicket: ::std::os::raw::c_int,
        pcbTicket: *mut uint32,
    ) -> AuthTicketHandle;

    #[link_name = "\u{1}SteamAPI_ISteamUser_CancelAuthTicket"]
    pub fn ISteamUser_CancelAuthTicket(this: *mut c_void, hAuthTicket: AuthTicketHandle);
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
struct CSteamAPIContext {
    m_pSteamClient: *mut c_void,
    m_pSteamUser: *mut c_void,
    m_pSteamFriends: *mut c_void,
    m_pSteamUtils: *mut c_void,
    m_pSteamMatchmaking: *mut c_void,
    m_pSteamUserStats: *mut c_void,
    m_pSteamApps: *mut c_void,
    m_pSteamMatchmakingServers: *mut c_void,
    m_pSteamNetworking: *mut c_void,
    m_pSteamRemoteStorage: *mut c_void,
    m_pSteamScreenshots: *mut c_void,
    m_pSteamHTTP: *mut c_void,
    m_pSteamUnifiedMessages: *mut c_void,
    m_pController: *mut c_void,
    m_pSteamUGC: *mut c_void,
    m_pSteamAppList: *mut c_void,
    m_pSteamMusic: *mut c_void,
    m_pSteamMusicRemote: *mut c_void,
    m_pSteamHTMLSurface: *mut c_void,
    m_pSteamInventory: *mut c_void,
    m_pSteamVideo: *mut c_void,
    m_pSteamParentalSettings: *mut c_void,
}

#[inline]
unsafe fn CSteamAPIContext_init(ctxt: *mut CSteamAPIContext) -> bool {
    let hSteamUser = SteamAPI_GetHSteamUser();
    let hSteamPipe = SteamAPI_GetHSteamPipe();
    if hSteamPipe == 0 {
        return false;
    }

    (*ctxt).m_pSteamClient =
        SteamInternal_CreateInterface(STEAMCLIENT_INTERFACE_VERSION.as_ptr() as *const _);
    if (*ctxt).m_pSteamClient.is_null() {
        return false;
    }

    (*ctxt).m_pSteamUser = ISteamClient_GetISteamUser(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMUSER_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamUser.is_null() {
        return false;
    }

    (*ctxt).m_pSteamFriends = ISteamClient_GetISteamFriends(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMFRIENDS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamFriends.is_null() {
        return false;
    }

    (*ctxt).m_pSteamUtils = ISteamClient_GetISteamUtils(
        (*ctxt).m_pSteamClient,
        hSteamPipe,
        STEAMUTILS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamUtils.is_null() {
        return false;
    }

    (*ctxt).m_pSteamMatchmaking = ISteamClient_GetISteamMatchmaking(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMMATCHMAKING_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamMatchmaking.is_null() {
        return false;
    }

    (*ctxt).m_pSteamMatchmakingServers = ISteamClient_GetISteamMatchmakingServers(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMMATCHMAKINGSERVERS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamMatchmakingServers.is_null() {
        return false;
    }

    (*ctxt).m_pSteamUserStats = ISteamClient_GetISteamUserStats(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMUSERSTATS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamUserStats.is_null() {
        return false;
    }

    (*ctxt).m_pSteamApps = ISteamClient_GetISteamApps(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMAPPS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamApps.is_null() {
        return false;
    }

    (*ctxt).m_pSteamNetworking = ISteamClient_GetISteamNetworking(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMNETWORKING_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamNetworking.is_null() {
        return false;
    }

    (*ctxt).m_pSteamRemoteStorage = ISteamClient_GetISteamRemoteStorage(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMREMOTESTORAGE_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamRemoteStorage.is_null() {
        return false;
    }

    (*ctxt).m_pSteamScreenshots = ISteamClient_GetISteamScreenshots(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMSCREENSHOTS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamScreenshots.is_null() {
        return false;
    }

    (*ctxt).m_pSteamHTTP = ISteamClient_GetISteamHTTP(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMHTTP_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamHTTP.is_null() {
        return false;
    }

    (*ctxt).m_pSteamUnifiedMessages = ISteamClient_GetISteamUnifiedMessages(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMUNIFIEDMESSAGES_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamUnifiedMessages.is_null() {
        return false;
    }

    (*ctxt).m_pController = ISteamClient_GetISteamController(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMCONTROLLER_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pController.is_null() {
        return false;
    }

    (*ctxt).m_pSteamUGC = ISteamClient_GetISteamUGC(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMUGC_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamUGC.is_null() {
        return false;
    }

    (*ctxt).m_pSteamAppList = ISteamClient_GetISteamAppList(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMAPPLIST_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamAppList.is_null() {
        return false;
    }

    (*ctxt).m_pSteamMusic = ISteamClient_GetISteamMusic(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMMUSIC_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamMusic.is_null() {
        return false;
    }

    (*ctxt).m_pSteamMusicRemote = ISteamClient_GetISteamMusicRemote(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMMUSICREMOTE_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamMusicRemote.is_null() {
        return false;
    }

    (*ctxt).m_pSteamHTMLSurface = ISteamClient_GetISteamHTMLSurface(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMHTMLSURFACE_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamHTMLSurface.is_null() {
        return false;
    }

    (*ctxt).m_pSteamInventory = ISteamClient_GetISteamInventory(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMINVENTORY_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamInventory.is_null() {
        return false;
    }

    (*ctxt).m_pSteamVideo = ISteamClient_GetISteamVideo(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMVIDEO_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamVideo.is_null() {
        return false;
    }

    (*ctxt).m_pSteamParentalSettings = ISteamClient_GetISteamParentalSettings(
        (*ctxt).m_pSteamClient,
        hSteamUser,
        hSteamPipe,
        STEAMPARENTALSETTINGS_INTERFACE_VERSION.as_ptr() as *const _,
    );
    if (*ctxt).m_pSteamParentalSettings.is_null() {
        return false;
    }

    true
}

#[inline]
unsafe fn CSteamAPIContext_clear(ctxt: *mut CSteamAPIContext) {
    (*ctxt).m_pSteamClient = null_mut();
    (*ctxt).m_pSteamFriends = null_mut();
    (*ctxt).m_pSteamUtils = null_mut();
    (*ctxt).m_pSteamMatchmaking = null_mut();
    (*ctxt).m_pSteamUserStats = null_mut();
    (*ctxt).m_pSteamApps = null_mut();
    (*ctxt).m_pSteamMatchmakingServers = null_mut();
    (*ctxt).m_pSteamNetworking = null_mut();
    (*ctxt).m_pSteamRemoteStorage = null_mut();
    (*ctxt).m_pSteamScreenshots = null_mut();
    (*ctxt).m_pSteamHTTP = null_mut();
    (*ctxt).m_pSteamUnifiedMessages = null_mut();
    (*ctxt).m_pController = null_mut();
    (*ctxt).m_pSteamUGC = null_mut();
    (*ctxt).m_pSteamAppList = null_mut();
    (*ctxt).m_pSteamMusic = null_mut();
    (*ctxt).m_pSteamMusicRemote = null_mut();
    (*ctxt).m_pSteamHTMLSurface = null_mut();
    (*ctxt).m_pSteamInventory = null_mut();
    (*ctxt).m_pSteamVideo = null_mut();
    (*ctxt).m_pSteamParentalSettings = null_mut();
}

#[inline]
unsafe extern "C" fn steam_internal_on_context_init(p: *mut c_void) {
    CSteamAPIContext_clear(p as *mut CSteamAPIContext);
    if SteamAPI_GetHSteamPipe() != 0 {
        CSteamAPIContext_init(p as *mut CSteamAPIContext);
    }
}

const CALLBACK_COUNTER_CONTEXT_SIZE: usize =
    (2 + (size_of::<CSteamAPIContext>() / size_of::<*mut c_void>()));
static mut CALLBACK_COUNTER_CONTEXT: [*mut c_void; CALLBACK_COUNTER_CONTEXT_SIZE] =
    [null_mut(); CALLBACK_COUNTER_CONTEXT_SIZE];

#[inline]
unsafe fn steam_internal_module_context() -> *mut CSteamAPIContext {
    CALLBACK_COUNTER_CONTEXT[0] = steam_internal_on_context_init as *mut c_void;
    SteamInternal_ContextInit(CALLBACK_COUNTER_CONTEXT.as_ptr() as *mut c_void)
        as *mut CSteamAPIContext
}

#[inline]
unsafe fn SteamUser() -> *mut c_void {
    (*steam_internal_module_context()).m_pSteamUser
}

pub(crate) fn steam_api_init() -> bool {
    unsafe { SteamAPI_Init() }
}

pub(crate) fn steam_api_shutdown() {
    unsafe { SteamAPI_Shutdown() }
}

pub(crate) mod steam_user {
    use super::{AuthTicketHandle, ISteamUser_CancelAuthTicket, ISteamUser_GetAuthSessionTicket,
                SteamUser};
    use std::os::raw::c_void;

    pub(crate) fn get_auth_session_ticket<T: AsMut<[u8]>>(
        mut ticket: T,
    ) -> (AuthTicketHandle, u32) {
        let mut olen = 0u32;
        let h: AuthTicketHandle;
        unsafe {
            h = ISteamUser_GetAuthSessionTicket(
                SteamUser(),
                ticket.as_mut().as_mut_ptr() as *mut c_void,
                ticket.as_mut().len() as i32,
                &mut olen,
            )
        }
        (h, olen)
    }

    pub(crate) fn cancel_auth_ticket(h: AuthTicketHandle) {
        unsafe { ISteamUser_CancelAuthTicket(SteamUser(), h) }
    }
}
