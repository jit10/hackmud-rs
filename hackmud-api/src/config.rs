use serde_json::{to_value, Value};

use types::Version;

pub static HACKMUD_SERVER_URI: &'static str = "https://www.hackmud.com";
pub static HACKMUD_CLIENT_VERSION: Version = Version {
    major: 1,
    minor: 4,
    build: 10,
};
#[cfg(feature = "imitate")]
pub(crate) static HACKMUD_SECRET_KEY : &'static str = "eb04baa99b61f50e5ed3af242bcf4ae0473a0ca16661401a1d3898e6dedd78dd53d87a7a640aed47cd8b633810e56c9e51b31e6d43641ff540fd221bb679dd86";

lazy_static! {
pub(crate) static ref HACKMUD_CLIENT_VERSION_JSON: Value =
    to_value(HACKMUD_CLIENT_VERSION.clone()).expect("Failed to convert version to json Value");
pub(crate) static ref HACKMUD_USER_AGENT: &'static str = if cfg!(feature = "imitate") {
    "UnityPlayer/5.5.4p5 (http://unity3d.com)"
} else {
    "hackmud-rs"
};
pub(crate) static ref EXTRA_HEADERS: &'static [(&'static str, &'static str)] =
    if cfg!(feature = "imitate") {
        &[("X-Unity-Version", "5.5.4p5")]
    } else {
        &[]
    };
}
