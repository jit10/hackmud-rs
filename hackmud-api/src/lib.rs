#![cfg_attr(feature = "cargo-clippy", allow(const_static_lifetime))]
#![feature(conservative_impl_trait)]

#[macro_use]
extern crate failure;
extern crate futures;
extern crate hyper;
extern crate hyper_rustls;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate nom;
extern crate percent_encoding;
extern crate ring;
extern crate rustls;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio_core;
extern crate uuid;
extern crate webpki_roots;

#[cfg(not(feature = "no_steam_auth"))]
mod steamworks;
mod config;
mod errors;
mod client;
mod chat_client;
pub use config::{HACKMUD_CLIENT_VERSION, HACKMUD_SERVER_URI};
pub use errors::{CertError, Error, InitError};
pub mod types;
#[cfg(not(feature = "no_steam_auth"))]
pub mod steam_auth;
pub use client::Client;
pub use chat_client::Client as ChatClient;
