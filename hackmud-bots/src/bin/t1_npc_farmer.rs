extern crate env_logger;
extern crate futures;
extern crate hackmud_client;
#[macro_use]
extern crate log;
extern crate serde_json;
extern crate tokio;

use std::process;
use std::net::SocketAddr;
use std::time::{Duration, Instant};
use tokio::prelude::*;
use future::{Either, loop_fn, Loop};
use tokio::timer;
use hackmud_client::DEFAULT_HACKMUD_CLIENTD_ADDR;
use hackmud_client::{ResponseError, client::Client};

const USER: &str = "unlambda";
const NPC_LISTER: &str = "cons.npcs";
const T1_FINDER: &str = "cons.t1_finder";
const T1_SOLVER: &str = "cons.t1_solver";

const NPC_PER_SOLVER: usize = 5;
const HARDLINE_TIMER: Duration = Duration::from_secs(112);
const HARDLINE_CD: Duration = Duration::from_secs(30);

fn with_hardline<F>(client: Client, fut: F) -> Box<Future<Item = (), Error = ResponseError>>
where
    F: Future<Item = (), Error = ResponseError> + 'static,
{
    use timer::Delay;

    Box::new(loop_fn(fut, move |fut| {
        let dc_fut = client.run_script("kernel.hardline", "{\"dc\": true}");
        client
            .run_script("kernel.hardline", "{\"activate\": true}")
            .and_then(move |res| {
                debug!("Hardline active: {}", res.retval);
                if res.retval
                    .get("ok")
                    .and_then(|v| v.as_bool())
                    .unwrap_or(false)
                {
                    Either::A(
                        Delay::new(Instant::now() + HARDLINE_TIMER)
                            .select2(fut)
                            .then(move |res| match res {
                                Ok(Either::A((_, fut))) => {
                                    debug!("hardline timer expired; waiting out the cooldown");
                                    Either::A(dc_fut.and_then(|_| {
                                        Delay::new(Instant::now() + HARDLINE_CD)
                                            .then(|_| Ok(Loop::Continue(fut)))
                                    }))
                                }
                                Ok(Either::B(_)) | Err(Either::A(_)) => {
                                    info!("Future finished");
                                    Either::B(future::ok(Loop::Break(())))
                                }
                                Err(Either::B((e, _))) => Either::B(future::err(e)),
                            }),
                    )
                } else {
                    debug!("hardline not available; waiting out the cooldown");
                    Either::B(
                        Delay::new(Instant::now() + HARDLINE_CD).then(|_| Ok(Loop::Continue(fut))),
                    )
                }
            })
    }))
}

fn farm_corp(
    client: &Client,
    corp: serde_json::Value,
) -> Box<Future<Item = (), Error = ResponseError>> {
    let corp_inner = corp.clone();
    let client_inner = client.clone();
    Box::new(
        client
            .run_script(
                T1_FINDER,
                &format!(
                    "{{\"corp\": #s.{}, \"refresh\": true}}",
                    corp.as_str().unwrap()
                ),
            )
            .and_then(move |res| {
                if res.retval
                    .get("ok")
                    .and_then(|v| v.as_bool())
                    .unwrap_or(false)
                {
                    let client = client_inner.clone();
                    Either::A(
                        loop_fn((true, vec![]), move |(cont, mut npcs)| {
                            if cont {
                                Either::A(
                                    client
                                        .run_script(
                                            T1_FINDER,
                                            &format!(
                                                "{{\"corp\": #s.{}}}",
                                                corp_inner.as_str().unwrap()
                                            ),
                                        )
                                        .and_then(|mut res| {
                                            let cont = res.retval
                                                .get("continue")
                                                .and_then(|v| v.as_bool())
                                                .unwrap_or(false);
                                            res.retval
                                                .get_mut("npcs")
                                                .and_then(|v| v.as_array_mut())
                                                .map(|v| npcs.append(v));
                                            Ok(Loop::Continue((cont, npcs)))
                                        }),
                                )
                            } else {
                                info!(
                                    "Discovered: {} npcs for corp: {}, farming..",
                                    npcs.len(),
                                    corp_inner.as_str().unwrap()
                                );
                                Either::B(future::ok(Loop::Break(npcs)))
                            }
                        }).and_then(move |npcs| {
                            stream::iter_ok(npcs)
                                .chunks(NPC_PER_SOLVER)
                                .for_each(move |npcs| {
                                    use std::fmt::Write;

                                    let mut args = String::from("{");
                                    for (i, n) in npcs.iter().enumerate() {
                                        if i != 0 {
                                            args.push(',');
                                        }
                                        args.write_fmt(format_args!(
                                            "\"t{}\": {}",
                                            i,
                                            n.as_str().unwrap()
                                        )).unwrap();
                                    }
                                    args.push('}');

                                    debug!("Running: {} {}", T1_SOLVER, args);
                                    client_inner.run_script(T1_SOLVER, &args).map(|_| ())
                                })
                        }),
                    )
                } else {
                    error!("Failed to farm npc: {}, got response {}", corp, res.retval);
                    Either::B(future::ok(()))
                }
            }),
    )
}

fn t1_farm(addr: SocketAddr) -> Result<(), ResponseError> {
    let mut rt = tokio::runtime::current_thread::Runtime::new().unwrap();
    rt.block_on(future::lazy(|| {
        // don't care about the notification stream.
        let mut client = Client::new(addr);
        client.spawn_message_processor(
            &tokio::executor::current_thread::TaskExecutor::current(),
            0x10,
        );
        client.set_user(USER).and_then(move |(b, msg)| {
            if b {
                info!("user set to: {}", USER);
                let client_inner = client.clone();
                let fut = loop_fn((), move |_| {
                    let client = client.clone();
                    client
                        .run_script(NPC_LISTER, "{\"tier\": 1}")
                        .and_then(|res| {
                            let corps = match res.retval {
                                serde_json::Value::Array(v) => v,
                                _ => vec![],
                            };
                            stream::iter_ok(corps).for_each(move |corp| farm_corp(&client, corp))
                        })
                        .and_then(|_| Ok(Loop::Continue(())))
                });

                Either::A(with_hardline(client_inner, fut))
            } else {
                error!("Failed to set user to {}: {}", USER, msg);
                Either::B(future::ok(()))
            }
        })
    }))
}

fn main() {
    env_logger::init();
    let addr = std::env::args()
        .nth(1)
        .and_then(|s| s.parse().ok())
        .unwrap_or_else(|| DEFAULT_HACKMUD_CLIENTD_ADDR.parse().unwrap());
    match t1_farm(addr) {
        Ok(_) => {
            process::exit(0);
        }
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    }
}
