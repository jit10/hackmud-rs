with import <nixpkgs> {};

(import ../shell.nix).overrideAttrs(oldAttrs: {
  inherit (oldAttrs);
  name = "hackmud-gui";
  buildInputs = oldAttrs.buildInputs ++ [ gnome3.adwaita-icon-theme gnome3.gtk cairo pango glib pkgconfig ];
})
