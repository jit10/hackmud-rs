#![feature(conservative_impl_trait, try_from)]
extern crate chrono;
extern crate env_logger;
extern crate futures;
extern crate gdk;
extern crate gdk_sys;
extern crate gio;
extern crate glib;
extern crate glib_sys;
extern crate gobject_sys;
extern crate gtk;
extern crate gtk_sys;
extern crate hackmud_api;
extern crate hackmud_client;
#[macro_use]
extern crate lazy_static;
extern crate libc;
#[macro_use]
extern crate log;
extern crate pango;
extern crate radix_trie;
extern crate regex;
#[macro_use]
extern crate serde_json;
extern crate tokio_core;
extern crate tokio_io;

use gio::prelude::*;
use gtk::prelude::*;

use std::borrow::Cow;
use std::collections::{HashMap, HashSet};
use std::cell::{Cell, RefCell};
use std::env::args;
use std::io;
use std::rc::Rc;
use std::net::SocketAddr;
use std::thread::{self, JoinHandle};
use env_logger::filter;
use tokio_core::net::TcpStream;
use tokio_core::reactor::Core;
use tokio_io::AsyncRead;
use futures::{future, Future, Sink, Stream};
use futures::sync::{mpsc, oneshot};

use hackmud_client::{DEFAULT_HACKMUD_CLIENTD_ADDR, HACKMUD_CLIENT_VERSION};
use hackmud_client::{Notification, NotificationData, Response, ResponseData};

mod colors;
mod format;

pub fn hackmud_css() -> String {
    format!(
        r#"
@define-color bg_color {black};
@define-color sep_color {text_blue};

* {{
    background-color: @bg_color;
    border-color:     @sep_color;
    border-image:     none;
    outline-width:    0px;
    font-family:      White Rabbit;
    font-size:        103%;
}}

separator.wide {{
    min-width:        9px;
    background-color: transparent;
    background-image: none;
    border:           none;
}}

spinner {{
    color: {eleet_orange};
}}

window {{
    border-style: solid;
    border-color: {gray};
    border-width: 2px;
}}

/* transparent background allows us to draw the background for block select and
   not let the original draw call repaint the background */
scrolledwindow, textview, textview text {{
    background-color: transparent;
    border-width:     0px;
}}

frame border, scrolledwindow#log-window {{
    border-style: solid;
    border-width: 2px;
}}

scrolledwindow overshoot, scrolledwindow undershoot {{
    background: none;
}}

/* scrollbar */

scrollbar {{
    margin-top:    0px;
    margin-bottom: 0px;
    border:        none;
}}

scrolledwindow:not(#shell-window) scrollbar {{
    /* same as textview margins in glade file */
    margin-top:    10px;
    margin-bottom: 10px;
}}

trough {{
    /* official gui */
    min-width:        9px;
    box-shadow:       none;
    border:           none;
    border-radius:    0;
    background-color: {trough_bg};
}}

slider {{
    /* official gui */
    min-width:        9px;
    margin:           0px;
    box-shadow:       none;
    border:           none;
    border-radius:    0;
    background-color: {slider_bg};
}}

/* .. */

headerbar {{
    background-image: none;
    border:           none;
}}

*:focus {{
    border-color: {hard_blue};
}}

textview, textview text {{
    background-image: none;
    text-shadow:      none;
    font-size:        16px;
    padding:          0px;
    color:            white;
    caret-color:      white;
}}

/* animation label inside shell-view */
scrolledwindow#shell-window > textview > label {{
    background-color: transparent;
    color:     @bg_color;
    font-size: 16px;
}}

entry {{
    background-image: none;
    text-shadow:      none;
    color:            {light_gray};
    caret-color:      white;
    font-size:        110%;
}}

selection {{
    color:            currentColor;
    background-color: {selection_bg};
}}

#log-window > textview text {{
    color: {light_gray};
}}

window label, messagedialog label, * label.title {{
    color: {title};
}}

scrolledwindow#shell-window > textview text,
scrolledwindow#chat-window > textview text,
window#hackmud-window box > label {{
    color: {text_blue};
}}

window#hackmud-window frame > label {{
    border-style:     solid;
    border-width:     2px;
    color:            @bg_color;
    background-color: {text_blue};
}}

window#hackmud-window paned > box:last-child > box {{
    border-style: solid;
    border-width: 2px;
}}

window#hackmud-window paned > box {{
    border: none;
}}

expander arrow, button label {{
    color: {hard_blue};
}}

button {{
    text-shadow:      0 1px {gray};
    -gtk-icon-shadow: 0 1px {gray};
}}

button:checked, button:active {{
    text-shadow:      0 -1px transparent;
    -gtk-icon-shadow: 0 -1px transparent;
}}

button:not(.titlebutton) {{
    background-image: none;
}}

button:checked {{
    background-color: {hard_blue};
}}

/* flip the colors */
expander arrow:checked, button:checked label {{
    background-color: {hard_blue};
    color:            @bg_color;
}}

expander arrow:hover, button:hover label, button:focus label, button:active label {{
    color: {eleet_orange};
}}

expander arrow:focus, button.radio:hover label, button.radio:focus label, button.radio:active label {{
    color: {hover};
}}

button.titlebutton {{
    border-color:     transparent;
    background-color: transparent;
    color:            transparent;
}}

button > image {{
    background-color: transparent;
}}
"#,
        black = colors::BLACK,
        gray = colors::ALL_COLORS[4],
        light_gray = colors::LIGHT_GRAY,
        text_blue = colors::TEXT_BLUE,
        title = colors::ALL_COLORS[15],
        hover = colors::ALL_COLORS[41],
        selection_bg = colors::SELECTION_BG,
        trough_bg = colors::SCROLLBAR_TROUGH_BG,
        slider_bg = colors::SCROLLBAR_SLIDER_BG,
        hard_blue = colors::HARDCORE_BLUE,
        eleet_orange = colors::ELEET_ORANGE
    )
}

// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param (mut $x:ident)) => ( mut $x );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
            move |$(clone!(@param $p),)+| $body
        }
    );
}

unsafe fn connect_after(
    receiver: *mut gobject_sys::GObject,
    signal_name: &str,
    trampoline: gobject_sys::GCallback,
    closure: *mut Box<Fn() + 'static>,
) -> glib::signal::SignalHandlerId {
    use glib::translate::{from_glib, ToGlibPtr};

    unsafe extern "C" fn destroy_closure(ptr: *mut libc::c_void, _: *mut gobject_sys::GClosure) {
        let _guard = glib::source::CallbackGuard::new();
        // destroy
        Box::<Box<Fn()>>::from_raw(ptr as *mut _);
    }

    let handle = gobject_sys::g_signal_connect_data(
        receiver,
        signal_name.to_glib_none().0,
        trampoline,
        closure as *mut _,
        Some(destroy_closure),
        gobject_sys::GConnectFlags::AFTER,
    );
    assert!(handle > 0);
    from_glib(handle)
}

fn synthesize_key_press_event<W: gtk::WidgetExt>(
    widget: &W,
    keyval: gdk::enums::key::Key,
    mod_mask: gdk::ModifierType,
) {
    use gdk_sys::*;
    use glib::translate::ToGlibPtr;

    unsafe {
        let mut keys: *mut GdkKeymapKey = std::ptr::null_mut();
        let mut n_keys: i32 = 0;
        if gdk_keymap_get_entries_for_keyval(
            gdk_keymap_get_default(),
            keyval,
            &mut keys as *mut *mut GdkKeymapKey,
            &mut n_keys as *mut i32,
        ) != 0
        {
            let ev = gdk_event_new(GDK_KEY_PRESS);
            gdk_event_set_device(
                ev,
                gdk_seat_get_keyboard(gdk_display_get_default_seat(gdk_display_get_default())),
            );
            let key_ev = ev as *mut GdkEventKey;
            (*key_ev).hardware_keycode = (*keys).keycode as u16;
            glib_sys::g_free(keys as *mut libc::c_void);
            (*key_ev).state = GdkModifierType::from_bits_truncate(mod_mask.bits());
            (*key_ev).window = widget.get_window().unwrap().clone().to_glib_full();
            (*key_ev).keyval = keyval;
            (*key_ev).length = 1;
            (*key_ev).send_event = 0;
            (*key_ev).time = GDK_CURRENT_TIME as u32;
            gtk_sys::gtk_main_do_event(ev);
            gdk_event_free(ev);
        }
    }
}

const MAX_BUFFER_LINES: usize = 16_000;

// To be called after inserting a line(or more) to ensure the buffer limit is enforced.
#[inline]
fn enforce_buffer_limit(buf: &gtk::TextBuffer) {
    let thresold = MAX_BUFFER_LINES / 100;
    if buf.get_line_count() > (thresold + MAX_BUFFER_LINES) as i32 {
        let mut iter = buf.get_start_iter();
        iter.forward_lines(buf.get_line_count() - MAX_BUFFER_LINES as i32);
        buf.delete(&mut buf.get_start_iter(), &mut iter);
    }
}

// Log to gui window `log_window`.
struct Logger(filter::Filter);

impl Logger {
    fn init(log_view: &gtk::TextView) {
        let log_buffer = log_view
            .get_buffer()
            .expect("Couldn't get textbuffer for `log_view`");
        GLOBAL.with(clone!(log_view, log_buffer => move |global| {
            global.borrow_mut().logger = Some((log_view, log_buffer));
        }));

        let mut builder = filter::Builder::new();
        let filter = if let Ok(gui_log) = std::env::var("GUI_LOG") {
            builder.parse(&gui_log)
        } else {
            builder.filter(Some("hackmud_gui"), log::LevelFilter::Debug)
        }.build();
        log::set_max_level(filter.filter());
        log::set_boxed_logger(Box::new(Logger(filter))).unwrap();
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        self.0.enabled(metadata)
    }

    fn log(&self, record: &log::Record) {
        use log::Level;

        if !self.0.matches(record) {
            return;
        }
        let level_color = match record.level() {
            Level::Error => colors::RED,
            Level::Warn => colors::ALL_COLORS[11], //orange
            Level::Info => colors::LIGHT_GREEN,
            Level::Debug => colors::TEXT_BLUE,
            Level::Trace => colors::WHITE,
        };

        // formatting for colored output. A bit expensive allocating the `String`.
        let colored_item: Vec<(Cow<'static, str>, Option<&'static str>)> = vec![
            (
                format!(
                    "{} ",
                    chrono::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, true)
                ).into(),
                Some(colors::GRAY),
            ),
            ("[".into(), None),
            (
                format!("{}", record.target()).into(),
                Some(colors::ALL_COLORS[43]), // module color
            ),
            (":: ".into(), None),
            (
                format!(
                    "{}:{}",
                    record
                        .file()
                        .and_then(|f| f.rsplit('/').next())
                        .unwrap_or("<unknown>"),
                    record.line().unwrap_or(0),
                ).into(),
                Some(colors::ALL_COLORS[41]), // file color
            ),
            ("] [".into(), None),
            (format!("{}", record.level()).into(), Some(level_color)),
            ("] ".into(), None),
            (format!("{}", record.args()).into(), None),
            ("\n\n".into(), None),
        ];
        glib::idle_add(move || {
            GLOBAL.with(|global| {
                if let Some((ref view, ref buf)) = global.borrow().logger {
                    let mut end = buf.get_end_iter();
                    // Sort of what `insert_markup` does, but without creating tags for each `span`.
                    for &(ref s, ref t) in &colored_item {
                        let start_off = end.get_offset();
                        buf.insert(&mut end, s);
                        if let Some(color) = *t {
                            buf.apply_tag_by_name(color, &buf.get_iter_at_offset(start_off), &end);
                        }
                    }
                    enforce_buffer_limit(buf);
                    // Clicking on the non-text area doesn't scroll to end.
                    if *buf == view.get_buffer().unwrap() {
                        view.scroll_mark_onscreen(&buf.get_selection_bound().unwrap());
                    }
                }
            });
            Continue(false)
        });
    }

    fn flush(&self) {}
}

struct GameData {
    user: Option<String>,
    gc: Option<format::Gc>,
    tutorial: Option<String>,
    completion: format::Completion,
    macros: HashMap<String, format::Macro>,
    chats_chat: bool,
    chats_shell: bool,
    ignored_users: HashSet<String>,
}

impl GameData {
    fn new() -> Self {
        Self {
            user: None,
            gc: None,
            tutorial: None,
            completion: format::Completion::new(),
            macros: HashMap::new(),
            chats_chat: false,
            chats_shell: false,
            ignored_users: HashSet::new(),
        }
    }

    fn reset(&mut self) {
        self.user = None;
        self.gc = None;
        self.tutorial = None;
    }
}

struct GlobalState {
    builder: Option<gtk::Builder>,
    req_tx: Option<mpsc::Sender<String>>,
    exit_tx: Option<oneshot::Sender<()>>,
    handle: Option<JoinHandle<()>>,
    logger: Option<(gtk::TextView, gtk::TextBuffer)>,
    status_anim_cb: Option<u32>,
    shell_anim_cb: Option<u32>,
    is_processing: bool,
    last_cmd: Option<String>,
    game_data: GameData,
}

impl GlobalState {
    fn new() -> Self {
        Self {
            builder: None,
            exit_tx: None,
            req_tx: None,
            handle: None,
            logger: None,
            status_anim_cb: None,
            shell_anim_cb: None,
            // At startup no commands can be sent and spinner is active till response is received.
            is_processing: true,
            last_cmd: None,
            game_data: GameData::new(),
        }
    }

    #[inline]
    fn send_command<S: Into<String>>(&mut self, cmd: S) -> bool {
        // Only errors out if the server violates protocol(send one response per request)/network thread exited.
        if let Some(err) = self.req_tx
            .as_mut()
            .and_then(|tx| tx.try_send(cmd.into()).err())
        {
            error!("Failure sending to `req_tx`: {}", err);
            // make sure we terminate the connection and mark busy.
            self.close_connection();
            false
        } else {
            true
        }
    }

    #[inline]
    fn close_connection(&mut self) {
        self.exit_tx.take().map(|tx| {
            tx.send(())
                .map_err(|_| error!("Failure during send to exit_tx channel"))
        });
        self.handle.take().map(|handle| {
            handle
                .join()
                .map_err(|_| error!("Error while joining network thead handle"))
        });
        // mark busy so no commands can be sent.
        self.is_processing = true;
    }

    fn anim_shell_busy(&mut self, view: &gtk::TextView, iter: &mut gtk::TextIter) {
        let label: gtk::Label = self.builder
            .as_ref()
            .unwrap()
            .get_object("busy_anim")
            .expect("Couldn't get `busy_anim` label");

        // move to beginning of line.
        if !iter.starts_line() {
            let l_off = iter.get_line_offset();
            iter.backward_chars(l_off);
        }
        let loc = view.get_iter_location(iter);
        let (x, y) = view.buffer_to_window_coords(gtk::TextWindowType::Widget, loc.x, loc.y);
        let y = y.min(view.get_allocation().height - font_size(view).1);
        view.move_child(&label, x, y);

        // remove running animation, if any.
        self.shell_anim_cb
            .take()
            .map(|id| label.remove_tick_callback(id));

        // animation text is 10 chars long.
        // once ascending ltr, then descending ltr char-by-char, total 20 frames.
        const FRAMES: usize = 20;
        lazy_static! {
            static ref ANIM_LABEL_TEXT: String = {
                let mut s = String::new();
                s.push_str(&str::repeat(" ", 10));
                s.push_str(&str::repeat(">", 10));
                s.push_str(&str::repeat(" ", 10));
                s
            };
        }
        // 20fps.
        let ival = 1_000_000 / 20;
        // first is the counter, the second is frame-clock incrementing at ival.
        let mut state = (0, 0i64);

        // show the label and add anim callback.
        label.set_visible(true);
        self.shell_anim_cb = Some(label.add_tick_callback(move |label, clock| {
            use gdk::FrameClockExt;

            // remove us and stop callback if we are no longer busy.
            let free = GLOBAL.with(|global| {
                let mut g_ref_mut = global.borrow_mut();
                let busy = g_ref_mut.is_processing;
                if !busy {
                    label.set_visible(false);
                    g_ref_mut
                        .shell_anim_cb
                        .take()
                        .map(|id| label.remove_tick_callback(id));
                }
                !busy
            });
            if free {
                return Continue(false);
            }

            let idx = FRAMES - state.0;
            label.set_label(&format!(
                "<span color=\"{}\">{}</span><span color=\"{}\">{}</span><span \
                 color=\"{}\">{}</span>",
                colors::GRAY,
                &ANIM_LABEL_TEXT[idx..idx + 5],
                colors::LIGHT_GRAY,
                &ANIM_LABEL_TEXT[idx + 5..idx + 9],
                colors::WHITE,
                &ANIM_LABEL_TEXT[idx + 9..idx + 10]
            ));
            let cur_ft = clock.get_frame_time();
            if state.1 == 0 {
                // the first time, record the frame clock.
                state.1 = cur_ft;
            } else if cur_ft >= (state.1 + ival) {
                state.1 = cur_ft;
                state.0 += 1;
                if state.0 >= FRAMES {
                    state.0 = 0;
                }
            }
            Continue(true)
        }));
    }

    #[inline]
    fn set_processing(&mut self, busy: bool) {
        if let Some(ref builder) = self.builder {
            let spinner: gtk::Spinner = builder
                .get_object("processing_spinner")
                .expect("Couldn't get `processing_spinner` gtkspinner");
            if busy {
                spinner.start();
            } else {
                spinner.stop();
            }
        }
        self.is_processing = busy;
    }
}

// declare a new thread local storage key
thread_local!(
    static GLOBAL: RefCell<GlobalState> = RefCell::new(GlobalState::new())
);

fn auto_hide_header(builder: &gtk::Builder) {
    let cmd_view: gtk::TextView = builder
        .get_object("cmd_view")
        .expect("Couldn't get `cmd_view` textview");

    let expander: gtk::Expander = builder
        .get_object("header_expander")
        .expect("Couldn't get `hackmud_header` headerbar");
    cmd_view.connect_focus_out_event(clone!(expander => move |_, _| {
        if !expander.get_expanded() {
            expander.set_expanded(true);
        }
        Inhibit(false)
    }));
    cmd_view.connect_focus_in_event(clone!(expander => move |_, _| {
        if expander.get_expanded() && !expander.has_focus() {
            expander.set_expanded(false);
        }
        Inhibit(false)
    }));
}

// https://github.com/rastersoft/terminus/blob/3fbc68b4b26d66957186d9cfaadc874a0b4e9154/src/PanedPercentage.vala
// Paned window maintains ratio when resizing.
// With size unchanged, changing pane separator establishes new ratio.
fn setup_paned_ratio(paned: &gtk::Paned) {
    const INITIAL_PANE_RATIO: f64 = 0.8;

    #[derive(Clone, Copy)]
    struct PaneState {
        size: i32,
        size_changed: bool,
        pos: i32,
        ratio: f64,
    }

    let state = Rc::new(Cell::new(PaneState {
        size: -1,
        size_changed: false,
        pos: -1,
        ratio: INITIAL_PANE_RATIO,
    }));
    paned.connect_size_allocate(clone!(state => move |_, alloc| {
        let mut pane_state = state.get();
        if pane_state.size != alloc.width {
            pane_state.size = alloc.width;
            pane_state.size_changed = true;
            state.set(pane_state);
        }
    }));
    paned.connect_draw(move |paned, _| {
        let mut pane_state = state.get();
        if pane_state.size_changed {
            pane_state.pos = paned
                .get_property_max_position()
                .min((pane_state.ratio * f64::from(pane_state.size)) as i32);
            paned.set_position(pane_state.pos);
            pane_state.size_changed = false;
        } else if paned.get_position() != pane_state.pos {
            pane_state.pos = paned.get_position();
            pane_state.ratio = f64::from(pane_state.pos) / f64::from(pane_state.size);
        }
        state.set(pane_state);
        Inhibit(false)
    });
}

#[inline]
fn setup_version_label(builder: &gtk::Builder) {
    let version: gtk::Label = builder
        .get_object("version_label")
        .expect("Couldn't get `version_label` label");
    version.set_markup(&format!(
        "<span size=\"small\" color=\"{}\">v.{}</span>",
        colors::ALL_COLORS[2],
        HACKMUD_CLIENT_VERSION,
    ));
}

#[inline]
fn font_size<W: gtk::WidgetExt>(w: &W) -> (i32, i32) {
    use pango::{units_to_double, ContextExt};

    let pango_ctx = w.get_pango_context().unwrap();
    let metrics = pango_ctx
        .get_metrics(
            Some(&pango_ctx.get_font_description().unwrap()),
            Some(&pango_ctx.get_language().unwrap()),
        )
        .unwrap();

    (
        units_to_double(metrics.get_approximate_char_width()).round() as i32,
        units_to_double(metrics.get_ascent() + metrics.get_descent()).round() as i32,
    )
}

const BUFFER_END_MARK: &str = "buffer_end";
const CMD_INSERT_MARK: &str = "cmd_insert";

fn setup_non_editable_text_window(window: &gtk::ScrolledWindow) {
    let view: gtk::TextView = window
        .get_child()
        .expect("Couldn't get textview child from scrolledwindow")
        .downcast()
        .expect("scrolledwindow child must be a textview");
    let buffer = view.get_buffer()
        .expect("Couldn't get textbuffer for textview");
    buffer.create_mark(BUFFER_END_MARK, &buffer.get_end_iter(), false);

    window.connect_edge_reached(clone!(buffer => move |_, pos| {
        if pos == gtk::PositionType::Bottom {
            buffer.place_cursor(&buffer.get_end_iter());
        }
    }));
    view.connect_popup_menu(|_| true);

    // Assuming font_size stays same during lifetime. XXX: Needs to be fixed, if that changes.
    let (font_w, font_h) = font_size(&view);

    // Selection rectangle. buf_x, buf_y is the pinned start of selection. Hence w, h can be (-ve).
    #[derive(Clone, Copy, Debug)]
    struct Selection {
        buf_x: i32,
        buf_y: i32,
        w: i32,
        h: i32,
    };

    impl Selection {
        #[inline]
        fn new() -> Self {
            Self {
                buf_x: -1,
                buf_y: -1,
                w: 0,
                h: 0,
            }
        }

        fn reset(&mut self) {
            self.w = 0;
            self.h = 0;
        }

        #[inline]
        fn queue_draw(&self, view: &gtk::TextView, (font_w, font_h): (i32, i32)) {
            let (mut x, mut y) =
                view.buffer_to_window_coords(gtk::TextWindowType::Text, self.buf_x, self.buf_y);
            if self.w < 0 {
                x += self.w;
            }
            if self.h < 0 {
                y += self.h;
            }
            // Thresold of font_* for the selection adjustment.
            view.queue_draw_area(x, y, self.w.abs() + font_w, self.h.abs() + font_h);
        }
    }

    #[inline]
    fn btn_to_buffer_coords(view: &gtk::TextView, (x, y): (f64, f64), font_w: i32) -> (i32, i32) {
        #[inline]
        fn round_closest(x: i32, n: i32) -> i32 {
            let m = x % n;
            if m > n / 2 {
                x + n - m
            } else {
                x - m
            }
        }

        // #[inline]
        // fn round_down(x: i32, n: i32) -> i32 {
        //     x - (x % n)
        // }

        let (x, y) = view.window_to_buffer_coords(
            gtk::TextWindowType::Text,
            x.round() as i32,
            y.round() as i32,
        );

        let l = view.get_left_margin();
        // was using : round_down(y-t, font_h) + view.get_top_margin()
        // it broke the selection anywhere after fallback font is used(it can have different height). Right now it breaks only inside the fallback font.
        // Since get_line_at_y() doesn't give y at display line(unfortunate), we try to get the start of display line iter and get its bounding box.
        // XXX: this is extremely hacky, might break in unknown ways. Maybe there is a better way?
        let y = if let Some(r) = view.get_iter_at_location(l, y)
            .map(|iter| view.get_iter_location(&iter))
        {
            r.y
        } else {
            view.get_line_at_y(y).1
        };
        (round_closest(x - l, font_w) + l, y)
    }

    // Shared selection state among the callback closures.
    let sel_state = Rc::new(Cell::new(Selection::new()));

    {
        // Resizing invalidates block selection.
        let alloc = Cell::new((-1, -1));
        view.connect_size_allocate(clone!(sel_state => move |_, r| {
            let (w, h) = alloc.get();
            if r.width != w || r.height != h {
                alloc.set((r.width, r.height));

                // Invalidate selection.(`draw()` will happen after allocation)
                let mut sel = sel_state.get();
                sel.reset();
                sel_state.set(sel);
            }
        }));
    }

    let parent = window
        .get_parent()
        .expect("Failed to get parent of scrolledwindow");
    // Ctrl+Click starts a new block selection.
    view.connect_button_press_event(clone!(buffer, sel_state => move |view, btn_ev| {
        use gdk::ModifierType as Mask;

        let mut sel = sel_state.get();
        if (btn_ev.get_state() & Mask::MODIFIER_MASK) == Mask::CONTROL_MASK
            && btn_ev.get_button() == 1
            && btn_ev.get_event_type() == gdk::EventType::ButtonPress
        {
            // Clear normal selection.
            if buffer.get_has_selection() {
                buffer
                    .place_cursor(&buffer.get_iter_at_mark(&buffer.get_selection_bound().unwrap()));
            }
            // Clear the previous block selection.
            if sel.w != 0 || sel.h != 0 {
                sel.queue_draw(view, (font_w, font_h));
            }

            // pointer to buffer coordinates.
            let (x, y) = btn_to_buffer_coords(view, btn_ev.get_position(), font_w);

            view.get_iter_at_location(x, y).map(|iter| buffer.place_cursor(&iter));

            // New selection; record the buffer co-ordinates.
            sel.reset();
            sel.buf_x = x;
            sel.buf_y = y;
            sel_state.set(sel);
            Inhibit(true)
        } else {
            // Invalidate selection.
            if sel.w != 0 || sel.h != 0 {
                sel.queue_draw(view, (font_w, font_h));
                sel.reset();
                sel_state.set(sel);
            }

            // No right-click popup. rather paste to `cmd_view`.
            let btn_no = btn_ev.get_button();
            if btn_no == 3 || btn_no == 2 {
                let mut btn_ev = btn_ev.clone();
                gtk::propagate_event(&parent, &mut btn_ev);
                Inhibit(true)
            } else {
                Inhibit(false)
            }
        }
    }));

    // On release we copy the selection to clipboard.
    view.connect_button_release_event(clone!(sel_state => move |view, btn_ev| {
        use gdk::ModifierType as Mask;

        if btn_ev.get_button() == 1
            && (btn_ev.get_state() & Mask::MODIFIER_MASK).contains(Mask::BUTTON1_MASK)
        {
            let sel = sel_state.get();

            if sel.w != 0 || sel.h != 0 {
                let (x, y) = (
                    sel.buf_x.min(sel.buf_x + sel.w),
                    sel.buf_y.min(sel.buf_y + sel.h),
                );
                let (w, h) = (sel.w.abs(), sel.h.abs());
                // We track the top of the line. This is also adjusted in `draw` callback.
                let lines = h / font_h + 1;
                let mut s = String::from("");
                for i in 0..lines {
                    if let Some(mut start) = view.get_iter_at_location(x, y + i * font_h) {
                        if w == 0 {
                            // The current char is selected, for h != 0. See `draw` where it is adjusted too.
                            // We adjust for non-visible chars.
                            for t in start.get_tags() {
                                if t.get_property_invisible() {
                                    start.forward_visible_cursor_position();
                                    break;
                                }
                            }
                            if let Some(c) = start.get_char() {
                                s.push(c);
                            }
                            s.push('\n');
                        } else if let Some(end) = view.get_iter_at_location(x + w, y + i * font_h) {
                            s.push_str(&start.get_visible_text(&end).unwrap_or_else(|| "".into()));
                            s.push('\n');
                        } else {
                            let mut end = start.clone();
                            end.forward_line();
                            s.push_str(&start.get_visible_text(&end).unwrap_or_else(|| "".into()));
                        }
                    } else {
                        s.push('\n');
                    }
                }
                gtk::Clipboard::get(&gdk::SELECTION_CLIPBOARD).set_text(&s);
            }
        }
        Inhibit(false)
    }));

    // Moving while holding ctrl to change selection.
    view.connect_motion_notify_event(clone!(sel_state => move |view, btn_ev| {
        use gdk::ModifierType as Mask;

        if (btn_ev.get_state() & Mask::MODIFIER_MASK) == (Mask::CONTROL_MASK | Mask::BUTTON1_MASK) {
            let mut sel = sel_state.get();

            // pointer to buffer coordinates.
            let (x, y) = btn_to_buffer_coords(view, btn_ev.get_position(), font_w);

            // calculate stuff for redraw.
            let (old_w, old_h) = (sel.w, sel.h);
            let (min_x, min_y) = (
                x.min(sel.buf_x.min(sel.buf_x + sel.w)),
                y.min(sel.buf_y.min(sel.buf_y + sel.h)),
            );
            // Extend.
            sel.w = x - sel.buf_x;
            sel.h = y - sel.buf_y;
            if old_w != sel.w || old_h != sel.h {
                #[inline]
                fn join(x: i32, y: i32) -> i32 {
                    if (x ^ y).signum() < 0 {
                        (x - y).abs()
                    } else {
                        x.abs().max(y.abs())
                    }
                }

                // (approximate but sufficient) draw union of old+new.
                let (x, y) = view.buffer_to_window_coords(gtk::TextWindowType::Text, min_x, min_y);
                view.queue_draw_area(
                    x,
                    y,
                    join(old_w, sel.w) + font_w,
                    join(old_h, sel.h) + font_h,
                );

                sel_state.set(sel);
            }
            Inhibit(true)
        } else {
            Inhibit(false)
        }
    }));

    // Visually draw the selection_bg color over the selection area.
    // This works (despite returning false) because the textview background is transparent.
    // Ideally we would subclass textview and override `draw_layer`.
    view.connect_draw(clone!(sel_state => move |view, cr| {
        let sel = sel_state.get();
        if sel.w != 0 || sel.h != 0 {
            let (x, mut y) =
                view.buffer_to_window_coords(gtk::TextWindowType::Text, sel.buf_x, sel.buf_y);
            let (mut w, mut h) = (sel.w, sel.h);
            if w == 0 {
                w = font_w;
            }
            // See `button_release_event` callback, where it is adjusted also.
            if h == 0 {
                h = font_h;
            } else if h > 0 {
                h += font_h;
            } else if h < 0 {
                y += font_h;
                h -= font_h;
            }
            cr.save();

            // colors::SELECTION_BG in [0, 1.0].
            cr.set_source_rgba(0.270588, 0.384314, 0.517647, 1.0);
            cr.rectangle(f64::from(x), f64::from(y), f64::from(w), f64::from(h));
            cr.clip_preserve();
            cr.fill();

            cr.restore();
        }
        Inhibit(false)
    }));
}

#[inline]
fn cmd_editor_paste_clipboard(cmd_view: &gtk::TextView, selection: &gdk::Atom) {
    if let Some(s) = gtk::Clipboard::get(selection).wait_for_text() {
        cmd_view.emit_insert_at_cursor(&s.lines().collect::<String>());
    }
}

#[inline]
fn get_columns(view: &gtk::TextView) -> i32 {
    let char_width = font_size(view).0;
    (view.get_allocated_width() - (view.get_left_margin() + view.get_right_margin())) / char_width
}

pub(crate) fn display_columns(view: &gtk::TextView, matches: &[String]) {
    if matches.is_empty() {
        return;
    }
    let buffer = view.get_buffer()
        .expect("couldn't find textbuffer in `display_columns`");
    let col_width = get_columns(view) as usize;
    let sep = 2;
    let fill = matches.iter().map(|s| s.len()).max().unwrap_or(0) + sep;
    let cols = ((col_width + sep) / fill).max(1);
    let lines = (matches.len() + cols - 1) / cols;
    let fill_str = str::repeat(" ", fill);

    let mut pos = buffer.get_end_iter();
    for i in 0..lines {
        for j in 0..cols {
            if let Some(text) = matches.get(i + lines * j) {
                buffer.insert(&mut pos, text);
                let fill = fill - text.len();
                if j < cols - 1 {
                    buffer.insert(&mut pos, &fill_str[0..fill]);
                }
            } else {
                break;
            }
        }
        buffer.insert(&mut pos, "\n");
    }
    buffer.insert(&mut pos, "\n");
    buffer.move_mark_by_name(BUFFER_END_MARK, &pos);
    view.scroll_mark_onscreen(&buffer.get_mark(BUFFER_END_MARK).unwrap());
}

fn setup_cmd_editor(cmd_view: &gtk::TextView) {
    use std::collections::VecDeque;

    cmd_view.connect_popup_menu(|_| true);
    cmd_view.connect_button_press_event(|view, btn_ev| {
        if btn_ev.get_button() == 3 {
            cmd_editor_paste_clipboard(view, &gdk::SELECTION_CLIPBOARD);
            Inhibit(true)
        } else if btn_ev.get_button() == 2 {
            cmd_editor_paste_clipboard(view, &gdk::SELECTION_PRIMARY);
            Inhibit(true)
        } else {
            Inhibit(false)
        }
    });

    // TODO: deal with all consts. Make a toml file for config?
    // TODO: app_dirs + history + macros persistent.
    const HISTORY_LIMIT: usize = 0x800;
    const PROMPT_STRING: &str = ":";

    let buffer = cmd_view
        .get_buffer()
        .expect("Couldn't get textbuffer for `cmd_view` textview");
    {
        buffer.set_text(PROMPT_STRING);
        let end = buffer.get_end_iter();
        buffer.create_mark(CMD_INSERT_MARK, &end, true);
        buffer.apply_tag_by_name("non_editable", &buffer.get_start_iter(), &end);
        let handler_id = Rc::new(RefCell::new(glib::translate::FromGlib::from_glib(
            std::u64::MAX,
        )));
        // A hack to make sure the cursor/selection can never move back past the prompt.
        handler_id.replace(buffer.connect_mark_set(
            clone!(handler_id => move |buffer, iter, mark| {
                if let Some(mark_name) = mark.get_name() {
                    match mark_name.as_str() {
                        "insert" | "selection_bound" => {
                            let prompt_end =
                                buffer.get_iter_at_mark(&buffer.get_mark(CMD_INSERT_MARK).unwrap());
                            if iter < &prompt_end {
                                let id_ref = handler_id.borrow();
                                buffer.block_signal(&id_ref);
                                buffer.move_mark(mark, &prompt_end);
                                buffer.unblock_signal(&id_ref);
                            }
                        }
                        _ => return,
                    }
                }
            }),
        ));
    }

    {
        use gtk::{TextBuffer, TextIter};

        fn connect_after_insert_text<F: Fn(&TextBuffer, &mut TextIter, &str) + 'static>(
            buf: &TextBuffer,
            f: F,
        ) -> glib::signal::SignalHandlerId {
            use std::mem::transmute;
            use glib::translate::{from_glib_borrow, ToGlibPtr};

            unsafe extern "C" fn insert_text_trampoline(
                this: *mut gtk_sys::GtkTextBuffer,
                location: *mut gtk_sys::GtkTextIter,
                text: *mut libc::c_char,
                len: libc::c_int,
                f: glib_sys::gpointer,
            ) {
                let _guard = glib::source::CallbackGuard::new();
                let f: &&(Fn(&TextBuffer, &mut TextIter, &str) + 'static) = transmute(f);
                f(
                    &from_glib_borrow(this),
                    &mut from_glib_borrow(location),
                    std::str::from_utf8(std::slice::from_raw_parts(
                        text as *const u8,
                        len as usize,
                    )).unwrap(),
                )
            }

            unsafe {
                let f: Box<Box<Fn(&TextBuffer, &mut TextIter, &str) + 'static>> =
                    Box::new(Box::new(f));
                connect_after(
                    buf.to_glib_none().0,
                    "insert-text",
                    transmute(insert_text_trampoline as usize),
                    Box::into_raw(f) as *mut _,
                )
            }
        }

        connect_after_insert_text(
            &buffer,
            clone!(cmd_view => move |buffer, (mut iter), _| {
                let mut iter_ = iter.clone();
                iter_.backward_char();
                let c = iter_.get_char().unwrap_or('a');
                if c == '\n' || c == '\r' {
                    buffer.delete(&mut iter_, &mut iter);
                    synthesize_key_press_event(
                        &cmd_view,
                        gdk::enums::key::Return,
                        gdk::ModifierType::empty(),
                    );
                    iter.assign(&buffer.get_end_iter());
                }
            }),
        );
    }

    buffer.connect_changed(|buffer| {
        format::cmd_buffer_highlight_syntax(buffer);
    });

    fn set_text(buffer: &gtk::TextBuffer, text: &str) {
        let mut prompt_end = buffer.get_iter_at_mark(&buffer.get_mark(CMD_INSERT_MARK).unwrap());
        buffer.delete(&mut prompt_end, &mut buffer.get_end_iter());
        if !text.is_empty() {
            buffer.insert(&mut prompt_end, text);
        }
    }

    fn get_text(buffer: &gtk::TextBuffer) -> Option<String> {
        buffer.get_text(
            &buffer.get_iter_at_mark(&buffer.get_mark(CMD_INSERT_MARK).unwrap()),
            &buffer.get_end_iter(),
            false,
        )
    }

    fn process_macro(
        shell_buffer: &gtk::TextBuffer,
        s: &str,
        global: &mut GlobalState,
    ) -> Option<String> {
        #[inline]
        fn escape_arg(s: &str) -> Option<String> {
            let mut escape = false;
            let mut res = String::new();
            for c in s.chars() {
                if escape {
                    escape = false;
                    if c == '\\' {
                        res.push('\\');
                    } else if c == 's' {
                        res.push(' ');
                    } else if c == 't' {
                        res.push('\t');
                    } else {
                        return None;
                    }
                } else if c == '\\' {
                    escape = true;
                } else {
                    res.push(c)
                }
            }
            if escape {
                None
            } else {
                Some(res)
            }
        }

        // macro.
        match format::define_macro(s) {
            Ok((name, macro_)) => {
                global.game_data.completion.add_macro(format!("/{}", name));
                global.game_data.macros.insert(name.into(), macro_);
                format::add_success_text(shell_buffer, "Success");
                None
            }
            Err(e) => if e.kind() == format::MacroErrKind::Invalid {
                let mut split_s = s.split_whitespace();
                let name = &split_s.next().unwrap()[1..]; // 1st char is '/'.
                if !format::is_valid_macroname(name) {
                    format::add_error_text(shell_buffer, "Invalid macro name");
                    None
                } else if let Some(macro_) = global.game_data.macros.get(name) {
                    let mut v = vec![];
                    for (i, arg) in split_s.map(escape_arg).enumerate() {
                        if let Some(s) = arg {
                            v.push(s);
                        } else {
                            format::add_error_text(
                                shell_buffer,
                                &format!(
                                    "Argument: {} contains unescaped '\\'; valid escape \
                                     sequences: \\\\(backslash), \\s(space), \\t(tab)",
                                    i
                                ),
                            );
                            return None;
                        }
                    }
                    match macro_.expand(&v.iter().map(|s| s.as_str()).collect::<Vec<&str>>()) {
                        Ok(s) => {
                            format::add_output(
                                shell_buffer,
                                &format!("`LRunning expanded command:` {}`C..`", s),
                            );
                            Some(s)
                        }
                        Err(s) => {
                            format::add_error_text(shell_buffer, &s.to_string());
                            return None;
                        }
                    }
                } else {
                    format::add_error_text(
                        shell_buffer,
                        &format!("Macro: {} is not defined.", name),
                    );
                    return None;
                }
            } else {
                format::add_error_text(shell_buffer, &format!("{}", e));
                None
            },
        }
    }

    #[derive(Default, Debug)]
    struct EditorState {
        hist_nav_i: usize,
        history: VecDeque<String>,
        // The uncommited current text, when we enter history navigation mode.
        cur_text: Option<String>,
    }

    impl EditorState {
        fn get_current(&self) -> Option<&String> {
            self.cur_text.as_ref()
        }

        fn history_add(&mut self, s: String) {
            if !self.history
                .back()
                .map(|last| s == last.as_str())
                .unwrap_or(false)
            {
                if self.history.len() >= HISTORY_LIMIT {
                    self.history.pop_front();
                }
                self.history.push_back(s);
            }
            self.cur_text = None;
            self.hist_nav_i = self.history.len();
        }

        fn history_move_next(&mut self, buf: &gtk::TextBuffer) {
            if self.hist_nav_i == self.history.len() || self.history.is_empty() {
                return;
            }
            self.hist_nav_i += 1;
            if self.hist_nav_i >= self.history.len() {
                self.hist_nav_i = self.history.len();
                set_text(buf, &self.cur_text.take().unwrap_or_else(|| "".into()));
            } else {
                set_text(
                    buf,
                    self.history
                        .get(self.hist_nav_i)
                        .map(|s| s.as_str())
                        .unwrap_or(""),
                );
            }
        }

        fn history_move_prev(&mut self, buf: &gtk::TextBuffer) {
            if self.history.is_empty() {
                return;
            }
            if self.hist_nav_i == 0 {
                self.hist_nav_i = self.history.len();
                set_text(buf, &self.cur_text.take().unwrap_or_else(|| "".into()));
            } else {
                if self.hist_nav_i == self.history.len() {
                    self.cur_text = get_text(buf);
                }
                self.hist_nav_i -= 1;
                set_text(
                    buf,
                    self.history
                        .get(self.hist_nav_i)
                        .map(|s| s.as_str())
                        .unwrap_or(""),
                );
            }
        }

        fn history_move_to_end(&mut self) {
            if self.hist_nav_i != self.history.len() {
                self.hist_nav_i = self.history.len();
                self.cur_text = None;
            }
        }
    }

    let mut shell_view: Option<gtk::TextView> = None;
    let mut chat_view: Option<gtk::TextView> = None;
    GLOBAL.with(|global| {
        if let Some(ref builder) = global.borrow().builder {
            shell_view = builder.get_object("shell_view");
            // Add `busy_anim` child for `shell_view`.
            let anim_lbl: gtk::Label = builder
                .get_object("busy_anim")
                .expect("Couldn't get `busy_anim` label");
            shell_view
                .as_ref()
                .map(|view| view.add_child_in_window(&anim_lbl, gtk::TextWindowType::Widget, 0, 0));
            chat_view = builder.get_object("chat_view");
        }
    });

    let chat_view = chat_view.expect("Failed to get `chat_view` textview");
    let shell_view = shell_view.expect("Failed to get `shell_view` textview");
    let shell_buffer = shell_view
        .get_buffer()
        .expect("Failed to get textbuffer for `shell_view`");

    let prompt_state = RefCell::new(EditorState::default());
    cmd_view.connect_key_press_event(move |view, key_ev| {
        use gdk::enums::key;
        use gdk::ModifierType as Mask;

        #[inline]
        fn scroll_shell_view(view: &gtk::TextView, scroll: gtk::ScrollType) {
            view.get_parent()
                .map(|w| {
                    w.downcast::<gtk::ScrolledWindow>()
                        .expect("`shell_view` must have a parent scrolledwindow")
                })
                .unwrap()
                .emit_scroll_child(scroll, false);
        }

        match key_ev.get_keyval() {
            k @ key::Page_Up | k @ key::Page_Down => {
                scroll_shell_view(
                    &shell_view,
                    if k == key::Page_Up {
                        gtk::ScrollType::PageUp
                    } else {
                        gtk::ScrollType::PageDown
                    },
                );
            }
            key::Home | key::KP_Home
                if (key_ev.get_state() & Mask::MODIFIER_MASK) == Mask::CONTROL_MASK =>
            {
                scroll_shell_view(&shell_view, gtk::ScrollType::Start)
            }
            key::End | key::KP_End
                if (key_ev.get_state() & Mask::MODIFIER_MASK) == Mask::CONTROL_MASK =>
            {
                scroll_shell_view(&shell_view, gtk::ScrollType::End)
            }
            key::Return | key::KP_Enter => {
                GLOBAL.with(|global| {
                    let mut g_ref_mut = global.borrow_mut();

                    // If already busy we just swallow the Enter key.
                    if g_ref_mut.is_processing {
                        return;
                    }

                    get_text(&buffer).map(|s| {
                        let s = s.trim();
                        if s.is_empty() {
                            return;
                        }
                        // save the command as last_cmd.
                        if s.starts_with('#') {
                            g_ref_mut.last_cmd = Some(s.into());
                        }
                        // history add.
                        prompt_state.borrow_mut().history_add(s.into());
                        // handle client only commands.
                        if s == "#clear" {
                            shell_buffer.set_text("");
                            return;
                        } else if s == "#clear_chat" {
                            chat_view
                                .get_buffer()
                                .expect("Failed to get textbuffer for `chat_view`")
                                .set_text("");
                            return;
                        }

                        // add command to shell.
                        format::add_command(&shell_buffer, &format!(">>{}", s));
                        shell_view
                            .scroll_mark_onscreen(&shell_buffer.get_mark(BUFFER_END_MARK).unwrap());

                        let mut cmd = if s.starts_with('/') {
                            if let Some(s) = process_macro(&shell_buffer, s, &mut g_ref_mut) {
                                // Add quote to keys.
                                format::fmt_command_args(&s)
                            } else {
                                // error while processing macro.
                                return;
                            }
                        } else {
                            // Add quote to keys.
                            format::fmt_command_args(s)
                        };
                        // add column size to scripts.
                        if !cmd.starts_with('#')
                            && (cmd.ends_with('}') || cmd.split_whitespace().count() == 1)
                        {
                            use std::fmt::Write;
                            cmd.write_fmt(format_args!(" {}", get_columns(&shell_view)))
                                .unwrap();
                        }
                        // mark busy.
                        g_ref_mut.set_processing(true);
                        //show busy animation.
                        g_ref_mut.anim_shell_busy(&shell_view, &mut shell_buffer.get_end_iter());
                        if !g_ref_mut.send_command(cmd) {
                            format::add_error_text(&shell_buffer, "-connection terminated-");
                        }
                    });
                    set_text(&buffer, "");
                });
            }
            key::Up | key::KP_Up => {
                if key_ev.get_state() & Mask::MODIFIER_MASK == Mask::CONTROL_MASK {
                    scroll_shell_view(&shell_view, gtk::ScrollType::StepUp)
                } else {
                    prompt_state.borrow_mut().history_move_prev(&buffer);
                }
            }
            key::Down | key::KP_Down => {
                if key_ev.get_state() & Mask::MODIFIER_MASK == Mask::CONTROL_MASK {
                    scroll_shell_view(&shell_view, gtk::ScrollType::StepDown)
                } else {
                    prompt_state.borrow_mut().history_move_next(&buffer);
                }
            }
            key::Escape => {
                // Set text back to the uncommited text(if any) before entering navigation mode.
                set_text(
                    &buffer,
                    prompt_state
                        .borrow()
                        .get_current()
                        .map(|s| s.as_str())
                        .unwrap_or(""),
                );
                // End navigation.
                prompt_state.borrow_mut().history_move_to_end();
            }
            // Paste Clipboard override.
            key::v if (key_ev.get_state() & Mask::MODIFIER_MASK) == Mask::CONTROL_MASK => {
                cmd_editor_paste_clipboard(view, &gdk::SELECTION_CLIPBOARD);
            }
            key::Insert | key::KP_Insert
                if (key_ev.get_state() & Mask::MODIFIER_MASK) == Mask::SHIFT_MASK =>
            {
                cmd_editor_paste_clipboard(view, &gdk::SELECTION_CLIPBOARD);
            }
            k => {
                // We are not in navigation mode anymore.
                prompt_state.borrow_mut().history_move_to_end();
                if k == key::Tab && (key_ev.get_state() & Mask::MODIFIER_MASK).is_empty() {
                    let text = get_text(&buffer).unwrap_or_else(|| "".into());
                    let w_end = text.ends_with(char::is_whitespace);
                    if format::AUTO_COMPLETE.is_match(&text) {
                        GLOBAL.with(|global| {
                            let text = text.trim();
                            let autos = global
                                .borrow_mut()
                                .game_data
                                .completion
                                .complete(text, w_end);
                            if autos.len() == 1 {
                                let m = if w_end {
                                    autos[0].trim_left()
                                } else {
                                    &autos[0]
                                };
                                buffer.insert(&mut buffer.get_end_iter(), m);
                            } else if !autos.is_empty() {
                                display_columns(&shell_view, &autos);
                                let n = text.len();
                                // text can't be empty.
                                let idx = text.find('.').map(|k| n - 1 - k).unwrap_or(n);
                                // Assuming we get sorted list of completions.(currently holds true)
                                let (s1, s2) = (
                                    &autos.first().unwrap()[idx..],
                                    &autos.last().unwrap()[idx..],
                                );
                                // insert the common prefix between first and last completion.
                                if let Some(((n, _), _)) = s2.char_indices()
                                    .zip(s1.chars())
                                    .skip_while(|&((_, c), c1)| c == c1)
                                    .next()
                                {
                                    buffer.insert(&mut buffer.get_end_iter(), &s1[..n]);
                                } else {
                                    buffer.insert(&mut buffer.get_end_iter(), s1);
                                }
                            }
                        });
                    }
                    // swallow Tab key.
                    return Inhibit(true);
                }
                return Inhibit(false);
            }
        }
        Inhibit(true)
    });
}

/* `badge` label update functions */

#[inline]
fn set_default_badge(label: &gtk::Label, user: &str) {
    use std::fmt::Write;

    let mut s = format!(
        "<span color=\"{user}\">sys</span>.<span color=\"{script}\">specs</span> &amp;&amp; <span \
         color=\"{user}\">accts</span>.<span color=\"{script}\">balance</span>",
        user = colors::USER_NAME,
        script = colors::SCRIPT_NAME
    );
    if !user.is_empty() {
        s.write_fmt(format_args!("\n\n-{}-", user)).unwrap();
    }
    label.set_markup(&s);
}

fn set_badge_specs(label: &gtk::Label, val: &serde_json::Value, user: &Option<String>) {
    use std::fmt::Write;
    use serde_json::Value;

    match *val {
        Value::String(ref s) => {
            let text = label.get_label().unwrap_or_else(|| "".into());
            let mut gc_str = String::new();
            if let Some(l) = text.lines().nth(4) {
                gc_str = l.splitn(2, '|').nth(1).unwrap_or("").into();
            }
            let lines: Vec<&str> = s.lines().collect();
            // 4 lines of graphic + username(tier)
            if lines.len() < 5 {
                let mut text = String::from("no upgrades present");
                if let Some(ref u) = *user {
                    text.write_fmt(format_args!("\n\n-{}-", u)).unwrap();
                }
                label.set_label(&text);
            } else {
                let mut text = String::new();
                // write the ascii graphic.
                for l in lines.iter().take(4) {
                    text.push_str(l);
                    text.push('\n');
                }
                lazy_static! {
                    static ref USER_SPEC: regex::Regex = regex::Regex::new(r"(?-u)`([0-9a-zA-Z])(\w+?)`\s*\(`([0-9a-zA-Z])(\w+?)`\)").unwrap();
                }
                // write the user(tier) with color.
                if let Some(c) = USER_SPEC.captures(lines[4]) {
                    c.get(1).map(|m| {
                        text.write_fmt(format_args!(
                            "<span color=\"{}\">",
                            format::char_to_color_name(m.as_str().chars().next().unwrap())
                        ))
                    });
                    c.get(2).map(|m| text.push_str(m.as_str()));
                    text.push_str("</span> ");
                    c.get(3).map(|m| {
                        text.write_fmt(format_args!(
                            "(<span color=\"{}\">",
                            format::char_to_color_name(m.as_str().chars().next().unwrap())
                        ))
                    });
                    c.get(4).map(|m| text.push_str(m.as_str()));
                    text.push_str("</span>) ");
                }
                // write the gc_str.
                if !gc_str.is_empty() {
                    text.push('|');
                    text.push_str(&gc_str);
                }
                label.set_label(&text);
            }
        }
        Value::Object(ref m) => {
            let mut spec: String = if m.get("tutorial")
                .map(|v| v.as_str().unwrap_or("").contains("trust.sentience"))
                .unwrap_or(false)
            {
                "prove sentience".into()
            } else {
                "system not initialized".into()
            };
            if let Some(ref u) = *user {
                spec.write_fmt(format_args!("\n\n-{}-", u)).unwrap();
            }
            label.set_label(&spec);
        }
        _ => {}
    }
    // Redraw the right-half of paned.
    label
        .get_parent()
        .unwrap()
        .get_parent()
        .unwrap()
        .queue_draw();
}

fn set_badge_currency(label: &gtk::Label, gc: format::Gc) {
    fn color_summary(gc: format::Gc) -> String {
        use std::fmt::Write;

        let summary = gc.summarize();
        let mut s = String::new();
        // numbers white.
        s.write_fmt(format_args!("<span color=\"{}\">", colors::WHITE))
            .unwrap();
        let mut n = 0;
        for (i, c) in summary.char_indices() {
            if let Some(k) = "QTBMK".find(c) {
                s.push_str(&summary[n..i]);
                n = i + 1;
                s.write_fmt(format_args!(
                    "<span color=\"{}\">{}</span>",
                    colors::CURRENCY_UNIT_COLORS[k],
                    c
                )).unwrap();
            } else if c == 'G' {
                s.write_fmt(format_args!(
                    "<span color=\"{}\">GC</span>",
                    colors::LIGHT_GRAY
                )).unwrap();
                break;
            }
        }
        s.push_str("</span>");
        s
    }

    let text = label.get_label().unwrap_or_else(|| "".into());
    let lines: Vec<&str> = text.lines().collect();
    if lines.len() < 2 {
        return;
    }
    // We have a username line.
    if let Some(line) = lines.last() {
        let mut s = String::new();
        for l in lines.iter().take(lines.len() - 1) {
            s.push_str(l);
            s.push('\n');
        }
        let mut split_iter = line.splitn(2, '|');
        s.push_str(split_iter.next().unwrap_or(""));
        s.push_str("| ");
        s.push_str(&color_summary(gc));
        label.set_label(&s);
        // Redraw the right-half of paned.
        label
            .get_parent()
            .unwrap()
            .get_parent()
            .unwrap()
            .queue_draw();
    }
}

/* ... */

/* `status` label update functions */

#[inline]
fn set_default_status(label: &gtk::Label) {
    label.set_markup(&format!(
        "<span color=\"{}\">sys</span>.<span color=\"{}\">status</span>",
        colors::USER_NAME,
        colors::SCRIPT_NAME
    ));
}

fn clear_hardline_status(label: &gtk::Label, cb_id: Option<u32>) {
    cb_id.map(|id| label.remove_tick_callback(id));
    let mut text = String::new();
    label
        .get_label()
        .map(|s| text.push_str(s.lines().next().unwrap_or("")));
    label.set_label(&text);
    // Redraw the right-half of paned.
    label
        .get_parent()
        .unwrap()
        .get_parent()
        .unwrap()
        .queue_draw();
}

#[inline]
fn set_status(
    label: &gtk::Label,
    breach: Option<bool>,
    hardline: Option<f64>,
    cb_id: &mut Option<u32>,
) {
    use std::fmt::Write;

    let mut text = breach
        .map(|b| {
            if b {
                format!("<span color=\"{}\">System Breach</span>", colors::RED)
            } else {
                "System Clear".into()
            }
        })
        .unwrap_or_else(|| {
            label
                .get_label()
                .and_then(|s| s.lines().next().map(|s| s.into()))
                .unwrap_or_else(|| "".into())
        });
    if let Some(t) = hardline {
        text.push('\n');
        text.write_fmt(format_args!(
            "<span color=\"{2}\">-Hardline Active(<span color=\"{0}\">{1:06.2}</span>)-</span>",
            colors::YELLOW,
            t,
            colors::WHITE,
        )).unwrap();
        // remove previous animation callback.
        cb_id.take().map(|id| label.remove_tick_callback(id));
        // attach a new callback with the timer.
        *cb_id = Some(anim_hardline_status(label, t));
    } else {
        label.get_label().and_then(|s| {
            s.lines().nth(1).map(|s| {
                text.push('\n');
                text.push_str(s);
            })
        });
    }
    label.set_label(&text);
    // Redraw the right-half of paned.
    label
        .get_parent()
        .unwrap()
        .get_parent()
        .unwrap()
        .queue_draw();
}

/* ... */

#[inline]
fn anim_hardline_status(label: &gtk::Label, remaining: f64) -> u32 {
    lazy_static! {
        static ref HARDLINE_TIMER: regex::Regex = regex::Regex::new(r"\((<.*?>)(?:[\d|\.]+)(<.*?>)\)").unwrap();
    }
    // 30fps.
    let ival = 1_000_000 / 30;
    // first is the timer, the second is frame-clock incrementing at ival.(both in micro-secs).
    let mut state = ((remaining * 1e6) as i64, 0i64);
    // attach the new callback.
    label.add_tick_callback(move |w, clock| {
        use gdk::FrameClockExt;
        if state.0 >= 0 {
            let cur_ft = clock.get_frame_time();
            if state.1 == 0 {
                // the first time, record the frame clock.
                state.1 = cur_ft;
            } else if cur_ft >= (state.1 + ival) {
                state.0 -= cur_ft - state.1;
                state.1 = cur_ft;
                // update text.
                let mut text = String::new();
                w.get_label().map(|s| {
                    text = HARDLINE_TIMER
                        .replace(
                            &s,
                            format!("(${{1}}{:06.2}${{2}})", (state.0 as f64 / 1e6)).as_str(),
                        )
                        .into();
                });
                w.set_label(&text);
            }
            glib::Continue(true)
        } else {
            // remove us.
            GLOBAL.with(|global| {
                global
                    .borrow_mut()
                    .status_anim_cb
                    .take()
                    .map(|id| w.remove_tick_callback(id))
            });
            // Remove hardline status.
            let mut text = String::new();
            w.get_label().map(|s| {
                text.push_str(s.lines().next().unwrap_or(""));
            });
            w.set_label(&text);
            // Redraw right half of paned.
            w.get_parent().unwrap().get_parent().unwrap().queue_draw();
            glib::Continue(false)
        }
    })
}

#[inline]
// Add all the colors as separate tags to tag-table.
fn setup_shared_tag_table(builder: &gtk::Builder) {
    let shared_tags: gtk::TextTagTable = builder
        .get_object("shared_tag_table")
        .expect("Couldn't get texttagtable `shared_tag_table`");

    for c in colors::ALL_COLORS.iter() {
        let tag = gtk::TextTag::new(Some(*c));
        tag.set_property_foreground(Some(c));
        shared_tags.add(&tag);
    }
    format::init();
}

fn build_hackmud_ui(builder: &gtk::Builder) {
    // When connected we create the `hackmud_window`.
    let app_window: gtk::ApplicationWindow = builder
        .get_object("hackmud_window")
        .expect("Couldn't get `hackmud_window` window");
    builder.get_application().as_ref().map(|app| {
        // destroy the `address_window`.
        for w in app.get_windows() {
            w.destroy();
        }
        app.add_window(&app_window);
    });

    // Setup the tag table used by all the buffers in glade file.
    setup_shared_tag_table(builder);

    setup_non_editable_text_window(&builder
        .get_object("log_window")
        .expect("Couldn't get `log_window` scrolledwindow"));

    setup_non_editable_text_window(&builder
        .get_object("shell_window")
        .expect("Couldn't get `shell_window` scrolledwindow"));

    setup_non_editable_text_window(&builder
        .get_object("chat_window")
        .expect("Couldn't get `chat_window` scrolledwindow"));

    set_default_badge(
        &builder
            .get_object("badge")
            .expect("Couldn't get `badge` label"),
        "",
    );

    set_default_status(&builder
        .get_object("status")
        .expect("Couldn't get `status` label"));

    // Register logger.
    Logger::init(&builder
        .get_object("log_view")
        .expect("Couldn't get `log_view` textview"));

    // Setup `cmd_view` line-editor.
    let cmd_view: gtk::TextView = builder
        .get_object("cmd_view")
        .expect("Couldn't get `cmd_view` textview");
    setup_cmd_editor(&cmd_view);

    // Setup paned.
    let paned: gtk::Paned = builder
        .get_object("client_paned")
        .expect("Couldn't get `client_paned` paned");

    setup_paned_ratio(&paned);
    // All the leaves(except `cmd_view`) of `paned` propagate `right-click/middle-click` events to parent.
    paned.connect_button_press_event(clone!(cmd_view => move |_, btn_ev| {
        if btn_ev.get_button() == 3 {
            cmd_editor_paste_clipboard(&cmd_view, &gdk::SELECTION_CLIPBOARD);
            Inhibit(true)
        } else if btn_ev.get_button() == 2 {
            cmd_editor_paste_clipboard(&cmd_view, &gdk::SELECTION_PRIMARY);
            Inhibit(true)
        } else {
            Inhibit(false)
        }
    }));

    // Show the version at bottom corner.
    setup_version_label(builder);

    auto_hide_header(builder);

    app_window.connect_delete_event(|w, _| {
        exit_network_thread();
        w.destroy();
        Inhibit(false)
    });
    // show the window.
    app_window.fullscreen();
    app_window.show_all();
}

fn on_connect() -> glib::Continue {
    let mut builder: Option<gtk::Builder> = None;
    GLOBAL.with(|global| {
        builder = global.borrow().builder.clone();
    });
    if let Some(ref builder) = builder {
        build_hackmud_ui(builder);
    }
    glib::Continue(false)
}

#[inline]
fn display_banner(buffer: &gtk::TextBuffer) {
    const BANNER: &str = r"
 _                   _                           _
| |__    __ _   ___ | | __ _ __ ___   _   _   __| |
| '_ \  / _` | / __|| |/ /| '_ ` _ \ | | | | / _` |
| | | || (_| || (__ |   < | | | | | || |_| || (_| |
|_| |_| \__,_| \___||_|\_\|_| |_| |_| \__,_| \__,_|

";
    format::add_colored_text(buffer, BANNER, colors::ELEET_ORANGE);
}

#[inline]
fn display_help(buffer: &gtk::TextBuffer, msg: &str) {
    let s = String::from("Use #help to see detailed usage.\n") + msg;
    format::add_output(buffer, &s);
}

// Must be called on gtk main thread.
fn process_response(global: &mut GlobalState, response: Response, unblock: bool) {
    use serde_json::Value;

    fn process_script_result(
        global: &mut GlobalState,
        mut result: hackmud_client::ScriptResult,
        buffer: &gtk::TextBuffer,
        status_lbl: &gtk::Label,
        badge_lbl: &gtk::Label,
    ) -> usize {
        fn get_opt_str(m: &mut Value, k: &str) -> Option<String> {
            m.get_mut(k).and_then(|val| match val.take() {
                Value::String(s) => Some(s),
                _ => None,
            })
        }

        let mut lines = 0;
        let script_name = result.script_name.clone();
        match script_name.as_ref() {
            "accts.balance" => {
                if let Some(gc) = result
                    .retval
                    .as_str()
                    .and_then(|s| s.parse::<format::Gc>().ok())
                {
                    global.game_data.gc = Some(gc);
                    set_badge_currency(badge_lbl, gc);
                }
                format::pretty_print(buffer, result.retval, &mut lines);
            }
            "accts.transactions" => {
                if let Some(msg) = result.retval.get("msg").and_then(|v| v.as_str()) {
                    format::add_output(buffer, msg);
                }
                if let Some(tx) = result.retval.get_mut("transactions").map(|v| v.take()) {
                    format::pretty_print(buffer, tx, &mut lines);
                }
            }
            "sys.upgrades" => {
                let mut done = false;
                {
                    if let Some(ref mut retval) = result.retval.as_object_mut() {
                        if retval.contains_key("msg") && retval.contains_key("upgrades") {
                            format::add_output(
                                buffer,
                                retval.get("msg").and_then(|v| v.as_str()).unwrap_or(""),
                            );
                            format::pretty_print(
                                buffer,
                                retval
                                    .get_mut("upgrades")
                                    .map(|v| v.take())
                                    .unwrap_or(serde_json::Value::Null),
                                &mut lines,
                            );
                            done = true;
                        }
                    }
                }
                if !done {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "sys.access_log" | "sys.upgrade_log" => {
                let mut done = false;
                {
                    if let Some(ref mut retval) = result.retval.as_object_mut() {
                        if retval.contains_key("msg") && retval.contains_key("logs") {
                            format::add_output(
                                buffer,
                                retval.get("msg").and_then(|v| v.as_str()).unwrap_or(""),
                            );
                            format::pretty_print(
                                buffer,
                                retval
                                    .get_mut("logs")
                                    .map(|v| v.take())
                                    .unwrap_or(Value::Null),
                                &mut lines,
                            );
                            done = true;
                        }
                    }
                }
                if !done {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "autos.reset" => {
                let mut done = false;
                {
                    if let Some(auto_str) =
                        result.retval.get("autocomplete").and_then(|v| v.as_str())
                    {
                        done = serde_json::from_str(auto_str)
                            .map(|autos| {
                                global.game_data.completion.assign(autos);
                                format::add_info_text(buffer, "-autocompletes reset-");
                                true
                            })
                            .unwrap_or(false);
                    }
                }
                if !done {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "gui.chats" => {
                if result.retval.get("shell").is_some() || result.retval.get("chat").is_some() {
                    let data = result.retval;
                    global.game_data.chats_shell =
                        data.get("shell").and_then(|v| v.as_bool()).unwrap_or(false);
                    global.game_data.chats_chat =
                        data.get("chat").and_then(|v| v.as_bool()).unwrap_or(false);
                    format::add_success_text(buffer, "Success");
                } else {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "gui.quiet" => {
                if result.retval.get("add").is_some() || result.retval.get("remove").is_some()
                    || result.retval.get("clear").is_some()
                {
                    get_opt_str(&mut result.retval, "add").map(|s| {
                        global.game_data.completion.remove_user(&s);
                        global.game_data.ignored_users.insert(s);
                    });
                    get_opt_str(&mut result.retval, "remove").map(|s| {
                        global.game_data.ignored_users.remove(&s);
                    });
                    if result
                        .retval
                        .get("clear")
                        .and_then(|v| v.as_bool())
                        .unwrap_or(false)
                    {
                        global.game_data.ignored_users.clear();
                    }
                    format::add_success_text(buffer, "Success");
                    get_opt_str(&mut result.retval, "msg").map(|s| {
                        format::add_output(buffer, &s);
                    });
                } else {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "kernel.hardline" => {
                let ok = result
                    .retval
                    .get("ok")
                    .and_then(|v| v.as_bool())
                    .unwrap_or(false);
                if ok {
                    let data = result.retval;
                    if data.get("dc").is_some() {
                        clear_hardline_status(status_lbl, global.status_anim_cb.take());
                        format::add_info_text(buffer, "-hardline disconnected-");
                    } else if let Some(v) = data.get("remaining") {
                        set_status(
                            status_lbl,
                            None,
                            Some(v.as_f64().unwrap_or(0.0)),
                            &mut global.status_anim_cb,
                        );
                        format::add_error_text(buffer, "-hardline active-");
                    } else {
                        format::add_output(buffer, "Activate hardline with { activate: true }");
                    }
                } else {
                    lines = format::ScriptResult::from(result).write_pretty(buffer);
                }
            }
            "sys.status" => {
                let mut data = result.retval;
                let breach = data.get("breach").and_then(|v| v.as_bool());
                // Update breach status.
                set_status(status_lbl, breach, None, &mut global.status_anim_cb);
                let hardline = data.get("remaining").and_then(|f| f.as_f64());
                if let Some(hardline) = hardline {
                    if data.get("hardline_failure")
                        .and_then(|v| v.as_bool())
                        .unwrap_or(false)
                    {
                        format::add_error_text(buffer, "-hardline failure-");
                        format::add_error_text(buffer, "-system is breached-");
                    } else {
                        format::add_error_text(buffer, "-hardline active-");
                        // Update hardline status.
                        set_status(status_lbl, None, Some(hardline), &mut global.status_anim_cb);
                    }
                }
                global.game_data.tutorial = get_opt_str(&mut data, "tutorial");
                let tut_ref = global.game_data.tutorial.as_ref();
                if breach.unwrap_or(false) {
                    format::add_error_text(buffer, "-system is breached-");
                } else if tut_ref.map(|s| !s.is_empty()).unwrap_or(false) {
                    display_help(buffer, tut_ref.map(|s| s.as_str()).unwrap_or(""));
                } else {
                    format::add_info_text(buffer, "-system status nominal-");
                }
            }
            "sys.loc" => {
                if result.retval == Value::Null {
                    format::add_output(buffer, "Your sys.loc has not yet been assigned");
                } else {
                    format::pretty_print(buffer, result.retval, &mut lines);
                }
            }
            "sys.specs" => {
                set_badge_specs(badge_lbl, &result.retval, &global.game_data.user);
                format::pretty_print(buffer, result.retval, &mut lines);
            }
            _ => {
                lines = format::ScriptResult::from(result).write_pretty(buffer);
            }
        }
        lines
    }

    let (shell_view, status_lbl, badge_lbl) = {
        let builder_ref = global.builder.as_ref().unwrap();
        let shell_view: gtk::TextView = builder_ref
            .get_object("shell_view")
            .expect("Failed to get textview `shell_view`");
        let status_lbl: gtk::Label = builder_ref
            .get_object("status")
            .expect("Failed to status label");
        let badge_lbl: gtk::Label = builder_ref
            .get_object("badge")
            .expect("Failed to badge label");
        (shell_view, status_lbl, badge_lbl)
    };
    let buffer = shell_view
        .get_buffer()
        .expect("Failed to get textbuffer for `shell_view`");

    let start_offset = buffer.get_end_iter().get_offset();
    let mut lines = 0;
    let view_lines = {
        let rect = shell_view.get_visible_rect();
        shell_view.get_line_at_y(rect.y + rect.height).0.get_line()
            - shell_view.get_line_at_y(rect.y).0.get_line()
    };

    trace!("Response received: {:?}", response);
    if !response.success {
        format::add_error_text(&buffer, "Failure");
    }

    match response.data {
        ResponseData::Empty => {
            if response.success {
                format::add_success_text(&buffer, "Success");
            }
        }
        ResponseData::Error(s) | ResponseData::Message(s) => {
            if !s.is_empty() {
                format::add_output(&buffer, &s);
            }
        }
        ResponseData::AuthError(s) => {
            clear_hardline_status(&status_lbl, global.status_anim_cb.take());
            status_lbl.set_label(&format!(
                "<span color=\"{}\">Auth Failure</span>",
                colors::RED
            ));
            format::add_error_text(&buffer, &s);
        }
        ResponseData::Status(status) => {
            if !status.authenticated {
                status_lbl.set_label(&format!(
                    "<span color=\"{}\">Auth Failure</span>",
                    colors::RED
                ));
            } else {
                status_lbl.get_text().map(|s| {
                    if s.contains("Failure") {
                        set_default_status(&status_lbl);
                    }
                });
            }
            format::add_output(
                &buffer,
                &format!(
                    "{{ user: \"{}\", authenticated: {}, notifications: {} }}",
                    status.user.as_ref().map(|s| s.as_str()).unwrap_or(""),
                    status.authenticated,
                    status.notifications
                ),
            )
        }
        ResponseData::ClientData(data) => {
            use std::iter::FromIterator;

            global.game_data.completion.assign(data.autocomplete);
            for u in &data.gui_quiet {
                global.game_data.completion.remove_user(u);
            }
            global.game_data.chats_chat = data.gui_chats_chat;
            global.game_data.chats_shell = data.gui_chats_shell;
            global.game_data.ignored_users = HashSet::from_iter(data.gui_quiet);
            format::add_info_text(&buffer, "-client data reset-");
        }
        ResponseData::UserInfo(info) => {
            let print_users = |v: &Vec<String>, max: u32, descr: &str| {
                let mut s = String::new();
                for (i, u) in v.iter().enumerate() {
                    s.push_str("`C");
                    s.push_str(u);
                    s.push('`');
                    if i != v.len() - 1 {
                        s.push_str(", ");
                    } else {
                        s.push(' ');
                    }
                }
                format::add_output(
                    &buffer,
                    &format!("{} users: {}({}/{})", descr, s, v.len(), max),
                );
            };
            format::add_output(
                &buffer,
                &format!(
                    "Active User: `C{}`",
                    global
                        .game_data
                        .user
                        .as_ref()
                        .map(|s| s.as_str())
                        .unwrap_or("NOT SET")
                ),
            );
            print_users(&info.available, info.max_available, "Available");
            print_users(&info.retired, info.max_retired, "Retired");
        }
        ResponseData::UploadResult(res) => {
            format::pretty_print(&buffer, serde_json::to_value(res).unwrap(), &mut lines);
        }
        ResponseData::ScriptResult(result) => {
            lines = process_script_result(global, result, &buffer, &status_lbl, &badge_lbl);
        }
    }

    match global.last_cmd.take() {
        Some(ref s) if s.starts_with('#') => {
            let mut words = s.split_whitespace();
            match words.next().unwrap_or("") {
                "#help" if response.success => {
                    // show additional client commands.
                    format::add_output(&buffer,
                    r#"Client-only Commands:
  #clear : Clears the output window.
  #clear_chat : Clears the chat window.
  `C/``hMACRONAME` `A=` `hCOMMAND` : Define a macro `hMACRONAME` assigned to `hCOMMAND`.
      `hCOMMAND` can contain template `N{n}` to denote positional argument `hn` to `hMACRONAME`.
  `C/``hMACRONAME` `hARGS` : Expand (just a string substitute) the `hMACRONAME` with given positional `hARGS` and execute the command.
      `hARGS` is whitespace separated but can contain escape sequence: (\t|\s|\\).
"#)
                }
                "#auth" => {
                    if response.success {
                        status_lbl.get_text().map(|s| {
                            if s.contains("Failure") {
                                set_default_status(&status_lbl);
                            }
                        });
                    } else {
                        clear_hardline_status(&status_lbl, global.status_anim_cb.take());
                        status_lbl.set_label(&format!(
                            "<span color=\"{}\">Auth Failure</span>",
                            colors::RED
                        ));
                    }
                }
                "#notifications" if response.success => {
                    let on = words.next().map(|s| s == "on").unwrap_or(false);
                    if on {
                        format::add_output(&buffer, "Checking status..");
                        shell_view.scroll_mark_onscreen(&buffer.get_mark(BUFFER_END_MARK).unwrap());
                        // queue "#status" command.
                        global.last_cmd = Some("#status".into());
                        if !global.send_command("#status") {
                            format::add_error_text(&buffer, "-connection terminated-");
                        }
                        // we are still busy.
                        return;
                    }
                }
                "#status" if response.success => {
                    // queue "sys.status" command.
                    if !global.send_command("sys.status") {
                        format::add_error_text(&buffer, "-connection terminated-");
                    }
                    // we are still busy.
                    return;
                }
                "#user" if response.success => {
                    let user: String = words.next().unwrap_or("").into();
                    let b = global
                        .game_data
                        .user
                        .as_ref()
                        .map(|s| s != user.as_str())
                        .unwrap_or(true);
                    // If user changed successfully.
                    if b {
                        clear_hardline_status(&status_lbl, global.status_anim_cb.take());
                        set_default_status(&status_lbl);
                        set_default_badge(&badge_lbl, &user);
                        // reset and set new user.
                        global.game_data.reset();
                        global.game_data.user = Some(user);

                        format::add_success_text(&buffer, "Success");
                        format::add_output(
                            &buffer,
                            "Enabling notifications..(disable them with `C#notifications` `Noff`).",
                        );
                        shell_view.scroll_mark_onscreen(&buffer.get_mark(BUFFER_END_MARK).unwrap());
                        // queue "notifications" command.
                        global.last_cmd = Some("#notifications on".into());
                        if !global.send_command("#notifications on") {
                            format::add_error_text(&buffer, "-connection terminated-");
                        }
                        // we are still busy.
                        return;
                    } else {
                        format::add_output(
                            &buffer,
                            &format!("User `C{}` is already active", &user),
                        );
                    }
                }
                "#retire_user" if response.success => {
                    let user: String = words.next().unwrap_or("").into();
                    let b = global
                        .game_data
                        .user
                        .as_ref()
                        .map(|s| s == user.as_str())
                        .unwrap_or(false);
                    // If retiring the current user.
                    if b {
                        clear_hardline_status(&status_lbl, global.status_anim_cb.take());
                        set_default_status(&status_lbl);
                        set_default_badge(&badge_lbl, "");
                        // reset.
                        global.game_data.reset();
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    let mut end = buffer.get_end_iter();
    if end.get_offset() != start_offset {
        buffer.insert(&mut end, "\n");
    }
    enforce_buffer_limit(&buffer);
    if lines as i32 >= view_lines {
        gtk::idle_add(move || {
            shell_view.scroll_to_iter(
                &mut buffer.get_iter_at_offset(start_offset),
                0.01,
                true,
                0.0,
                0.0,
            );
            glib::Continue(false)
        });
    } else {
        shell_view.scroll_mark_onscreen(&buffer.get_mark(BUFFER_END_MARK).unwrap());
    }

    // mark ready, if unblock = true.
    global.set_processing(!unblock);
}

fn process_notification(global: &mut GlobalState, note: Notification) {
    #[inline]
    fn time_string(t: u64) -> String {
        use chrono::TimeZone;
        chrono::Utc
            .timestamp(t as i64, 0)
            .format("%H%M")
            .to_string()
    }

    #[inline]
    fn backspace(buf: &gtk::TextBuffer, iter: &mut gtk::TextIter) {
        let mut start = iter.clone();
        start.backward_char();
        buf.delete(&mut start, iter);
    }

    #[inline]
    fn print_chat(
        buf: &gtk::TextBuffer,
        t: u64,
        origin: &str,
        user: &str,
        msg: &str,
        origin_color: &'static str,
        ignore: bool,
    ) {
        if ignore {
            format::add_colored_text(
                buf,
                &format!("{} {} {} :::{}:::", time_string(t), origin, user, msg),
                colors::IGNORE,
            );
        } else {
            format::add_colored_text(buf, &format!("{} ", time_string(t)), colors::GRAY);
            format::add_colored_text(buf, &format!("{} ", origin), origin_color);
            format::add_colored_text(buf, &format!("{} ", user), format::get_user_color(user));
            format::add_colored_text(buf, ":::", colors::IGNORE);
            format::add_output(buf, msg);
            // Delete the newline.
            backspace(buf, &mut buf.get_end_iter());
            format::add_colored_text(buf, ":::\n", colors::IGNORE);
        }
    }

    use NotificationData::*;

    let (shell_view, chat_view, status_lbl, badge_lbl) = {
        let builder_ref = global.builder.as_ref().unwrap();
        let shell_view: gtk::TextView = builder_ref
            .get_object("shell_view")
            .expect("Failed to get textview `shell_view`");
        let chat_view: gtk::TextView = builder_ref
            .get_object("chat_view")
            .expect("Failed to get textview `chat_view`");
        let status_lbl: gtk::Label = builder_ref
            .get_object("status")
            .expect("Failed to status label");
        let badge_lbl: gtk::Label = builder_ref
            .get_object("badge")
            .expect("Failed to badge label");
        (shell_view, chat_view, status_lbl, badge_lbl)
    };
    let buffer = shell_view
        .get_buffer()
        .expect("Failed to get textbuffer for `shell_view`");
    let chat_buffer = chat_view
        .get_buffer()
        .expect("Failed to get textbuffer for `chat_view`");
    let start_offset = buffer.get_end_iter().get_offset();

    trace!("Notification received: {:?}", note);
    match note.data {
        Exit => {
            global.close_connection();
            format::add_error_text(&buffer, "-connection terminated-");
        }
        AutoComplete { autos, remove } => {
            global.game_data.completion.merge(autos, remove);
            format::add_info_text(
                &buffer,
                if !remove {
                    "-autocompletes added-"
                } else {
                    "-autocompletes removed-"
                },
            );
        }
        LocCaller(s) => {
            format::add_output(&buffer, &format!("sys.loc called from {}", s));
        }
        ChatJoin { user, channel, msg } | ChatLeave { user, channel, msg } => {
            let ignore = global.game_data.ignored_users.contains(&user);
            if global.game_data.chats_shell {
                print_chat(
                    &buffer,
                    note.timestamp,
                    &channel,
                    &user,
                    &msg,
                    colors::CYAN,
                    ignore,
                );
            }
            if global.game_data.chats_chat {
                print_chat(
                    &chat_buffer,
                    note.timestamp,
                    &channel,
                    &user,
                    &msg,
                    colors::CYAN,
                    ignore,
                );
                chat_buffer.insert(&mut chat_buffer.get_end_iter(), "\n");
                chat_view.scroll_mark_onscreen(&chat_buffer.get_selection_bound().unwrap());
            }
        }
        ChatSend { user, channel, msg } => {
            let msg = format::filter_chat_msg(&msg);
            let ignore = global.game_data.ignored_users.contains(&user);
            if global.game_data.chats_shell {
                print_chat(
                    &buffer,
                    note.timestamp,
                    &channel,
                    &user,
                    &msg,
                    colors::MAGENTA,
                    ignore,
                );
            }
            if global.game_data.chats_chat {
                print_chat(
                    &chat_buffer,
                    note.timestamp,
                    &channel,
                    &user,
                    &msg,
                    colors::MAGENTA,
                    ignore,
                );
                chat_buffer.insert(&mut chat_buffer.get_end_iter(), "\n");
                chat_view.scroll_mark_onscreen(&chat_buffer.get_selection_bound().unwrap());
            }
        }
        ChatTell { user, dir, msg } => {
            let msg = format::filter_chat_msg(&msg);
            let ignore = global.game_data.ignored_users.contains(&user);
            if global.game_data.chats_shell {
                print_chat(
                    &buffer,
                    note.timestamp,
                    &format!("{}", dir),
                    &user,
                    &msg,
                    colors::CYAN,
                    ignore,
                );
            }
            if global.game_data.chats_chat {
                print_chat(
                    &chat_buffer,
                    note.timestamp,
                    &format!("{}", dir),
                    &user,
                    &msg,
                    colors::CYAN,
                    ignore,
                );
                chat_buffer.insert(&mut chat_buffer.get_end_iter(), "\n");
                chat_view.scroll_mark_onscreen(&chat_buffer.get_selection_bound().unwrap());
            }
        }
        Xfer {
            dir,
            user,
            amount,
            memo,
        } => {
            let change = amount.parse().unwrap_or_else(|_| 0.into());
            let descr = if dir == hackmud_client::Direction::To {
                global.game_data.gc.as_mut().map(|n| {
                    *n -= change;
                    set_badge_currency(&badge_lbl, *n);
                });
                "Transferred"
            } else {
                global.game_data.gc.as_mut().map(|n| {
                    *n += change;
                    set_badge_currency(&badge_lbl, *n);
                });
                "Received"
            };
            format::add_output(
                &buffer,
                &if let Some(memo) = memo {
                    format!("{} {} {} `C{}`: {}", descr, amount, dir, user, memo)
                } else {
                    format!("{} {} {} `C{}`", descr, amount, dir, user)
                },
            );
        }
        SysStatus {
            breach,
            tutorial,
            hardline,
        } => {
            if let Some(f) = hardline {
                format::add_error_text(
                    &buffer,
                    &format!("-hardline connection reset- {:.2}s remain", f),
                );
            };
            // set status.
            set_status(&status_lbl, breach, hardline, &mut global.status_anim_cb);
            if let Some(b) = breach {
                if b {
                    format::add_error_text(&buffer, "-system is breached-");
                } else {
                    format::add_output(
                        &buffer,
                        "System locks `Crotated`. System breach state reset",
                    );
                }
            }
            if !tutorial.is_empty() {
                global.game_data.tutorial = Some(tutorial);
            }
        }
        Upgrade {
            action,
            name,
            rarity,
        } => {
            let rarity = rarity.min((colors::RARITY_COLORS.len() - 1) as u8) as usize;
            format::add_colored_text(
                &buffer,
                &format!("-{}- ", name),
                colors::RARITY_COLORS[rarity],
            );
            format::add_info_text(&buffer, &action);
        }
    }
    {
        let mut end = buffer.get_end_iter();
        if end.get_offset() != start_offset {
            buffer.insert(&mut end, "\n");
            shell_view.scroll_mark_onscreen(&buffer.get_selection_bound().unwrap());
        }
    }
    enforce_buffer_limit(&buffer);
    enforce_buffer_limit(&chat_buffer);
}

fn handle_connection<Fut>(
    addr: SocketAddr,
    stream: Result<TcpStream, io::Error>,
    req_rx: mpsc::Receiver<String>,
    exit_fut: Fut,
) -> impl Future<Item = (), Error = io::Error>
where
    Fut: Future<Item = (), Error = io::Error>,
{
    use future::Either;
    use hackmud_client::{Message, codec::ClientCodec};
    type ClientStream = tokio_io::codec::Framed<TcpStream, ClientCodec>;

    fn do_command<F, Fut>(
        stream: ClientStream,
        exit_fut: Fut,
        cmd: String,
        mut f: F,
    ) -> Result<Either<(ClientStream, Fut), ()>, io::Error>
    where
        Fut: Future<Item = (), Error = io::Error>,
        F: FnMut(Response) -> Result<(), io::Error>,
    {
        match stream
            .send(cmd)
            .and_then(|stream| {
                stream.into_future().map_err(|(e, _)| {
                    io::Error::new(
                        io::ErrorKind::InvalidData,
                        format!("Invalid message: {}", e),
                    )
                })
            })
            .select2(exit_fut)
            .wait()
        {
            Ok(Either::A(((resp, stream_rest), exit_fut))) => {
                if let Some(Message::Response(resp)) = resp {
                    f(resp).map(|_| Either::A((stream_rest, exit_fut)))
                } else {
                    Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        "Failed to get response from server",
                    ))
                }
            }
            Ok(Either::B(_)) => Ok(Either::B(())),
            Err(Either::A((e, _))) | Err(Either::B((e, _))) => Err(e),
        }
    }

    match stream {
        Ok(stream) => {
            // show the client ui.
            glib::idle_add(on_connect);
            debug!("Connection established to: {}", addr);

            let mut stream = stream.framed(ClientCodec::new());
            let mut auth_status = false;

            // Get connectionstatus.
            let mut res = do_command(stream, exit_fut, "#status".into(), |resp| {
                use std::convert::TryInto;
                use hackmud_client::response::StatusResponse;

                let resp: Result<StatusResponse, _> = resp.try_into();
                match resp {
                    Ok(status) => {
                        let status = status.0.unwrap().1;
                        trace!("Received status response: {:?}", status);
                        info!("Handshake complete.");
                        auth_status = status.authenticated;
                        // show banner, help, set status.
                        glib::idle_add(move || {
                            GLOBAL.with(|global| {
                                let mut g_ref_mut = global.borrow_mut();
                                let (status, shell_view, shell_buffer) = {
                                    let builder_ref = g_ref_mut.builder.as_ref().unwrap();
                                    let shell_view: gtk::TextView = builder_ref
                                        .get_object("shell_view")
                                        .expect("Failed to get textbuffer `shell_view`");
                                    let shell_buffer: gtk::TextBuffer = builder_ref
                                        .get_object("shell_buffer")
                                        .expect("Failed to get textbuffer `shell_buffer`");
                                    let status: gtk::Label = builder_ref
                                        .get_object("status")
                                        .expect("Failed to status label");
                                    (status, shell_view, shell_buffer)
                                };
                                display_banner(&shell_buffer);
                                display_help(&shell_buffer, "");
                                // not authenticated.
                                if !auth_status {
                                    status.set_label(&format!(
                                        "<span color=\"{}\">Auth Failure</span>",
                                        colors::RED
                                    ));
                                }
                                // show animation.
                                g_ref_mut
                                    .anim_shell_busy(&shell_view, &mut shell_buffer.get_end_iter());
                            });
                            glib::Continue(false)
                        });

                        Ok(())
                    }
                    Err(e) => Err(io::Error::new(
                        io::ErrorKind::InvalidData,
                        format!("Invalid message: {}", e),
                    )),
                }
            });
            if auth_status {
                res = match res {
                    Ok(Either::A((stream_rest, exit_fut))) => {
                        stream = stream_rest;
                        do_command(stream, exit_fut, "#client_data".into(), |resp| {
                            let mut resp = Some(resp);
                            glib::idle_add(move || {
                                let resp = resp.take().unwrap();
                                GLOBAL.with(|global| {
                                    // Still busy.
                                    process_response(&mut global.borrow_mut(), resp, false)
                                });
                                glib::Continue(false)
                            });
                            Ok(())
                        })
                    }
                    Ok(_) => {
                        return Either::A(Box::new(future::ok(())) as Box<Future<Item = _, Error = _>>)
                    }
                    Err(e) => return Either::B(future::err(e)),
                };

                res = match res {
                    Ok(Either::A((stream_rest, exit_fut))) => {
                        stream = stream_rest;
                        do_command(stream, exit_fut, "#list_users".into(), |resp| {
                            let mut resp = Some(resp);
                            glib::idle_add(move || {
                                let resp = resp.take().unwrap();
                                GLOBAL.with(|global| {
                                    // Still busy.
                                    process_response(&mut global.borrow_mut(), resp, false)
                                });
                                glib::Continue(false)
                            });
                            Ok(())
                        })
                    }
                    Ok(_) => {
                        return Either::A(Box::new(future::ok(())) as Box<Future<Item = _, Error = _>>)
                    }
                    Err(e) => return Either::B(future::err(e)),
                };
            }

            match res {
                Ok(Either::A((stream_rest, _))) => {
                    stream = stream_rest;
                    glib::idle_add(|| {
                        GLOBAL.with(|global| {
                            info!("Client ready.");
                            global.borrow_mut().set_processing(false);
                        });
                        glib::Continue(false)
                    });
                }
                Ok(_) => {
                    return Either::A(Box::new(future::ok(())) as Box<Future<Item = _, Error = _>>)
                }
                Err(e) => return Either::B(future::err(e)),
            }

            // start processing the rest.
            let (wr, rd) = stream.split();
            let reader = rd.for_each(|msg| {
                let mut msg = Some(msg);
                glib::idle_add(move || {
                    let msg = msg.take().unwrap();
                    GLOBAL.with(|global| match msg {
                        Message::Response(resp) => {
                            process_response(&mut global.borrow_mut(), resp, true)
                        }
                        Message::Notifications(notes) => {
                            let mut global_ref_mut = global.borrow_mut();
                            for note in notes {
                                process_notification(&mut global_ref_mut, note);
                            }
                        }
                    });
                    glib::Continue(false)
                });
                Ok(())
            }).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::InvalidData,
                    format!("Invalid message: {}", e),
                )
            });
            let writer = req_rx
                .map_err(|_| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        "sender stored in global state for gtk thread was dropped",
                    )
                })
                .forward(wr);
            Either::A(Box::new(
                reader
                    .select(writer.map(|_| ()))
                    .map(|_| ())
                    .map_err(|e| e.0),
            ) as Box<Future<Item = _, Error = _>>)
        }
        Err(e) => {
            // delete the info messagedialog.
            glib::idle_add(|| {
                let windows = gtk::Window::list_toplevels();
                for win in windows {
                    let _ = win.downcast::<gtk::Window>().map(|w| {
                        if w.get_modal() {
                            w.destroy();
                        }
                    });
                }
                glib::Continue(false)
            });
            Either::B(future::err(e))
        }
    }
}

fn spawn_network_thread(addr: SocketAddr, exit_rx: oneshot::Receiver<()>) {
    let (req_tx, req_rx) = mpsc::channel(1);
    let handle = thread::Builder::new()
        .name("clientd-client".into())
        .spawn(move || {
            let mut core = Core::new().unwrap();
            let exit_fut = exit_rx.map_err(|e| error!("exit_rx error: {}", e)).shared();
            let exit_fut_inner = exit_fut
                .clone()
                .map(|_| info!("Cancelled while waiting for StatusResponse"))
                .map_err(|_| io::Error::new(io::ErrorKind::Other, "exit_fut error"));
            core.run(
                TcpStream::connect2(&addr)
                    .then(move |res| handle_connection(addr, res, req_rx, exit_fut_inner))
                    .select(
                        exit_fut
                            .map(|_| ())
                            .map_err(|_| io::Error::new(io::ErrorKind::Other, "exit_fut error")),
                    )
                    .map(|(_, _)| info!("Terminating event loop: connection closed."))
                    .map_err(|(e, _)| error!("Terminating event loop with error: {}", e)),
            ).unwrap_or(());

            info!("Network thread terminating..");
        })
        .unwrap();
    GLOBAL.with(move |global| {
        let mut g_ref_mut = global.borrow_mut();
        g_ref_mut.req_tx = Some(req_tx);
        g_ref_mut.handle = Some(handle);
    });
}

fn exit_network_thread() {
    GLOBAL.with(|global| {
        global.borrow_mut().close_connection();
    });
}

fn filter_editable_on_insert<T>(
    editable: &T,
    text: &str,
    pos: &mut i32,
    f: fn(&str) -> Cow<str>,
    handler_id: &glib::signal::SignalHandlerId,
) -> bool
where
    T: glib::IsA<gtk::Editable> + ObjectExt,
{
    let new_text = f(text);
    editable.block_signal(handler_id);
    editable.insert_text(new_text.as_ref(), pos);
    editable.unblock_signal(handler_id);
    editable.stop_signal_emission("insert-text");
    !new_text.as_ref().is_empty()
}

fn build_ui(application: &gtk::Application) {
    // use gdk::enums::key;
    use gtk::{ApplicationWindow, Builder, Button, Entry};

    fn valid_address_str(s: &str) -> Cow<str> {
        for c in s.chars() {
            if c == '[' || c == ']' || c == ':' || c == '.' || (c >= '0' && c <= '9')
                || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')
            {
                continue;
            }
            return "".into();
        }
        s.into()
    }

    // set WM_CLASS(for X windowing system).
    gdk::set_program_class("Hackmud-gui");

    // Add custom css as styleprovider.
    let screen = gdk::Screen::get_default().expect("Failed to get gdk::Screen object");
    let css_provider = gtk::CssProvider::new();
    css_provider
        .load_from_data(hackmud_css().as_bytes())
        .unwrap_or_else(|e| panic!("Failed to load css data: {}", e));
    gtk::StyleContext::add_provider_for_screen(
        &screen,
        &css_provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    let glade_src = include_str!("hackmud-ui.glade");
    let builder = Builder::new_from_string(glade_src);

    GLOBAL.with(clone!(builder => move |global| {
        global.borrow_mut().builder = Some(builder);
    }));

    let window: ApplicationWindow = builder
        .get_object("address_window")
        .expect("Couldn't get `address_window` window");
    let address_entry: Entry = builder
        .get_object("address_entry")
        .expect("Couldn't get `address_entry` button");
    let address_ok: Button = builder
        .get_object("address_ok_btn")
        .expect("Couldn't get `address_ok_btn` button");

    address_entry.set_text(DEFAULT_HACKMUD_CLIENTD_ADDR);
    // Disable pop-up.
    address_entry.connect_popup_menu(|_| true);
    address_entry.connect_button_press_event(|_, btn_ev| {
        if btn_ev.get_button() == 3 {
            Inhibit(true)
        } else {
            Inhibit(false)
        }
    });

    let handler_id = Rc::new(RefCell::new(glib::translate::FromGlib::from_glib(
        std::u64::MAX,
    )));
    handler_id.replace(address_entry.connect_insert_text(
        clone!(handler_id => move |instance, text, pos| {
            filter_editable_on_insert(instance, text, pos, valid_address_str, &handler_id.borrow());
        }),
    ));

    window.set_default(Some(&address_ok));
    window.set_application(application);
    window.connect_delete_event(|w, _| {
        w.destroy();
        Inhibit(false)
    });

    address_ok.connect_clicked(clone!(address_entry, window => move |_| {
        use gtk::{MessageDialog, DialogFlags, MessageType, ButtonsType};

        let input = address_entry.get_text().unwrap_or_else(||"".into());
        let socket_addr: Option<SocketAddr> = input.as_str().parse().ok();
        if let Some(socket_addr) = socket_addr {
            let (tx, rx) = oneshot::channel();
            GLOBAL.with(move |global| {
                global.borrow_mut().exit_tx = Some(tx);
            });

            let connect_win = MessageDialog::new(
                Some(&window),
                DialogFlags::MODAL | DialogFlags::DESTROY_WITH_PARENT,
                MessageType::Info, ButtonsType::Cancel,
                &format!("Connecting to {}..", socket_addr),
            );
            connect_win.set_transient_for(&window);
            connect_win.connect_response(|win, _| {
                exit_network_thread();
                win.destroy();
            });
            connect_win.show_all();
            spawn_network_thread(socket_addr, rx);
        } else {
            let err_win = MessageDialog::new(
                Some(&window),
                DialogFlags::MODAL | DialogFlags::DESTROY_WITH_PARENT,
                MessageType::Error, ButtonsType::Close,
                if input.is_empty() {
                    "Enter a socket address[host:port]"
                } else {
                    "Invalid socket address!!"
                }
            );
            err_win.set_transient_for(&window);
            err_win.connect_response(|win, _| win.destroy());
            err_win.show_all();
        }
    }));
    window.show_all();
}

fn main() {
    let application =
        gtk::Application::new("com.hackmud.hackmud-gtk-ui", gio::ApplicationFlags::empty())
            .expect("Initialization failed...");

    application.connect_startup(move |app| {
        build_ui(app);
    });
    application.connect_activate(|_| {});

    application.run(&args().collect::<Vec<_>>());
}
