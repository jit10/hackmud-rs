#![allow(dead_code)]

// Everything below was transformed from contents of `Colors class` with regexp and elisp.
pub(crate) const ALL_COLORS: [&str; 52] = [
    "#000000", "#ffffff", "#404040", "#cacaca", "#676767", "#9c9c9c", "#7e0000", "#ff0000",
    "#8e3535", "#ff8383", "#a34f00", "#ff8000", "#725438", "#f4aa70", "#a98600", "#fbc804",
    "#b2944a", "#ffd863", "#949600", "#fff404", "#4a5326", "#f4f998", "#299400", "#1eff00",
    "#24381c", "#b3ff9c", "#00545c", "#00ffff", "#324b4d", "#8fe6ff", "#0073a7", "#0070dd",
    "#395a6d", "#a4e3ff", "#020067", "#0000ff", "#517aa1", "#7ab2f4", "#611d81", "#b035ee",
    "#44314d", "#e7c4ff", "#8d0069", "#ff00ec", "#983984", "#ff96e0", "#890024", "#ff0070",
    "#762e4b", "#ff6a98", "#101215", "#0c112b",
];

pub(crate) const USER_COLORS: [&str; 6] = [
    YELLOW,
    PINK,
    GREEN,
    LIGHT_GRAY,
    LIGHT_YELLOW,
    LIGHT_GREEN,
];

pub(crate) const CURRENCY_UNIT_COLORS: [&str; 5] = [
    RED,     // Q
    MAGENTA, // T
    YELLOW,  // B
    GREEN,   // M
    CYAN,    // K
];

pub(crate) const RARITY_COLORS: [&str; 6] = [
    GRAY,
    WHITE,
    HAXOR_GREEN,
    HARDCORE_BLUE,
    UBER_PURPLE,
    ELEET_ORANGE,
];

pub(crate) const SELECTION_BG: &str = "#456284";
pub(crate) const SCROLLBAR_TROUGH_BG: &str = "#213247";
pub(crate) const SCROLLBAR_SLIDER_BG: &str = "#406997";
pub(crate) const KEY: &str = CYAN;
pub(crate) const VALUE: &str = MAGENTA;
pub(crate) const USER_NAME: &str = GRAY;
pub(crate) const SCRIPT_NAME: &str = GREEN;

pub(crate) const BLACK: &str = ALL_COLORS[50];
pub(crate) const CYAN: &str = ALL_COLORS[27];
pub(crate) const ELEET_ORANGE: &str = ALL_COLORS[11];
pub(crate) const GRAY: &str = ALL_COLORS[5];
pub(crate) const GREEN: &str = ALL_COLORS[23];
pub(crate) const HARDCORE_BLUE: &str = ALL_COLORS[31];
pub(crate) const HAXOR_GREEN: &str = ALL_COLORS[23];
pub(crate) const IGNORE: &str = ALL_COLORS[2];
pub(crate) const LIGHT_GRAY: &str = ALL_COLORS[3];
pub(crate) const LIGHT_GREEN: &str = ALL_COLORS[25];
pub(crate) const LIGHT_YELLOW: &str = ALL_COLORS[21];
pub(crate) const MAGENTA: &str = ALL_COLORS[43];
pub(crate) const PINK: &str = ALL_COLORS[45];
pub(crate) const RED: &str = ALL_COLORS[7];
pub(crate) const TEXT_BLUE: &str = ALL_COLORS[37];
pub(crate) const UBER_PURPLE: &str = ALL_COLORS[39];
pub(crate) const WHITE: &str = ALL_COLORS[1];
pub(crate) const YELLOW: &str = ALL_COLORS[19];
