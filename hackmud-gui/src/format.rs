use std::collections::HashMap;
use std::fmt;
use gtk::{self, prelude::*};
use regex::{Captures, Match, Regex};
use colors;
use hackmud_api::types::AutoComplete;
use radix_trie::{Trie, TrieCommon};
use serde_json::{to_string_pretty, Value};

lazy_static! {
    pub(crate) static ref AUTO_COMPLETE: Regex = Regex::new(r"^\s*(?:[#/]\w*|(?:[a-z_][a-z0-9_]*\.?(?:[a-z_][a-z0-9_]*\s*)?))$").unwrap();

    static ref SP_CMDS: Regex = Regex::new(r"(?m)^\s*(#(?:clear_chat|clear|help|exit|auth|status|chat_pass|revoke_chat_tokens|client_data|list_users|create_user|retire_user|user|notifications|up|down|delete))\s*").unwrap();

    static ref CHAT_DISALLOWED_CHARS: Regex = Regex::new(r"[^\P{Other}\n\t\u0020]|[\u00A0\u180E\u2004\u2005\u2006\u2008\u2009\u200A\u200B\u202F\u205F\uFEFF]|(﹕|︰|：|︓|꞉|᠄|᠄|˸|ː|։|∶|;|⁏|︔|﹔|；|׃|\u0703|\u0704|\u0743|\u0744|\u0831|\u0A03|\u0A83|\u11A2|\u1361|\u1393|\u16EC|\u1803|\u1C7A|\u205A|\u2083|\u2805|\u2806|\u2818|\u2828|\u2830|\u2841|\u2842|\u2844|\u28A0|\u2890|\uA4FC|\uA4FD|\uA789|\uA952|︰)").unwrap();
    static ref CHAT_DISALLOWED_STRING: Regex = Regex::new(r"([:;][:;][:;])|(T[RA]U[S5]T[^\n]+?C[O0][MN][MN]?U[MN][I1l]C[RA]T[1Il][O0][MN])").unwrap();

    static ref AT_USER: Regex = Regex::new(r"@([\w_]+)").unwrap();
    static ref AT_USER_IN_QUOTES: Regex = Regex::new(r#""[^"]*@([\w_]+)[^"]*""#).unwrap();
    static ref CURRENCY: Regex = Regex::new(r"((?:-)?(?:\d{1,5}(Q))?(?:\d{1,3}(T))?(?:\d{1,3}(B))?(?:\d{1,3}(M))?(?:\d{1,3}(K))?(?:\d{1,3})?)(GC)(?:\W|$)").unwrap();
    static ref COLOR_CODE: Regex = Regex::new(r"(`[0-9a-zA-Z])([^:`\n]{1,2}|[^`\n]{3,}?)(`)").unwrap();
    static ref DATE: Regex = Regex::new(r"(\d{1,4})(AD) (D)(\d{1,3})").unwrap();
    static ref FULL_SCRIPT: Regex = Regex::new(r"(?:#s\.|[^#\.a-z0-9_]|^)(?:(?P<trust>trust|accts|autos|scripts|users|sys|corps|chats|gui|escrow|market|kernel)|(?P<user>[a-z_][a-z0-9_]*))\.(?P<script>[a-z_][a-z0-9_]*)").unwrap();
    static ref KEY_VALUE: Regex = Regex::new(r#"(?P<key>(?:"(?:[^"\\\n]|\.)+")|(?:[a-zA-z_]\w*))[ \t]*:[ \t]*(?:\{|\[|(?P<value>true|false|null|(?:"(?:[^"\\\n]|\.)*")|(?:-?\d+\.?\d*)|#s\.[a-z_][a-z0-9_]*\.[a-z_][a-z0-9_]*))"#).unwrap();
    static ref KEY_VALUE_SUGGEST: Regex = Regex::new(r#"(?:(?:"(?:[^"\\\n]|\.)+")|(?:[a-zA-z_]\w*))[ \t]*:[ \t]*((?:<[^>]+>)|(?:"<(?:(?:[^"\\\n]|\.)*>")))"#).unwrap();
    static ref SCRIPT: Regex = Regex::new(r"^[a-z_][a-z0-9_]*\s+").unwrap();
    static ref TRUST_MESSAGE: Regex = Regex::new(r":::TRUST COMMUNICATION:::").unwrap();

    static ref QUOTED_KEY: Regex = Regex::new(r#"(?P<prefix>(?:\{|,)?\s*)"(?P<key>\w+)"(?P<suffix>[ \t]*:[ \t]*(?:"|-|\w|#|\{|\[))"#).unwrap();
    static ref UNQUOTED_KEY: Regex = Regex::new(r#"(?P<prefix>(?:\{|,)?\s*)(?P<key>\w+)(?P<suffix>[ \t]*:[ \t]*(?:"|-|\w|#|\{|\[|$))"#).unwrap();
    static ref STRING: Regex = Regex::new(r#""(?:\\"|[^"])*?""#).unwrap();
}

const MAX_RESULT_LINES: usize = 1000;
const GC_UNIT_VALS: [i64; 6] = [
    1_000_000_000_000_000,
    1_000_000_000_000,
    1_000_000_000,
    1_000_000,
    1_000,
    1,
];

static CORRUPTION_CHARS: [&str; 10] = ["¡", "¢", "Á", "¤", "Ã", "¦", "§", "¨", "©", "ª"];
static mut USER_COLORS_MAP: Option<HashMap<String, &'static str>> = None;

#[derive(Debug)]
pub struct Completion(Trie<String, bool>, Trie<String, Value>);

impl From<AutoComplete> for Completion {
    fn from(autos: AutoComplete) -> Self {
        let mut c = Completion(Trie::new(), Trie::new());
        c.merge(autos, false);
        c
    }
}

impl Completion {
    pub(crate) fn new() -> Self {
        let mut c = Completion(Trie::new(), Trie::new());
        c.add_defaults();
        c
    }

    #[inline]
    fn add_defaults(&mut self) {
        const CMDS: [&str; 17] = [
            "#clear",
            "#clear_chat",
            "#help",
            "#exit",
            "#auth",
            "#status",
            "#chat_pass",
            "#revoke_chat_tokens",
            "#client_data",
            "#list_users",
            "#create_user",
            "#retire_user",
            "#user",
            "#notifications",
            "#up",
            "#down",
            "#delete",
        ];
        for &s in &CMDS {
            self.add_user(s);
        }
        self.add_user("scripts");
        self.add_user("trust");
        self.add_script("trust.sentience", Value::Null);
        self.add_user("kernel");
        self.add_script("kernel.hardline", json!({"activate": true}));
    }

    #[inline]
    fn add_user<S: Into<String>>(&mut self, user: S) {
        self.0.insert(user.into(), true);
    }

    #[inline]
    fn add_script<S: Into<String>>(&mut self, script: S, value: Value) {
        debug_assert!(value.is_null() || value.is_object());
        self.1.insert(script.into(), value);
    }

    #[inline]
    pub(crate) fn add_macro<S: Into<String>>(&mut self, name: S) {
        self.add_user(name);
    }

    #[inline]
    pub(crate) fn assign(&mut self, autos: AutoComplete) {
        *self = autos.into();
        self.add_defaults();
    }

    pub(crate) fn complete(&self, prefix: &str, force: bool) -> Vec<String> {
        if prefix.is_empty() {
            return vec![];
        }
        let idx = prefix.find('.').unwrap_or(0);
        let mut r = vec![];
        if idx > 0 {
            if let Some(t) = self.1.get_raw_descendant(prefix) {
                let k = t.key().map(|s| s.as_str());
                if (k == Some(prefix) && force) || t.len() == 1 {
                    let v = t.value()
                        .expect("Both key and value should be present in subtrie");
                    if v.is_object() {
                        use std::fmt::Write;
                        let mut js = String::new();
                        js.write_fmt(format_args!("{} {{", &k.unwrap()[prefix.len()..]))
                            .unwrap();
                        let v_obj = v.as_object().unwrap();
                        for (i, (k, v)) in v_obj.iter().enumerate() {
                            v.as_str()
                                .map(|s| js.write_fmt(format_args!(" {}: {}", k, s)).unwrap());
                            if i != v_obj.len() - 1 {
                                js.push(',');
                            }
                        }
                        js.push_str(" }");
                        r.push(js);
                    } else if v.is_null() {
                        r.push(k.unwrap()[prefix.len()..].into());
                    }
                } else {
                    for k in t.keys() {
                        r.push(k[idx + 1..].into());
                    }
                }
            }
        } else if let Some(t) = self.0.get_raw_descendant(prefix) {
                if t.len() == 1 {
                    r.push(t.keys().next().unwrap()[prefix.len()..].into());
                } else {
                    for k in t.keys() {
                        r.push(k.clone());
                    }
                }
        }
        r
    }

    pub(crate) fn remove_user(&mut self, user: &str) {
        self.0.remove(user);
        let mut keys = vec![];
        if let Some(t) = self.1.get_raw_descendant(&(user.to_owned() + ".")) {
            keys = t.keys().cloned().collect();
        }
        for k in keys {
            self.1.remove(k.as_str());
        }
    }

    pub(crate) fn merge(&mut self, other: AutoComplete, remove: bool) {
        if remove {
            for (u, v) in other.user_autos.iter() {
                if v.is_null() {
                    self.remove_user(u);
                } else if let Value::Object(ref m) = *v {
                    for s in m.keys() {
                        self.1.remove(&(u.clone() + &format!(".{}", s)));
                    }
                    if self.1.get_raw_descendant(&(u.clone() + ".")).is_none() {
                        self.0.remove(u.as_str());
                    }
                }
            }
        } else {
            for (u, v) in other.user_autos {
                if v.is_null() {
                    self.add_user(u);
                } else if let Value::Object(m) = v {
                    self.add_user(u.clone());
                    for (s, v) in m {
                        self.add_script(u.clone() + &format!(".{}", s), v);
                    }
                }
            }
        }
    }
}

#[inline]
pub(crate) fn init() {
    unsafe {
        USER_COLORS_MAP = Some(HashMap::new());
    }
}

pub(crate) fn cmd_buffer_highlight_syntax(buffer: &gtk::TextBuffer) {
    let cmd_start = buffer.get_iter_at_mark(&buffer.get_mark("cmd_insert").unwrap());
    let cmd_end = buffer.get_end_iter();
    let cmd_text = buffer
        .get_text(&cmd_start, &cmd_end, false)
        .unwrap_or_else(|| "".into());

    if cmd_text.is_empty() {
        return;
    }
    buffer.remove_all_tags(&cmd_start, &cmd_end);
    let mut start = cmd_start.clone();
    let mut text: &str = &cmd_text;

    // special commands with gray color.
    SP_CMDS
        .find(text)
        .map(|m| {
            start.forward_chars(m.end() as i32);
            text = &text[m.end()..];
            buffer.apply_tag_by_name(colors::GRAY, &cmd_start, &start);
        })
        .unwrap_or_else(|| {
            SCRIPT.find(text).map(|m| {
                start.forward_chars(m.end() as i32);
                text = &text[m.end()..];
                buffer.apply_tag_by_name(colors::SCRIPT_NAME, &cmd_start, &start);
            });
        });

    // Higher priority color removes the overlapping color before applying its color.
    // KEY_VALUE coloring has lowest priority.
    for c in KEY_VALUE.captures_iter(text) {
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, &start, text, &m, colors::KEY, false));
        c.get(2)
            .map(|m| re_match_apply_tag(buffer, &start, text, &m, colors::VALUE, false));
    }

    // highlight "@user".
    highlight_user(buffer, &start, text, &AT_USER_IN_QUOTES);
    // highlight user/trust scripts.
    highlight_full_script(buffer, &start, text);
}

#[inline]
pub(crate) fn get_user_color(username: &str) -> &'static str {
    static mut CUR_COLOR_IDX: usize = 0;
    unsafe {
        USER_COLORS_MAP
            .as_mut()
            .unwrap()
            .entry(username.to_owned())
            .or_insert_with(|| {
                let color = colors::USER_COLORS[CUR_COLOR_IDX];
                CUR_COLOR_IDX = (CUR_COLOR_IDX + 1) % colors::USER_COLORS.len();
                color
            })
    }
}

// highlight "@some_user" if we have already seen `some_user`.
// iter corresponds to start of `text` in buffer.
fn highlight_user(buffer: &gtk::TextBuffer, iter: &gtk::TextIter, text: &str, re: &Regex) {
    for c in re.captures_iter(text) {
        c.get(1).map(|m| {
            let color;
            unsafe {
                color = USER_COLORS_MAP.as_ref().unwrap().get(m.as_str());
            }
            if let Some(color) = color {
                let mut start = iter.clone();
                start.set_offset(iter.get_offset() + m.start() as i32 - 1);
                let mut n_start = start.clone();
                n_start.forward_char();
                let mut end = iter.clone();
                end.set_offset(iter.get_offset() + m.end() as i32);
                // remove any previous coloring here.
                buffer.remove_all_tags(&start, &end);
                // color `@`.
                buffer.apply_tag_by_name(colors::GRAY, &start, &n_start);
                // color `user`.
                buffer.apply_tag_by_name(color, &n_start, &end);
            }
        });
    }
}

#[inline]
fn highlight_full_script(buffer: &gtk::TextBuffer, iter: &gtk::TextIter, text: &str) {
    for c in FULL_SCRIPT.captures_iter(text) {
        // trust users
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, colors::ELEET_ORANGE, true));
        // ordinary users. group(1) didn't happen.
        c.get(2)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, colors::USER_NAME, true));
        // script name. group(1/2) definitely happened.
        c.get(3)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, colors::SCRIPT_NAME, true));
    }
}

#[inline]
fn highlight_currency(buffer: &gtk::TextBuffer, iter: &gtk::TextIter, text: &str) {
    use colors::*;

    for c in CURRENCY.captures_iter(text) {
        c.get(1).map(|m| {
            if m.start() != m.end() {
                // numbers -> light_gray.
                re_match_apply_tag(buffer, iter, text, &m, LIGHT_GRAY, true);
                // color the units.
                for i in 0..CURRENCY_UNIT_COLORS.len() {
                    c.get(i + 2).map(|m| {
                        re_match_apply_tag(buffer, iter, text, &m, CURRENCY_UNIT_COLORS[i], true)
                    });
                }
            }
        });
        // "GC" -> gray.
        c.get(CURRENCY_UNIT_COLORS.len() + 2)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, GRAY, true));
    }
}

#[inline]
fn highlight_date(buffer: &gtk::TextBuffer, iter: &gtk::TextIter, text: &str) {
    use colors::*;
    for c in DATE.captures_iter(text) {
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, WHITE, true));
        c.get(2)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, LIGHT_GRAY, true));
        c.get(3)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, GRAY, true));
        c.get(4)
            .map(|m| re_match_apply_tag(buffer, iter, text, &m, GREEN, true));
    }
}

#[inline]
pub(crate) fn char_to_color_name(code: char) -> &'static str {
    use colors::*;

    debug_assert!(code.len_utf8() == 1);
    let code = code as u8;
    if code.is_ascii_digit() {
        RARITY_COLORS[(RARITY_COLORS.len() -1).min((code - b'0') as usize)]
    } else if code.is_ascii_lowercase() {
        ALL_COLORS[2 * 25.min((code - b'a') as usize)]
    } else if code.is_ascii_uppercase() {
        ALL_COLORS[2 * 25.min((code - b'A') as usize) + 1]
    } else {
        unreachable!()
    }
}

#[inline]
fn highlight_color_code(buffer: &gtk::TextBuffer, iter: &gtk::TextIter, text: &str) {
    for c in COLOR_CODE.captures_iter(text) {
        c.get(0).map(|m| {
            let mut skip = false;
            {
                // after the backtick.
                let text = &m.as_str()[1..];
                // these are the regex whose start can also be a color-code.
                // For these we skip applying the color-code.
                CURRENCY.find(text).map(|m| skip |= m.start() == 0);
                DATE.find(text).map(|m| skip |= m.start() == 0);
                FULL_SCRIPT.find(text).map(|m| skip |= m.start() == 0);
            }
            if !skip {
                // match implies group(1) must be present.
                let m = c.get(1).unwrap();
                re_match_apply_tag(buffer, iter, text, &m, "invisible", true);
                let color = char_to_color_name(m.as_str().chars().nth(1).unwrap());
                c.get(2)
                    .map(|m| re_match_apply_tag(buffer, iter, text, &m, color, true));
                c.get(3)
                    .map(|m| re_match_apply_tag(buffer, iter, text, &m, "invisible", true));
            }
        });
    }
}

#[inline]
fn re_match_apply_tag(
    buffer: &gtk::TextBuffer,
    iter: &gtk::TextIter,
    text: &str,
    m: &Match,
    tag_name: &'static str,
    remove: bool,
) {
    if m.start() == m.end() {
        return;
    }
    // Unfortunate quadratic complexity.(for applying k tags = O(kn))
    let match_start = text[..m.start()].chars().count();
    let mut start = iter.clone();
    start.forward_chars(match_start as i32);
    let mut end = start.clone();
    end.forward_chars(m.as_str().chars().count() as i32);
    if remove {
        buffer.remove_all_tags(&start, &end);
    }
    buffer.apply_tag_by_name(tag_name, &start, &end);
}

fn add_text(buffer: &gtk::TextBuffer, text: &str, cmd: bool) {
    let zero = Regex::new("\0").unwrap();
    let text = zero.replace_all(text, CORRUPTION_CHARS[3]);
    let mut start = buffer.get_end_iter();
    let start_off = start.get_offset();
    buffer.insert(&mut start, &text);
    buffer.insert(&mut start, "\n");
    start.set_offset(start_off);
    if cmd {
        buffer.apply_tag_by_name(colors::WHITE, &start, &buffer.get_end_iter());
    }

    // special commands with gray color.
    for c in SP_CMDS.captures_iter(&text) {
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, &start, &text, &m, colors::GRAY, false));
    }
    // key-value lowest priority.
    for c in KEY_VALUE.captures_iter(&text) {
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, &start, &text, &m, colors::KEY, false));
        c.get(2)
            .map(|m| re_match_apply_tag(buffer, &start, &text, &m, colors::VALUE, false));
    }
    // key-value suggest(k: <stuff>|"<stuff>")
    for c in KEY_VALUE_SUGGEST.captures_iter(&text) {
        // only value part is captured. we want to color them light-gray.
        c.get(1)
            .map(|m| re_match_apply_tag(buffer, &start, &text, &m, colors::LIGHT_GRAY, true));
    }
    // highlight color-code.
    highlight_color_code(buffer, &start, &text);
    // highlight date.
    highlight_date(buffer, &start, &text);
    // highlight currency.
    highlight_currency(buffer, &start, &text);
    // highlight "@user".
    highlight_user(buffer, &start, &text, &AT_USER);
    // highlight user/trust scripts.
    highlight_full_script(buffer, &start, &text);
    // highlight trust message.
    for c in TRUST_MESSAGE.captures_iter(&text) {
        c.get(0)
            .map(|m| re_match_apply_tag(buffer, &start, &text, &m, colors::RED, false));
    }
}

#[inline]
pub(crate) fn add_output(buffer: &gtk::TextBuffer, text: &str) {
    add_text(buffer, text, false);
}

#[inline]
pub(crate) fn add_command(buffer: &gtk::TextBuffer, text: &str) {
    add_text(buffer, text, true);
}

#[inline]
pub(crate) fn add_colored_text(buffer: &gtk::TextBuffer, text: &str, color: &'static str) {
    let mut end = buffer.get_end_iter();
    let start_off = end.get_offset();
    buffer.insert(&mut end, text);
    let mut start = end.clone();
    start.set_offset(start_off);
    buffer.apply_tag_by_name(color, &start, &end);
}

#[inline]
pub(crate) fn add_success_text(buffer: &gtk::TextBuffer, text: &str) {
    add_colored_text(buffer, text, colors::GREEN);
    buffer.insert(&mut buffer.get_end_iter(), "\n");
}

#[inline]
pub(crate) fn add_error_text(buffer: &gtk::TextBuffer, text: &str) {
    add_colored_text(buffer, text, colors::RED);
    buffer.insert(&mut buffer.get_end_iter(), "\n");
}

#[inline]
pub(crate) fn add_info_text(buffer: &gtk::TextBuffer, text: &str) {
    add_colored_text(buffer, text, colors::GRAY);
    buffer.insert(&mut buffer.get_end_iter(), "\n");
}

pub(crate) fn pretty_print(buffer: &gtk::TextBuffer, val: Value, lines: &mut usize) {
    #[inline]
    fn fmt_script_result<S: Into<String>>(text: S) -> String {
        let esc_quote = Regex::new(r#"\\""#).unwrap();
        let text = text.into();
        let text = QUOTED_KEY.replace_all(&text, "$prefix$key$suffix");
        esc_quote.replace_all(&text, "\"").into()
    }

    if *lines >= MAX_RESULT_LINES {
        return;
    }
    match val {
        Value::Array(vals) => for v in vals {
            pretty_print(buffer, v, lines);
        },
        mut v => {
            let text = if v.is_null() {
                "".into()
            } else if v.is_string() {
                // top level string.
                match v.take() {
                    Value::String(s) => fmt_script_result(s),
                    _ => unreachable!(),
                }
            } else {
                fmt_script_result(to_string_pretty(&v).unwrap())
            };
            *lines += text.lines().count();
            add_output(buffer, &text);
        }
    }
}

pub(crate) struct ScriptResult(usize, ::hackmud_client::ScriptResult);

impl ScriptResult {
    pub(crate) fn write_pretty(self, buffer: &gtk::TextBuffer) -> usize {
        let (mut lines, result) = (self.0, self.1);
        let script_name = result.script_name;
        match result.retval {
            Value::Object(mut m) => {
                let len = m.len();
                let ok = m.get("ok").and_then(|v| v.as_bool());
                let has_msg = m.get("msg").map(|v| v.is_string()).unwrap_or(false);
                if ok.is_some() && (len == 1 || (len == 2 && has_msg)) {
                    let ok = ok.unwrap_or(false);
                    if ok {
                        add_success_text(
                            buffer,
                            if script_name == "chats.tell" || script_name == "chats.send" {
                                "Msg Sent"
                            } else {
                                "Success"
                            },
                        );
                    } else {
                        add_error_text(buffer, "Failure");
                    }
                    lines += 1;
                    if has_msg {
                        pretty_print(buffer, m.remove("msg").unwrap(), &mut lines);
                    }
                } else {
                    pretty_print(buffer, Value::Object(m), &mut lines);
                }
            }
            v => pretty_print(buffer, v, &mut lines),
        }

        if lines >= MAX_RESULT_LINES {
            add_error_text(
                buffer,
                &format!("<too many lines ({}), output was culled>", MAX_RESULT_LINES),
            );
        }
        lines
    }
}

impl From<::hackmud_client::ScriptResult> for ScriptResult {
    fn from(res: ::hackmud_client::ScriptResult) -> Self {
        ScriptResult(0, res)
    }
}

pub(crate) fn fmt_command_args(cmd: &str) -> String {
    let mut s = String::new();
    let mut last = 0;
    for c in STRING.captures_iter(cmd) {
        let m = c.get(0).unwrap();
        let (start, end) = (m.start(), m.end());
        if last != start {
            s.push_str(&UNQUOTED_KEY.replace_all(&cmd[last..start], "$prefix\"$key\"$suffix"));
        }
        last  = end;
        s.push_str(m.as_str());
    }
    if last < cmd.len() {
        s.push_str(&UNQUOTED_KEY.replace_all(&cmd[last..cmd.len()], "$prefix\"$key\"$suffix"));
    }
    s
}

pub(crate) fn filter_chat_msg(msg: &str) -> String {
    #[inline]
    fn rand() -> usize {
        unsafe { ::libc::rand() as usize }
    }
    // atmost 1000 chars.
    let mut msg = &msg[..msg.len().min(1000)];
    // atmost 10 lines.
    if let Some(s) = msg.lines().nth(10) {
        msg = &msg[..(s.as_ptr() as usize - msg.as_ptr() as usize)];
    }

    let msg = CHAT_DISALLOWED_CHARS.replace_all(msg, |_: &Captures| {
        CORRUPTION_CHARS[rand() % CORRUPTION_CHARS.len()].into()
    });
    CHAT_DISALLOWED_STRING
        .replace_all(&msg, |cap: &Captures| {
            let mut s = String::new();
            for c in CORRUPTION_CHARS.iter().take(5) {
                s.push_str(c);
            }
            if cap.get(1).is_some() {
                s.chars().take(3).collect()
            } else {
                s
            }
        })
        .into()
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) enum MacroErrKind {
    Invalid,
    BodyEmpty,
    NameErr,
    Unescaped(usize),
    ArgErr { given: usize, needed: usize },
}

impl MacroErrKind {
    fn as_str(&self) -> ::std::borrow::Cow<'static, str> {
        use self::MacroErrKind::*;

        match *self {
            Invalid => "Invalid macro definition, must be of form `/name = body`".into(),
            BodyEmpty => "Empty macro body".into(),
            NameErr => "Invalid macro name, must start with ascii-alphabetic/'_' followed by 0 or \
                        more ascii-alphanumeric/'_' chars"
                .into(),
            Unescaped(n) => format!(
                "Unescaped char inside body at byte index: {}, Curly braces('{{'|'}}') must be \
                 escaped by repeating itself",
                n
            ).into(),
            ArgErr { given, needed } => {
                format!("Required atleast {} arguments; provided: {}", needed, given).into()
            }
        }
    }
}

impl fmt::Display for MacroErrKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub(crate) struct ParseMacroError {
    kind: MacroErrKind,
}

impl ParseMacroError {
    fn new(kind: MacroErrKind) -> Self {
        Self { kind }
    }

    pub(crate) fn kind(&self) -> MacroErrKind {
        self.kind
    }
}

impl fmt::Display for ParseMacroError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_fmt(format_args!(
            "Failed to parse macro definition: {}",
            self.kind
        ))
    }
}

impl ::std::error::Error for ParseMacroError {
    fn description(&self) -> &str {
        "Failed to parse macro definition"
    }
}

#[inline]
pub(crate) fn is_valid_macroname(name: &str) -> bool {
    name.starts_with(|c: char| c.is_ascii_alphabetic() || c == '_')
        && name.chars().all(|c| c.is_ascii_alphanumeric() || c == '_')
}

#[derive(Debug, PartialEq)]
pub(crate) struct Macro {
    args: usize,
    body: String,
    fmt: Vec<Result<(usize, usize), usize>>,
}

impl Macro {
    pub(crate) fn expand(&self, args: &[&str]) -> Result<String, MacroErrKind> {
        if args.len() < self.args {
            return Err(MacroErrKind::ArgErr {
                given: args.len(),
                needed: self.args,
            });
        }

        let mut s = String::new();
        for &piece in &self.fmt {
            match piece {
                Ok((start, end)) => {
                    let mut prev = ' ';
                    s.reserve(end - start);
                    // guaranteed to be escaped if created using define_macro(..)
                    for c in self.body[start..end].chars() {
                        if !((c == '{' || c == '}') && prev == c) {
                            prev = c;
                            s.push(c);
                        }
                    }
                }
                Err(n) => {
                    s.push_str(args[n]);
                }
            }
        }
        Ok(s)
    }
}

pub(crate) fn define_macro(def: &str) -> Result<(&str, Macro), ParseMacroError> {
    #[inline]
    fn find_non_escaped(s: &str) -> Option<usize> {
        let mut prev = None;
        let mut idx = None;
        for (n, c) in s.char_indices() {
            if let Some(pc) = prev {
                if c != pc {
                    idx = Some(n - 1);
                    break;
                }
            }
            if c == '{' || c == '}' {
                if prev.is_some() {
                    prev = None;
                } else {
                    prev = Some(c);
                }
            }
        }
        if prev.is_some() && idx.is_none() {
            idx = Some(s.len() - 1)
        }
        idx
    }

    let def_ptr = def.as_ptr() as usize;
    let def = def.trim();
    if !def.starts_with('/') {
        return Err(ParseMacroError::new(MacroErrKind::Invalid));
    }

    let res: Vec<&str> = def[1..].splitn(2, '=').collect();
    let name = res[0].trim_right();
    if res.len() < 2 {
        return Err(ParseMacroError::new(MacroErrKind::Invalid));
    } else if res[1].trim_left().is_empty() {
        return Err(ParseMacroError::new(MacroErrKind::BodyEmpty));
    } else if !is_valid_macroname(name) {
        return Err(ParseMacroError::new(MacroErrKind::NameErr));
    }

    lazy_static! {
        static ref MACRO_TEMPLATE: Regex = Regex::new(r"(\{(\d+)\})").unwrap();
    }
    let body = res[1].trim_left();
    let body_pos = body.as_ptr() as usize - def_ptr;
    let body: String = body.into();
    let mut fmt = Vec::new();
    let mut max_arg_pos = -1;
    let mut last = 0;
    for c in MACRO_TEMPLATE.captures_iter(&body) {
        if let Some(m) = c.get(1) {
            let start = m.start();
            let end = m.end();

            if last != start {
                let s = &body[last..start];
                // make sure the curly braces are escaped.
                if let Some(idx) = find_non_escaped(s) {
                    // skip if the previous segment ends with unbalanced '{'.
                    if idx == start - 1 && body[start - 1..].starts_with('{') {
                        continue;
                    } else {
                        return Err(ParseMacroError::new(MacroErrKind::Unescaped(
                            body_pos + last + idx,
                        )));
                    }
                }
                fmt.push(Ok((last, start)));
            }
            last = end;
            let arg_pos = c.get(2).unwrap().as_str().parse().unwrap();
            max_arg_pos = max_arg_pos.max(arg_pos as isize);
            // push the placeholder.
            fmt.push(Err(arg_pos));
        }
    }
    if last < body.len() {
        let s = &body[last..];
        // make sure the curly braces are escaped.
        if let Some(idx) = find_non_escaped(s) {
            return Err(ParseMacroError::new(MacroErrKind::Unescaped(
                body_pos + last + idx,
            )));
        }
        fmt.push(Ok((last, body.len())));
    }
    Ok((
        name,
        Macro {
            args: (max_arg_pos + 1) as usize,
            body,
            fmt,
        },
    ))
}

#[derive(Debug)]
pub(crate) struct ParseGcError;

impl ParseGcError {
    fn __description(&self) -> &str {
        "Failed to parse GC number from string"
    }
}

impl fmt::Display for ParseGcError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.__description().fmt(f)
    }
}

impl ::std::error::Error for ParseGcError {
    fn description(&self) -> &str {
        self.__description()
    }
}

#[derive(Clone, Copy, Debug)]
pub(crate) struct Gc(i64);

impl Gc {
    fn format<W>(&self, w: &mut W, summarize: bool) -> fmt::Result
    where
        W: fmt::Write,
    {
        if self.0.is_negative() {
            w.write_char('-')?;
        }
        let mut count = 0;
        let mut gc = self.0.abs();
        const GC_UNITS: [char; 5] = ['Q', 'T', 'B', 'M', 'K'];
        for i in 0..GC_UNIT_VALS.len() - 1 {
            if count >= 2 {
                gc = 0;
                break;
            }
            let v = gc / GC_UNIT_VALS[i];
            if v > 0 {
                if summarize {
                    count += 1;
                }
                w.write_fmt(format_args!("{}{}", v, GC_UNITS[i]))?;
                gc -= v * GC_UNIT_VALS[i];
            }
        }
        if gc > 0 {
            w.write_fmt(format_args!("{}GC", gc))
        } else {
            w.write_str("GC")
        }
    }

    pub(crate) fn summarize(&self) -> String {
        let mut s = String::new();
        self.format(&mut s, true).unwrap();
        s
    }
}

impl From<i64> for Gc {
    fn from(n: i64) -> Gc {
        Gc(n)
    }
}

impl ::std::ops::Add for Gc {
    type Output = Gc;

    fn add(self, other: Gc) -> Gc {
        Gc(self.0 + other.0)
    }
}

impl ::std::ops::AddAssign for Gc {
    fn add_assign(&mut self, other: Gc) {
        self.0 += other.0
    }
}

impl ::std::ops::Sub for Gc {
    type Output = Gc;

    fn sub(self, other: Gc) -> Gc {
        Gc(self.0 - other.0)
    }
}

impl ::std::ops::SubAssign for Gc {
    fn sub_assign(&mut self, other: Gc) {
        self.0 -= other.0
    }
}

impl ::std::str::FromStr for Gc {
    type Err = ParseGcError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static!{
            static ref PARSE_GC: Regex = Regex::new(r"(?-u)^(-)?(?:(\d{1,5})Q)?(?:(\d{1,3})T)?(?:(\d{1,3})B)?(?:(\d{1,3})M)?(?:(\d{1,3})K)?(\d{1,3})?GC$").unwrap();
        }
        if let Some(c) = PARSE_GC.captures(s) {
            let mut gc = 0i64;
            // (0) is the complete match, (1) is the sign.
            for i in 2..c.len() {
                c.get(i).map(|m| {
                    gc += s[m.start()..m.end()].parse::<i64>().unwrap() * GC_UNIT_VALS[i - 2]
                });
            }
            c.get(1).map(|_| gc = -gc);
            Ok(Gc(gc))
        } else {
            Err(ParseGcError {})
        }
    }
}

impl fmt::Display for Gc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.format(f, false)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init() -> Completion {
        let mut c = Completion::new();
        let m = json!({
            "xor": {"lib": {"a": null}, "foo_": {}, "foo_bar": {"x": 1}},
            "xo": {"foo": null},
            "xop": {"bar": null},
            "lol": null,
        });
        if let Value::Object(m) = m {
            let autocomplete = AutoComplete {
                user_autos: m,
                count: 3,
            };
            c.assign(autocomplete);
        }
        c
    }

    #[test]
    fn auto_complete_test() {
        let c = init();
        assert_eq!(
            c.complete("#c", false),
            vec!["#chat_pass", "#client_data", "#create_user"]
        );
        assert_eq!(c.complete("x", false), vec!["xo", "xop", "xor"]);
        assert_eq!(c.complete("xo", false), vec!["xo", "xop", "xor"]);
        assert_eq!(c.complete("xor", false), vec![""]);
        assert_eq!(c.complete("tr", false), vec!["ust"]);
        assert_eq!(c.complete("xor.", false), vec!["foo_", "foo_bar", "lib"]);
        assert_eq!(c.complete("lol.", false).is_empty(), true);
        assert_eq!(c.complete("xop.b", false), vec!["ar"]);
        assert_eq!(c.complete("xop.bar", false), vec![""]);
        assert_eq!(c.complete("xor.foo", false), vec!["foo_", "foo_bar"]);
        assert_eq!(c.complete("xor.foo", true), vec!["foo_", "foo_bar"]);
        assert_eq!(c.complete("xor.foo_", false), vec!["foo_", "foo_bar"]);
        assert_eq!(c.complete("xor.foo_", true), vec![" {}"]);
        assert_eq!(c.complete("xor.li", false), vec!["b {\"a\":null}"]);
        assert_eq!(c.complete("xor.foo_bar", true), vec![" {\"x\":1}"]);
        assert_eq!(
            c.complete("kernel.hardline", false),
            vec![" {\"activate\":true}"]
        );
    }

    #[test]
    fn merge_add() {
        let mut c = init();
        let m = json!({
            "xop": {"add": {"a": "hello"}},
            "meh": null,
        });
        if let Value::Object(m) = m {
            let autocomplete = AutoComplete {
                user_autos: m,
                count: 1,
            };
            c.merge(autocomplete, false);
            assert_eq!(c.complete("m", false), vec!["eh"]);
            assert_eq!(c.complete("meh", false), vec![""]);
            assert_eq!(c.complete("meh.", false).is_empty(), true);
            assert_eq!(c.complete("xo", false), vec!["xo", "xop", "xor"]);
            assert_eq!(c.complete("xop", false), vec![""]);
            assert_eq!(c.complete("xop.", false), vec!["add", "bar"]);
            assert_eq!(c.complete("xop.a", false), vec!["dd {\"a\":\"hello\"}"]);
        }
    }

    #[test]
    fn merge_remove() {
        // remove all scripts.
        let mut c = init();
        let m = json!({
            "xor": {"lib": {"a": null}, "foo_": {}, "foo_bar": {"x": 1}},
        });
        if let Value::Object(m) = m {
            let autocomplete = AutoComplete {
                user_autos: m,
                count: 1,
            };
            c.merge(autocomplete, true);
            assert_eq!(c.complete("xo", false), vec!["xo", "xop"]);
            assert_eq!(c.complete("xor", false).is_empty(), true);
            assert_eq!(c.complete("xor.", false).is_empty(), true);
        }

        // remove the user.
        let mut c = init();
        let m = json!({
            "xo": null,
        });
        if let Value::Object(m) = m {
            let autocomplete = AutoComplete {
                user_autos: m,
                count: 1,
            };
            c.merge(autocomplete, true);
            assert_eq!(c.complete("xo", false), vec!["xop", "xor"]);
            assert_eq!(c.complete("xo.", false).is_empty(), true);
            assert_eq!(c.complete("xor.", false), vec!["foo_", "foo_bar", "lib"]);
            assert_eq!(c.complete("xop.", false), vec!["bar"]);
        }

        // remove partial.
        let mut c = init();
        let m = json!({
            "xor": {"lib": {"a": null}, "foo_bar": {"x": 1}},
        });
        if let Value::Object(m) = m {
            let autocomplete = AutoComplete {
                user_autos: m,
                count: 1,
            };
            c.merge(autocomplete, true);
            assert_eq!(c.complete("xo", false), vec!["xo", "xop", "xor"]);
            assert_eq!(c.complete("xor", false), vec![""]);
            assert_eq!(c.complete("xor.", false), vec!["foo_ {}"]);
            assert_eq!(c.complete("xor.f", false), vec!["oo_ {}"]);
        }
    }

    #[test]
    fn chat_filter_test() {
        let s = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n";
        assert_eq!(filter_chat_msg(s), "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n");
        let s = "::;";
        assert_eq!(filter_chat_msg(s), "¡¢Á");
        let s = "TRU5T C0MMUN1CATlOM";
        assert_eq!(filter_chat_msg(s), "¡¢Á¤Ã");
    }

    #[test]
    fn macro_test() {
        use self::MacroErrKind::*;
        assert_eq!(
            define_macro("/0lol"),
            Err(ParseMacroError { kind: Invalid })
        );
        assert_eq!(
            define_macro("/0lol="),
            Err(ParseMacroError { kind: BodyEmpty })
        );
        assert_eq!(
            define_macro("/0lol=foo"),
            Err(ParseMacroError { kind: NameErr })
        );
        assert_eq!(
            define_macro("/_lol_9=foo}"),
            Err(ParseMacroError {
                kind: Unescaped(11),
            })
        );
        assert_eq!(
            define_macro("/_lol_9 = {{foo: \"bar{0}\", ok: {s}}"),
            Err(ParseMacroError {
                kind: Unescaped(31),
            })
        );
        assert_eq!(
            define_macro("/_lol_9 = {{foo: \"bar{0}\", ok: {1}}"),
            Err(ParseMacroError {
                kind: Unescaped(34),
            })
        );
        assert_eq!(
            define_macro("/_lol9_asd=foo"),
            Ok((
                "_lol9_asd",
                Macro {
                    args: 1,
                    body: "foo".into(),
                    fmt: vec![Ok((0, 3))],
                }
            ))
        );
        assert_eq!(
            define_macro("/_lol9 = foo {{\"a\": \"{2}\", \"b\": {0}}} "),
            Ok((
                "_lol9",
                Macro {
                    args: 3,
                    body: "foo {{\"a\": \"{2}\", \"b\": {0}}}".into(),
                    fmt: vec![Ok((0, 12)), Err(2), Ok((15, 23)), Err(0), Ok((26, 28))],
                }
            ))
        );
        assert_eq!(
            define_macro("/_lol9 = foo {{\"a\": \"{2}\", \"b\": {0}}} ")
                .unwrap()
                .1
                .expand(&["hello", "world"]),
            Err(MacroErrKind::ArgErr {
                given: 2,
                needed: 3,
            }),
        );
        assert_eq!(
            define_macro("/_lol9 = foo {{\"a\": \"{2}\", \"b\": {0}}} ")
                .unwrap()
                .1
                .expand(&["123.45", "hello", "world"]),
            Ok(String::from("foo {\"a\": \"world\", \"b\": 123.45}")),
        );
    }

    #[test]
    fn command_args_fmt_test() {
        assert_eq!(
            fmt_command_args("foo {a: \"hello\", \"b\": 232.2}"),
            String::from("foo {\"a\": \"hello\", \"b\": 232.2}"),
        );
    }
}
