with import <nixpkgs> {};

let mozpkgs = fetchFromGitHub {
  owner = "mozilla";
  repo = "nixpkgs-mozilla";
  rev = "a34ede5e90ec484e606480022e8ec1f6cddc8c2c";
  sha256 = "0aigrxi750f9q7gp5wh176ikzxyvgrxwfvwx0ab1nm9c6ia648z5";
};
in
with import "${mozpkgs.out}/rust-overlay.nix" pkgs pkgs;

let nightly = rustChannelOf {date = "2018-05-08"; channel = "nightly"; };
in
stdenv.mkDerivation {
  name = "hackmud-rs";
  buildInputs = [ llvmPackages.libclang nightly.rust ];
  LIBCLANG_PATH = "${llvmPackages.libclang}/lib";
}
