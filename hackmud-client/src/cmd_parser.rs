use std::path::PathBuf;
use pest::{Parser, iterators::Pair};
use hackmud_api::types::{ScriptProperties, ScriptVisibility};
use {Command, ScriptArgs};

#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("commands.pest");

#[derive(Parser)]
#[grammar = "commands.pest"]
struct CommandParser;

fn one_arg_rule_parse<T, F>(pair: Pair<Rule>, f: F) -> T
where
    F: FnOnce(Rule, &str) -> T,
{
    let v: Vec<_> = pair.into_inner().collect();
    assert_eq!(
        v.len(),
        2,
        "command must contain one argument rule preceded by spaces rule"
    );
    let arg_pair = &v[1];
    f(arg_pair.as_rule(), arg_pair.as_str())
}

fn path_unescape(p: &str) -> String {
    assert!(
        p.len() >= 2,
        "path string must be surrounded by double quotes"
    );
    let mut s = String::new();
    let mut escape = false;
    let mut start = 1;
    for (i, c) in p[1..p.len() - 1].chars().enumerate() {
        if escape {
            assert!(
                (c == '"' || c == '\\'),
                "path string must have quotes and backslashes escaped with a backslash"
            );
            s.push(c);
            start = i + 2;
            escape = false;
        } else {
            assert!(
                c != '"',
                "path string must have quotes escaped with backslash"
            );
            if c == '\\' {
                if start < i + 1 {
                    s.push_str(&p[start..i + 1]);
                }
                escape = true;
                continue;
            }
        }
    }
    if start < p.len() - 1 {
        s.push_str(&p[start..p.len() - 1])
    }
    assert!(!escape, "End quote mustn't be escaped");
    s
}

macro_rules! game_object_string_or (
    ($err: expr) => (
        |r, s| match r {
            Rule::game_object => s.into(),
            _ => unreachable!($err),
        }
    )
);

// replace scriptor syntax(#s.user.script) with the scriptor object.
fn script_args_fmt(pair: Pair<Rule>) -> String {
    assert_eq!(
        pair.as_rule(),
        Rule::script_args,
        "pair must be script_args rule"
    );
    let args_span = pair.clone().into_span();
    let mut script_args = String::with_capacity(args_span.end() - args_span.start());
    let mut last_pos = args_span.start_pos();
    for span in pair.into_inner().filter_map(|tl_pair| {
        let mut v: Vec<_> = tl_pair.into_inner().collect();
        assert_eq!(
            v.len(),
            2,
            "top_level_pair must contain js_string rule(key) and (js_value|scriptor) rule(value)"
        );
        let scriptor_pair = v.pop().unwrap();
        if scriptor_pair.as_rule() == Rule::scriptor {
            Some(scriptor_pair.into_span())
        } else {
            None
        }
    }) {
        script_args.push_str(last_pos.span(&span.start_pos()).as_str());
        script_args.push_str(&span.as_str().replace("#s.", "{\"__scriptor\": \""));
        script_args.push_str("\"}");
        last_pos = span.end_pos();
    }
    if !script_args.is_empty() {
        script_args.push_str(last_pos.span(&args_span.end_pos()).as_str());
    } else {
        script_args.push_str(args_span.as_str());
    }
    script_args
}

pub(crate) fn parse_command(line: &[u8]) -> Command {
    let line = match ::std::str::from_utf8(line) {
        Ok(l) => l,
        Err(e) => return Command::ParseError(format!("{}", e)),
    };
    let mut pairs = match CommandParser::parse(Rule::command, line) {
        Ok(p) => p,
        Err(e) => return Command::ParseError(format!("{}", e)),
    };
    let pair = pairs.next();
    assert!(pair.is_some(), "must parse to command rule");
    let cmd_pair = pair.unwrap().into_inner().next();
    assert!(
        cmd_pair.is_some(),
        "command pair must contain one of the command rules"
    );
    let cmd_pair = cmd_pair.unwrap();
    match cmd_pair.as_rule() {
        Rule::help => Command::Help,
        Rule::exit => Command::Exit,
        Rule::status => Command::Status,
        Rule::chat_pass => Command::GetChatPass,
        Rule::revoke_chat_toks => Command::RevokeChatToken,
        Rule::client_data => Command::GetClientData,
        Rule::list_users => Command::ListUsers,
        Rule::notification => {
            Command::EnableNotification(one_arg_rule_parse(cmd_pair, |r, _| match r {
                Rule::off => false,
                Rule::on => true,
                _ => unreachable!("notification rule must have argument as on|off rule"),
            }))
        }
        Rule::delete => Command::DeleteScript(one_arg_rule_parse(
            cmd_pair,
            game_object_string_or!("delete rule must have argument game_object rule"),
        )),
        Rule::set_user => Command::SetUser(one_arg_rule_parse(
            cmd_pair,
            game_object_string_or!("set_user rule must have argument game_object rule"),
        )),
        Rule::create_user => Command::CreateUser(one_arg_rule_parse(
            cmd_pair,
            game_object_string_or!("create_user rule must have argument game_object rule"),
        )),
        Rule::retire_user => Command::RetireUser(one_arg_rule_parse(
            cmd_pair,
            game_object_string_or!("retire_user rule must have argument game_object rule"),
        )),
        Rule::auth => Command::AuthToken(one_arg_rule_parse(cmd_pair, |r, s| match r {
            Rule::auth_token => s.into(),
            _ => unreachable!("auth rule must have argument auth_token rule"),
        })),
        Rule::upload_script => {
            let mut script_name = "";
            let mut path_string = "";
            let mut script_args = String::new();
            let mut prop = ScriptProperties::default();
            for inner_pair in cmd_pair.into_inner() {
                match inner_pair.as_rule() {
                    Rule::game_object => script_name = inner_pair.as_str(),
                    Rule::path_string => path_string = inner_pair.as_str(),
                    Rule::public => prop.set_visibility = Some(ScriptVisibility::Public),
                    Rule::private => prop.set_visibility = Some(ScriptVisibility::Private),
                    Rule::dry => prop.is_dry = true,
                    Rule::shift => prop.is_shift = true,
                    Rule::run => {
                        prop.run_on_upload = true;
                        let mut v: Vec<_> = inner_pair.into_inner().collect();
                        if v.len() == 2 {
                            script_args = script_args_fmt(v.pop().unwrap());
                        } else {
                            assert_eq!(
                                v.len(),
                                0,
                                "run rule should contain either spaces followed by script_args or \
                                 be empty"
                            );
                        }
                    }
                    Rule::spaces1 => {}
                    r => unreachable!(
                        "unrecognized rule {:?} in argument of upload_script rule",
                        r
                    ),
                }
            }
            assert_ne!(script_name, "", "script_name rule can't be empty string");

            Command::UploadScript(
                script_name.into(),
                if path_string.is_empty() {
                    None
                } else {
                    Some(PathBuf::from(path_unescape(path_string)))
                },
                prop,
                ScriptArgs(script_args),
            )
        }
        Rule::download_script => {
            let mut script_name = "";
            let mut path_string = "";
            for inner_pair in cmd_pair.into_inner() {
                match inner_pair.as_rule() {
                    Rule::game_object => script_name = inner_pair.as_str(),
                    Rule::path_string => path_string = inner_pair.as_str(),
                    Rule::spaces1 => {}
                    r => unreachable!(
                        "unrecognized rule {:?} in argument of download_script rule",
                        r
                    ),
                }
            }
            assert_ne!(script_name, "", "script_name rule can't be empty string");

            Command::DownloadScript(
                script_name.into(),
                if path_string.is_empty() {
                    None
                } else {
                    Some(PathBuf::from(path_unescape(path_string)))
                },
            )
        }
        Rule::run_script => {
            let mut script_obj = "";
            let mut script_args = String::new();
            let mut cols = None;
            for inner_pair in cmd_pair.into_inner() {
                match inner_pair.as_rule() {
                    Rule::script_object => script_obj = inner_pair.as_str(),
                    Rule::script_args => script_args = script_args_fmt(inner_pair),
                    Rule::int => cols = inner_pair.as_str().parse().ok(),
                    Rule::spaces1 => {}
                    r => unreachable!("unrecognized rule {:?} in argument of run_script rule", r),
                }
            }
            assert_ne!(script_obj, "", "script_object rule can't be empty string");

            Command::RunScript(script_obj.into(), ScriptArgs(script_args), cols)
        }
        r => unreachable!("command rule mustn't contain rule: {:?}", r),
    }
}
