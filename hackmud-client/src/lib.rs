#![feature(conservative_impl_trait, duration_extras, try_from)]
extern crate app_dirs2;
extern crate bytes;
extern crate failure;
extern crate fern;
extern crate fs2;
#[macro_use]
extern crate futures;
extern crate futures_cpupool;
extern crate hackmud_api;
extern crate humantime;
#[macro_use]
extern crate log;
extern crate pest;
#[macro_use]
extern crate pest_derive;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_signal;
extern crate tokio_timer;

use std::borrow::Cow;
use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;
use std::collections::HashMap;
use std::io::{self, Read, Write};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::fs::{self, File};
use std::str;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use app_dirs2::{AppDataType, AppDirsError, AppInfo};
use bytes::Bytes;
use fs2::FileExt;
use failure::ResultExt;
use humantime::format_rfc3339_seconds;
use log::LevelFilter;
use tokio_core::net::{TcpListener, TcpStream};
use tokio_core::reactor::{Core, Handle};
use tokio_io::AsyncRead;
use tokio_timer::Timer;
use futures::{future, stream, Future, Sink, Stream, stream::StreamFuture};
use futures::sync::{mpsc, oneshot};
use futures_cpupool::CpuPool;
use serde_json::{Map, Value as JsonValue};

use hackmud_api::{Client as ApiClient, Error as ApiError};
use hackmud_api::types::{AutoComplete, ClientData, Notification as ApiNotification,
                         ScriptProperties, UpdateResult, UserInfo};

pub const HACKMUD_CLIENTD_APPINFO: AppInfo = AppInfo {
    name: "hackmud_clientd",
    author: "github.com/jpathy",
};

pub use hackmud_api::HACKMUD_CLIENT_VERSION;

pub const DEFAULT_HACKMUD_CLIENTD_ADDR: &str = "127.0.0.1:60497";

const LOGSIZE_LIMIT: u64 = 100 * 1024;
const SCRIPT_LIMIT: u64 = 50 * 1024;

const REQUEST_TIMEOUT: Duration = Duration::from_secs(15);
const TIMER_TICK: Duration = Duration::from_millis(250);

const TIMEOUT_ERROR: ResponseData = ResponseData::Error(Cow::Borrowed("Request timed out."));

#[cfg_attr(rustfmt, rustfmt_skip)]
pub static HELP_STRING : &str = r#"
Available commands:
  #help : Prints this text.
  #exit : Shutdown this server.
  #auth `hTOKEN` :
      Use the authentication token `hTOKEN` for all request to hackmud server.
      It should be set prior to running commands; otherwise, requests will fail with auth error.
  #status : Get the status (user, notification, auth) of this connection.
  #chat_pass : Get chat pass.
  #revoke_chat_tokens : Revoke all chat tokens.
  #client_data : Get Client data. (autocompletes, ignored users etc.)
  #list_users : Lists the available and retired users.
  #create_user `hUSER` : Create a user `hUSER`.
  #retire_user `hUSER` : Retire user `hUSER`.
  #user `hUSER` :
      Log in as `hUSER` and execute all commands as `hUSER` for this connection.
      No default, MUST be set before issuing following commands.
  #notifications (`Lon`|`Loff`) :
      Turn on/off notifications(default `Loff`).
      When turned on, server sends "Notification"s as it recieves from hackmud server.
  #up `hSCRIPTNAME` [`hFILEPATH`] [`Lprivate`|`Lpublic`] [`Ldry`] [`Lshift`] [`Lrun` [`hSCRIPTARGS`]] :
      Uploads the contents of `hFILEPATH` OR `hSCRIPTNAME`.js from user's "script directory" as `hSCRIPTNAME`.
      Takes optional properties and trigger execution on upload.
  #down `hSCRIPTNAME` [`hFILEPATH`] :
      Downloads the script `hSCRIPTNAME` for the current user and save it to `hFILEPATH`.
  #delete `hSCRIPTNAME` :
      Delete the script `hSCRIPTNAME` from hackmud server for the current user.
      Local script is untouched.
  [`CUSER.`]`LSCRIPTNAME` [`hSCRIPTARGS`] [`hCOLS`]:
      Run the script `CUSER.``LSCRIPTNAME` on the server.
      `hCOLS` is the number of columns used by scripts as a hint for formatting their output.
      `hUSER` defaults to the current user set for this connection.

IMPORTANT INFORMATION:
  "Notification"s are a vector of JSON records; "Response" to commands are JSON records.
  For more details look into the crate docs.
  All commands must be followed by atleast one CRLF/LF.
  `hSCRIPTARGS` is a JSON object and mustn't contain CRLF/LF(see above).
"#;

#[derive(Clone, Debug, Eq, PartialEq)]
struct ScriptArgs(String);

impl AsRef<str> for ScriptArgs {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<ScriptArgs> for String {
    fn from(script_args: ScriptArgs) -> String {
        script_args.0
    }
}

// TODO: rename crate to hackmud-client and bin to hackmud-clientd; Add bot module; with all the commands; should be blocking api.
#[derive(Clone, Debug)]
enum Command {
    // technically not a user Command but used to report error in Decoder.
    ParseError(String),
    // actual Commands.
    Help,
    Exit,
    Status,
    AuthToken(String),
    EnableNotification(bool),
    GetClientData,
    GetChatPass,
    RevokeChatToken,
    ListUsers,
    CreateUser(String),
    RetireUser(String),
    SetUser(String),
    UploadScript(String, Option<PathBuf>, ScriptProperties, ScriptArgs),
    DeleteScript(String),
    DownloadScript(String, Option<PathBuf>),
    RunScript(String, ScriptArgs, Option<u32>),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Response {
    pub success: bool,
    pub data: ResponseData,
}

impl Response {
    pub fn is_success(&self) -> bool {
        self.success
    }

    pub fn is_error(&self) -> bool {
        match self.data {
            ResponseData::AuthError(_) | ResponseData::Error(_) => true,
            _ => false,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ConnectionStatus {
    pub authenticated: bool,
    pub notifications: bool,
    pub user: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ScriptResult {
    pub script_name: String,
    pub retval: JsonValue,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "t", content = "c")]
pub enum ResponseData {
    Empty,
    Status(ConnectionStatus),
    AuthError(Cow<'static, str>),
    Error(Cow<'static, str>),
    Message(Cow<'static, str>),
    ClientData(ClientData),
    UserInfo(UserInfo),
    UploadResult(UpdateResult),
    ScriptResult(ScriptResult),
}

pub use response::{ResponseError, ResponseResult};

macro_rules! into_fn {
    ($fn_id:ident(Empty) -> $res_ty:ty) => {
        into_fn!(__impl $fn_id(ResponseData::Empty => ()) -> $res_ty);
    };
    ($fn_id:ident($($variant:ident)|*) -> $res_ty:ty) => {
        into_fn!(__impl $fn_id($(ResponseData::$variant(d) => d.into()),*) -> $res_ty);
    };
    (__impl $fn_id:ident($($variant:pat => $out:expr),*) -> $res_ty:ty) => {
        fn $fn_id(self) -> Option<Result<$res_ty, ResponseError>> {
            match self {
                $($variant => Some(Ok($out)),)*
                ResponseData::AuthError(s) => Some(Err(ResponseError::new(true, s.into()))),
                ResponseData::Error(s) => Some(Err(ResponseError::new(false, s.into()))),
                _ => None,
            }
        }
    };
}

macro_rules! into_fns {
    ($($fn_id:ident($($variant:ident)|*) -> $res_ty:ty),* $(,)*) => {
        $(into_fn!($fn_id($($variant)|*) -> $res_ty);)*
    };
}

impl ResponseData {
    into_fns![
        into_empty(Empty) -> (),
        into_message(Message) -> String,
        into_status(Status) -> ConnectionStatus,
        into_client_data(ClientData) -> ClientData,
        into_user_info(UserInfo) -> UserInfo,
        into_upload_result(UploadResult|ScriptResult) -> response::UploadResult,
        into_script_result(ScriptResult) -> ScriptResult,
    ];
}

pub mod response {
    use std::convert::{AsRef, From, TryFrom};
    use std::{error, fmt};
    use super::{ClientData, ConnectionStatus, Response, ScriptResult, UpdateResult, UserInfo};

    macro_rules! impl_try_from_response {
        ($(($for_ty:ident, $method:ident)),* $(,)*) => {
            $(impl TryFrom<Response> for $for_ty {
                type Error = ConvertError;
                fn try_from(resp: Response) -> Result<$for_ty, Self::Error> {
                    let success = resp.success;
                    resp.data.$method()
                        .map(|res| Ok($for_ty(res.map(|ok| (success, ok)))))
                        .unwrap_or(Err(ConvertError(stringify!($for_ty))))
                }
            })*
        }
    }

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct ResponseError {
        is_auth_err: bool,
        error: String,
    }

    impl ResponseError {
        pub(super) fn new(is_auth_err: bool, error: String) -> Self {
            Self { is_auth_err, error }
        }

        pub fn is_auth_error(&self) -> bool {
            self.is_auth_err
        }
    }

    impl AsRef<str> for ResponseError {
        fn as_ref(&self) -> &str {
            &self.error
        }
    }

    impl fmt::Display for ResponseError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let prefix = if self.is_auth_err {
                "Authentication failed: "
            } else {
                ""
            };
            write!(f, "{}{}", prefix, self.error)
        }
    }

    impl error::Error for ResponseError {
        fn description(&self) -> &str {
            if self.is_auth_err {
                "Auth failure response"
            } else {
                "Error response"
            }
        }
    }

    pub type ResponseResult<T> = Result<(bool, T), ResponseError>;

    #[derive(Clone, Debug, Eq, PartialEq)]
    pub struct ConvertError(&'static str);

    impl fmt::Display for ConvertError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "ResponseResult of type {} not available", self.0)
        }
    }

    impl error::Error for ConvertError {
        fn description(&self) -> &str {
            "Response convertion error"
        }
    }

    impl From<ConvertError> for ResponseError {
        fn from(e: ConvertError) -> Self {
            ResponseError {
                is_auth_err: false,
                error: format!("{}", e),
            }
        }
    }

    pub enum UploadResult {
        Update(UpdateResult),
        Run(ScriptResult),
    }

    impl From<UpdateResult> for UploadResult {
        fn from(res: UpdateResult) -> Self {
            UploadResult::Update(res)
        }
    }

    impl From<ScriptResult> for UploadResult {
        fn from(res: ScriptResult) -> Self {
            UploadResult::Run(res)
        }
    }

    pub struct HelpResponse(pub ResponseResult<String>);
    pub struct StatusResponse(pub ResponseResult<ConnectionStatus>);
    pub struct AuthResponse(pub ResponseResult<()>);
    pub struct ChatPassResponse(pub ResponseResult<String>);
    pub struct RevokeChatTokensResponse(pub ResponseResult<()>);
    pub struct ClientDataResponse(pub ResponseResult<ClientData>);
    pub struct ListUsersResponse(pub ResponseResult<UserInfo>);
    pub struct CreateUserResponse(pub ResponseResult<String>);
    pub struct RetireUserResponse(pub ResponseResult<String>);
    pub struct SetUserResponse(pub ResponseResult<String>);
    pub struct EnableNotificationResponse(pub ResponseResult<()>);
    pub struct UploadScriptResponse(pub ResponseResult<UploadResult>);
    pub struct DownloadScriptResponse(pub ResponseResult<()>);
    pub struct DeleteScriptResponse(pub ResponseResult<String>);
    pub struct RunScriptResponse(pub ResponseResult<ScriptResult>);

    #[cfg_attr(rustfmt, rustfmt_skip)]
    impl_try_from_response![
        (HelpResponse, into_message),
        (StatusResponse, into_status),
        (AuthResponse, into_empty),
        (ChatPassResponse, into_message),
        (RevokeChatTokensResponse, into_empty),
        (ClientDataResponse, into_client_data),
        (ListUsersResponse, into_user_info),
        (CreateUserResponse, into_message),
        (RetireUserResponse, into_message),
        (SetUserResponse, into_message),
        (EnableNotificationResponse, into_empty),
        (UploadScriptResponse, into_upload_result),
        (DownloadScriptResponse, into_empty),
        (DeleteScriptResponse, into_message),
        (RunScriptResponse, into_script_result),
    ];
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Notification {
    #[serde(rename = "epoch")]
    pub timestamp: u64,
    pub data: NotificationData,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Direction {
    From,
    To,
}

impl std::fmt::Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match *self {
                Direction::From => "from",
                Direction::To => "to",
            }
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "t", content = "c")]
pub enum NotificationData {
    Exit,
    AutoComplete {
        autos: AutoComplete,
        remove: bool,
    },
    ChatJoin {
        channel: String,
        user: String,
        msg: String,
    },
    ChatLeave {
        channel: String,
        user: String,
        msg: String,
    },
    ChatSend {
        channel: String,
        user: String,
        msg: String,
    },
    ChatTell {
        dir: Direction,
        user: String,
        msg: String,
    },
    Xfer {
        dir: Direction,
        user: String,
        amount: String,
        memo: Option<String>,
    },
    LocCaller(String),
    SysStatus {
        breach: Option<bool>,
        hardline: Option<f64>,
        tutorial: String,
    },
    Upgrade {
        action: String,
        name: String,
        rarity: u8,
    },
}

// Processes an ApiNotification to produce either a known notification type or a script result.
fn process_notification(
    ApiNotification {
        script_name,
        timestamp,
        run_id,
        result: hackmud_api::types::ScriptResult { data, .. },
        ..
    }: ApiNotification,
) -> Result<Option<Notification>, (String, String, JsonValue)> {
    #[inline]
    fn get_opt_str(m: &mut Map<String, JsonValue>, k: &str) -> Option<String> {
        m.get_mut(k).and_then(|val| match val.take() {
            JsonValue::String(s) => Some(s),
            _ => None,
        })
    }

    #[inline]
    fn get_str(m: &mut Map<String, JsonValue>, k: &str) -> String {
        get_opt_str(m, k).unwrap_or_else(|| "".into())
    }

    use hackmud_api::types::ScriptData;
    match data {
        ScriptData::RetValue(val) => Err((script_name, run_id, val)),
        ScriptData::NotificationData(mut data) => {
            Ok(match script_name.as_ref() {
                "autocomplete" => match serde_json::from_str(&get_str(&mut data, "autos")) {
                    Ok(autos) => {
                        let remove = data.get("remove")
                            .and_then(|b| b.as_bool())
                            .unwrap_or(false);
                        Some(NotificationData::AutoComplete { autos, remove })
                    }
                    Err(e) => {
                        error!("Failed to parse autocomplete notification: {}", e);
                        None
                    }
                },
                "chats.send" => Some(NotificationData::ChatSend {
                    channel: get_str(&mut data, "channel"),
                    user: get_str(&mut data, "user"),
                    msg: get_str(&mut data, "msg"),
                }),
                "chats.join" => Some(NotificationData::ChatJoin {
                    channel: get_str(&mut data, "channel"),
                    user: get_str(&mut data, "user"),
                    msg: get_str(&mut data, "msg"),
                }),
                "chats.leave" => Some(NotificationData::ChatLeave {
                    channel: get_str(&mut data, "channel"),
                    user: get_str(&mut data, "user"),
                    msg: get_str(&mut data, "msg"),
                }),
                "chats.tell" => {
                    let (dir, user) = if data.get("from_user").is_some() {
                        (Direction::From, get_str(&mut data, "from_user"))
                    } else {
                        (Direction::To, get_str(&mut data, "to_user"))
                    };

                    Some(NotificationData::ChatTell {
                        dir,
                        user,
                        msg: get_str(&mut data, "msg"),
                    })
                }
                "sys.loc" => Some(NotificationData::LocCaller(get_str(
                    &mut data,
                    "script_name",
                ))),
                "accts.xfer" => {
                    let (dir, user) = if data.get("direction")
                        .map(|dir| dir == "from")
                        .unwrap_or(false)
                    {
                        (Direction::From, get_str(&mut data, "sender"))
                    } else {
                        (Direction::To, get_str(&mut data, "recipient"))
                    };
                    Some(NotificationData::Xfer {
                        dir,
                        user,
                        amount: get_str(&mut data, "amount"),
                        memo: get_opt_str(&mut data, "memo"),
                    })
                }
                "sys.status" => {
                    let breach = data.get("breach").and_then(|b| b.as_bool());
                    let hardline = data.get("remaining").and_then(|f| f.as_f64());
                    Some(NotificationData::SysStatus {
                        breach,
                        hardline,
                        tutorial: get_str(&mut data, "tutorial"),
                    })
                }
                "sys.upgrades" => {
                    let name: String;
                    let action: String;
                    if let Some(s) = get_opt_str(&mut data, "name") {
                        name = s;
                        action = get_str(&mut data, "action");
                    } else if let Some(s) = get_opt_str(&mut data, "loaded") {
                        name = s;
                        action = "loaded".into();
                    } else if let Some(s) = get_opt_str(&mut data, "unloaded") {
                        name = s;
                        action = "unloaded".into();
                    } else if let Some(s) = get_opt_str(&mut data, "error") {
                        trace!("Upgrade transfer failed: {}", s);
                        return Ok(None);
                    } else {
                        error!("Unknown sys.upgrades notification: {:?}", data);
                        return Ok(None);
                    }
                    let rarity = data.get("rarity").and_then(|f| f.as_u64()).unwrap_or(0) as u8;
                    Some(NotificationData::Upgrade {
                        name,
                        rarity,
                        action,
                    })
                }
                _ => {
                    error!(
                        "Currently not processing notification: {:?} from script: {}",
                        data, script_name
                    );
                    None
                }
            }.map(|data| Notification { timestamp, data }))
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
pub enum Message {
    Response(Response),
    Notifications(Vec<Notification>),
}

mod cmd_parser;
pub mod codec {
    use super::io;
    use super::bytes::{BufMut, Bytes, BytesMut};
    use super::tokio_io::codec::{Decoder, Encoder};
    use super::serde_json::{from_slice as json_from_slice, Error as JsonError};
    use super::cmd_parser::parse_command;
    use super::{Command, Message};

    #[derive(Default)]
    pub struct ClientCodec {
        next_index: usize,
    }

    impl ClientCodec {
        pub fn new() -> Self {
            Self { next_index: 0 }
        }
    }

    impl Encoder for ClientCodec {
        type Item = String;
        type Error = io::Error;

        fn encode(&mut self, data: String, buf: &mut BytesMut) -> io::Result<()> {
            let data = data.trim_right();
            buf.reserve(data.len() + 1);
            buf.put_slice(data.as_bytes());
            buf.put_u8(b'\n');
            Ok(())
        }
    }

    #[derive(Debug)]
    pub enum MsgDecodeError {
        Io(io::Error),
        Json(JsonError),
    }

    impl ::std::fmt::Display for MsgDecodeError {
        fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
            match *self {
                MsgDecodeError::Io(ref e) => e.fmt(f),
                MsgDecodeError::Json(ref e) => e.fmt(f),
            }
        }
    }

    impl From<JsonError> for MsgDecodeError {
        fn from(e: JsonError) -> Self {
            MsgDecodeError::Json(e)
        }
    }

    impl From<io::Error> for MsgDecodeError {
        fn from(e: io::Error) -> Self {
            MsgDecodeError::Io(e)
        }
    }

    impl Decoder for ClientCodec {
        type Item = Message;
        type Error = MsgDecodeError;

        fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Message>, MsgDecodeError> {
            if let Some(newline_offset) = buf[self.next_index..].iter().position(|b| *b == b'\n') {
                let newline_index = newline_offset + self.next_index;
                let line = buf.split_to(newline_index + 1);
                let line = &line[..line.len() - 1];
                let line = without_carriage_return(line);
                let msg = json_from_slice(line)?;
                self.next_index = 0;
                Ok(Some(msg))
            } else {
                self.next_index = buf.len();
                Ok(None)
            }
        }

        fn decode_eof(&mut self, buf: &mut BytesMut) -> Result<Option<Message>, MsgDecodeError> {
            Ok(match self.decode(buf)? {
                Some(frame) => Some(frame),
                None => {
                    if buf.is_empty() || buf == &b"\r"[..] {
                        None
                    } else {
                        let line = without_carriage_return(&buf[..]);
                        let msg = json_from_slice(line)?;
                        Some(msg)
                    }
                }
            })
        }
    }

    pub(super) struct ServerCodec {
        next_index: usize,
    }

    impl ServerCodec {
        pub(super) fn new() -> Self {
            Self { next_index: 0 }
        }
    }

    fn without_carriage_return(s: &[u8]) -> &[u8] {
        if let Some(&b'\r') = s.last() {
            &s[..s.len() - 1]
        } else {
            s
        }
    }

    // Similar to LinesCodec but converts utf8 decoding error into Command::ParseError.
    // use parse_command to yield a Command.
    impl Decoder for ServerCodec {
        type Item = Command;
        type Error = io::Error;

        fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Command>, io::Error> {
            if let Some(newline_offset) = buf[self.next_index..].iter().position(|b| *b == b'\n') {
                let newline_index = newline_offset + self.next_index;
                let line = buf.split_to(newline_index + 1);
                let line = &line[..line.len() - 1];
                let line = without_carriage_return(line);
                let cmd = parse_command(line);
                self.next_index = 0;
                Ok(Some(cmd))
            } else {
                self.next_index = buf.len();
                Ok(None)
            }
        }

        fn decode_eof(&mut self, buf: &mut BytesMut) -> Result<Option<Command>, io::Error> {
            Ok(match self.decode(buf)? {
                Some(frame) => Some(frame),
                None => {
                    if buf.is_empty() || buf == &b"\r"[..] {
                        None
                    } else {
                        let line = without_carriage_return(&buf[..]);
                        let cmd = parse_command(line);
                        Some(cmd)
                    }
                }
            })
        }
    }

    // Writes json encoded bytes separated by newline.
    impl Encoder for ServerCodec {
        type Item = Bytes;
        type Error = io::Error;

        fn encode(&mut self, data: Bytes, buf: &mut BytesMut) -> io::Result<()> {
            buf.reserve(data.len() + 1);
            buf.put_slice(&data);
            buf.put_u8(b'\n');
            Ok(())
        }
    }

}

pub mod client {
    use super::{Rc, RefCell, std::collections::VecDeque, std::convert::TryInto};
    use super::{io, mpsc, oneshot};
    use super::{AsyncRead, Future, Sink, SocketAddr, Stream, TcpStream, future::Executor,
                future::FutureResult};
    use super::{Message, Notification, Response, ScriptResult, codec::ClientCodec, response::*};

    #[derive(Clone)]
    pub struct Client {
        addr: SocketAddr,
        tx: Option<mpsc::Sender<(String, oneshot::Sender<Response>)>>,
        handle: Option<Rc<oneshot::SpawnHandle<(), io::Error>>>,
    }

    // TODO: add more command functions.
    impl Client {
        pub fn new(addr: SocketAddr) -> Self {
            Self {
                addr,
                tx: None,
                handle: None,
            }
        }

        pub fn spawn_message_processor<E>(
            &mut self,
            executor: &E,
            stream_buf_size: usize,
        ) -> impl Stream<Item = Vec<Notification>, Error = ()>
        where
            E: Executor<oneshot::Execute<Box<Future<Item = (), Error = io::Error>>>>,
        {
            // Cancel the previous processor future.
            drop(self.handle.take());
            let (tx, rx) = mpsc::channel(0);
            let (mut ntx, nrx) = mpsc::channel(stream_buf_size);
            self.tx = Some(tx);
            let fut: Box<Future<Item = (), Error = io::Error>> = Box::new(
                TcpStream::connect2(&self.addr).and_then(move |stream| {
                    let (wr, rd) = stream.framed(ClientCodec::new()).split();
                    let cur_tx = Rc::new(RefCell::new(VecDeque::new()));

                    let cur_tx_wr = cur_tx.clone();
                    let writer = rx.map(move |(msg, otx)| {
                        cur_tx_wr.borrow_mut().push_back(otx);
                        msg
                    }).map_err(|_| io::Error::new(io::ErrorKind::Other, "Client dropped"))
                        .forward(wr)
                        .map(|_| ());

                    let reader = rd.map_err(|_| {
                        io::Error::new(
                            io::ErrorKind::InvalidData,
                            format!("Invalid message: {}", 1),
                        )
                    }).for_each(move |msg| {
                        use future::{ok, Either, IntoFuture};

                        match msg {
                            Message::Response(resp) => {
                                debug!("Received response: {:?}", resp);
                                Either::A(
                                    cur_tx
                                        .borrow_mut()
                                        .pop_front()
                                        .expect(
                                            "Protocol violation: each request should receive \
                                             exactly one response",
                                        )
                                        .send(resp)
                                        .map_err(|r| {
                                            io::Error::new(
                                                io::ErrorKind::Other,
                                                format!("Failed to process response: {:?}", r),
                                            )
                                        })
                                        .into_future(),
                                )
                            }
                            Message::Notifications(notes) => {
                                // We don't want to error out, if the stream was dropped by the user.
                                Either::B(if ntx.poll_ready().is_ok() {
                                    Either::A(ntx.clone().send(notes).map(|_| ()).or_else(|e| {
                                        error!("notification stream dropped: {}", e);
                                        Ok(())
                                    }))
                                } else {
                                    Either::B(ok(()))
                                })
                            }
                        }
                    });
                    reader.select(writer).map(|_| ()).map_err(|(e, _)| e)
                }),
            );
            self.handle = Some(Rc::new(oneshot::spawn(fut, executor)));
            nrx
        }

        #[inline]
        pub fn get_response<S: Into<String>>(
            &self,
            cmd: S,
        ) -> impl Future<Item = Response, Error = ResponseError> {
            let (otx, orx) = oneshot::channel();
            let tx = self.tx
                .as_ref()
                .expect("Must spawn message processor before calling this function.")
                .clone();
            tx.send((cmd.into(), otx))
                .map_err(|e| {
                    ResponseError::new(false, format!("Failed to send to message processor: {}", e))
                })
                .and_then(|_| {
                    orx.map_err(|e| {
                        ResponseError::new(
                            false,
                            format!("Message processor dropped sender: {}", e),
                        )
                    })
                })
        }

        pub fn set_user(
            &self,
            user: &str,
        ) -> impl Future<Item = (bool, String), Error = ResponseError> {
            self.get_response(format!("#user {}", user))
                .and_then(|resp| {
                    let resp: Result<SetUserResponse, ResponseError> =
                        resp.try_into().map_err(|e: ConvertError| e.into());
                    FutureResult::from(resp.and_then(|r| r.0))
                })
        }

        pub fn set_notifications(
            &self,
            b: bool,
        ) -> impl Future<Item = bool, Error = ResponseError> {
            self.get_response(format!("#notifications {}", if b { "on" } else { "off" }))
                .and_then(|resp| {
                    let resp: Result<EnableNotificationResponse, ResponseError> =
                        resp.try_into().map_err(|e: ConvertError| e.into());
                    FutureResult::from(resp.and_then(|r| r.0.map(|v| v.0)))
                })
        }

        pub fn run_script(
            &self,
            script_name: &str,
            script_args: &str,
        ) -> impl Future<Item = ScriptResult, Error = ResponseError> {
            self.get_response(format!("{} {}", script_name, script_args))
                .and_then(|resp| {
                    let resp: Result<RunScriptResponse, ResponseError> =
                        resp.try_into().map_err(|e: ConvertError| e.into());
                    FutureResult::from(resp.and_then(|r| r.0.map(|v| v.1)))
                })
        }
    }
}

// Provides an extra cancel() method to stop polling underlying stream
// and poll until the running future to finish.
mod extra_future {
    use futures::{Async, Future, IntoFuture, Poll, Stream};

    #[derive(Debug)]
    #[must_use = "streams do nothing unless polled"]
    pub struct ForEach<S, F, U>
    where
        U: IntoFuture,
    {
        stop: bool,
        stream: S,
        f: F,
        fut: Option<U::Future>,
    }

    impl<S, F, U: IntoFuture> ForEach<S, F, U> {
        pub fn get_mut(&mut self) -> &mut S {
            &mut self.stream
        }

        pub fn cancel(&mut self) {
            self.stop = true;
        }
    }

    impl<S, F, U> ForEach<S, F, U>
    where
        S: Stream,
        F: FnMut(S::Item) -> U,
        U: IntoFuture<Item = (), Error = S::Error>,
    {
        pub fn new(s: S, f: F) -> Self {
            ForEach {
                stop: false,
                stream: s,
                f: f,
                fut: None,
            }
        }
    }

    impl<S, F, U> Future for ForEach<S, F, U>
    where
        S: Stream,
        F: FnMut(S::Item) -> U,
        U: IntoFuture<Item = (), Error = S::Error>,
    {
        type Item = ();
        type Error = S::Error;

        fn poll(&mut self) -> Poll<(), S::Error> {
            loop {
                if let Some(mut fut) = self.fut.take() {
                    if fut.poll()?.is_not_ready() {
                        self.fut = Some(fut);
                        return Ok(Async::NotReady);
                    }
                }

                if !self.stop {
                    match try_ready!(self.stream.poll()) {
                        Some(e) => self.fut = Some((self.f)(e).into_future()),
                        None => break,
                    }
                } else {
                    break;
                }
            }
            Ok(Async::Ready(()))
        }
    }

    /// A future used to collect all the results of a stream into one generic type.
    ///
    /// This future is returned by the `Stream::fold` method.
    #[derive(Debug)]
    #[must_use = "streams do nothing unless polled"]
    pub struct Fold<S, F, Fut, T>
    where
        Fut: IntoFuture,
    {
        stop: bool,
        stream: S,
        f: F,
        state: State<T, Fut::Future>,
    }

    #[derive(Debug)]
    enum State<T, F>
    where
        F: Future,
    {
        /// Placeholder state when doing work
        Empty,

        /// Ready to process the next stream item; current accumulator is the `T`
        Ready(T),

        /// Working on a future the process the previous stream item
        Processing(F),
    }

    impl<S, F, Fut, T> Fold<S, F, Fut, T>
    where
        S: Stream,
        F: FnMut(T, S::Item) -> Fut,
        Fut: IntoFuture<Item = T>,
        S::Error: From<Fut::Error>,
    {
        pub fn new(s: S, f: F, t: T) -> Self {
            Fold {
                stop: false,
                stream: s,
                f: f,
                state: State::Ready(t),
            }
        }

        pub fn cancel(&mut self) {
            self.stop = true;
        }
    }

    impl<S, F, Fut, T> Future for Fold<S, F, Fut, T>
    where
        S: Stream,
        F: FnMut(T, S::Item) -> Fut,
        Fut: IntoFuture<Item = T>,
        S::Error: From<Fut::Error>,
    {
        type Item = T;
        type Error = S::Error;

        fn poll(&mut self) -> Poll<T, S::Error> {
            loop {
                match ::std::mem::replace(&mut self.state, State::Empty) {
                    State::Empty => panic!("cannot poll Fold twice"),
                    State::Ready(state) => {
                        if self.stop {
                            return Ok(Async::Ready(state));
                        } else {
                            match self.stream.poll()? {
                                Async::Ready(Some(e)) => {
                                    let future = (self.f)(state, e);
                                    let future = future.into_future();
                                    self.state = State::Processing(future);
                                }
                                Async::Ready(None) => return Ok(Async::Ready(state)),
                                Async::NotReady => {
                                    self.state = State::Ready(state);
                                    return Ok(Async::NotReady);
                                }
                            }
                        }
                    }
                    State::Processing(mut fut) => match fut.poll()? {
                        Async::Ready(state) => self.state = State::Ready(state),
                        Async::NotReady => {
                            self.state = State::Processing(fut);
                            return Ok(Async::NotReady);
                        }
                    },
                }
            }
        }
    }
}

// Used by Timer::timeout(..) with hackmud_api futures to deal with timeouts.
enum ApiTimeoutError {
    Timeout,
    Api(ApiError),
}

impl<F> From<tokio_timer::TimeoutError<F>> for ApiTimeoutError {
    fn from(e: tokio_timer::TimeoutError<F>) -> ApiTimeoutError {
        if let tokio_timer::TimeoutError::Timer(_, e) = e {
            error!("Timer error: {}", e);
        }
        ApiTimeoutError::Timeout
    }
}

#[inline]
fn mpsc_send_json<T>(
    tx: mpsc::Sender<Bytes>,
    data: T,
) -> Box<Future<Item = mpsc::Sender<Bytes>, Error = ()>>
where
    T: serde::Serialize,
{
    Box::new(
        tx.send(
            serde_json::to_vec(&data)
                .expect("Json serialization mustn't fail")
                .into(),
        ).map_err(|e| warn!("Failed to send response to client receiver: {}", e)),
    )
}

#[inline]
fn oneshot_send_response(tx: oneshot::Sender<Response>, resp: Response) {
    tx.send(resp).unwrap_or_else(|resp| {
        warn!(
            "Command processor failed to send response: {:?} to a client",
            resp
        )
    })
}

// Type of message used by the channel to send commands to command processors.
type CmdMessage = (oneshot::Sender<Response>, Command);

// Connection state and the associated future for clients.
#[derive(Debug)]
struct ClientState {
    user: Option<String>,
    subscribed: bool,
    handle: Option<oneshot::SpawnHandle<(), ()>>,
}

// Per user state and the associated future.
struct UserTask {
    // Tracks the clients that receive notifications/send commands.
    subscribers: HashMap<SocketAddr, Option<mpsc::Sender<Bytes>>>,
    // Used by the per-user command processor future.
    cmd_tx: mpsc::Sender<CmdMessage>,
    // Can only run one script at a time(remote server design).
    // Notification poller future signals when we receive the result of script.
    now_running: Option<(String, oneshot::Sender<ScriptResult>)>,
    handle: Option<oneshot::SpawnHandle<(), ()>>,
}

// State used by all clients and user-tasks and initialization.
struct Inner {
    authenticated: bool,
    // Shouldn't be used by anyone else. Should use SharedState::exit_server() instead.
    server_exit_tx: Option<oneshot::Sender<()>>,
    // Shared future that other tasks(clients/user_tasks) can use to handle termination event.
    exit_signal: future::Shared<Box<Future<Item = (), Error = ()>>>,
    // Used to spawn user_tasks. Might be unncessary when we upgrade to newer version.
    handle: Handle,
    // Used for file io in user_tasks.
    pool: CpuPool,
    timer: Timer,
    // Used by the non-user command processor.
    non_user_cmd_tx: mpsc::Sender<CmdMessage>,
    hackmud_client: ApiClient,
    // All alive clients.
    clients: HashMap<SocketAddr, ClientState>,
    // All running user_tasks.
    user_tasks: HashMap<String, UserTask>,
}

impl Inner {
    fn new(
        executor: Handle,
        hackmud_client: ApiClient,
        non_user_cmd_tx: mpsc::Sender<CmdMessage>,
    ) -> Self {
        let (tx, rx) = oneshot::channel();
        Self {
            authenticated: false,
            handle: executor,
            pool: CpuPool::new_num_cpus(),
            server_exit_tx: Some(tx),
            exit_signal: (Box::new(rx.map_err(|e| error!("exit_signal error: {}", e)))
                as Box<Future<Item = (), Error = ()>>)
                .shared(),
            non_user_cmd_tx,
            hackmud_client: hackmud_client,
            timer: tokio_timer::wheel().tick_duration(TIMER_TICK).build(),
            clients: HashMap::new(),
            user_tasks: HashMap::new(),
        }
    }
}

// Wrapper around Inner with interior mutability and reference counting.
#[derive(Clone)]
struct SharedState {
    inner: Rc<RefCell<Inner>>,
}

impl SharedState {
    fn new(
        executor: Handle,
        hackmud_client: ApiClient,
        non_user_cmd_tx: mpsc::Sender<CmdMessage>,
    ) -> Self {
        Self {
            inner: Rc::new(RefCell::new(Inner::new(
                executor,
                hackmud_client,
                non_user_cmd_tx,
            ))),
        }
    }

    #[inline]
    fn inner_ref(&self) -> Ref<Inner> {
        self.inner.borrow()
    }

    #[inline]
    fn inner_mut(&self) -> RefMut<Inner> {
        self.inner.borrow_mut()
    }

    // Use this to signal termination event.
    #[inline]
    fn exit_server(&self) {
        self.inner_mut().server_exit_tx.take().map(|exit_tx| {
            exit_tx.send(()).unwrap_or_else(|_| {
                info!("Another task has already sent exit signal to server");
            })
        });
    }

    // Create a new user_task that runs a periodic notification poller and command processor.
    fn create_user_task(&self, user: &str) {
        use future::Either;
        use future::Loop;

        // It is used to interrupt the poller sleeping(and poll immediately) when we receive a command.
        let (ping_tx, ping_rx) = mpsc::channel(0);
        // `CmdMessage` channel.
        let (cmd_tx, cmd_rx) = mpsc::channel(0);

        let shared = self.clone();
        let user1 = user.to_string();

        // Future which produces a `Response` with script result.
        // Succeeds when the notification poller receives the result from server
        // and sends the matching result using Inner::now_running.
        fn script_result(
            user: String,
            script: String,
            run_id: String,
            shared: &SharedState,
        ) -> impl Future<Item = Response, Error = ApiError> {
            let (run_tx, run_rx) = oneshot::channel();
            let mut inner_mut = shared.inner_mut();
            inner_mut
                .user_tasks
                .get_mut(&user)
                .map(|task| task.now_running = Some((run_id, run_tx)));
            run_rx.then(move |res| {
                Ok(match res {
                    Ok(res) => Response {
                        success: true,
                        data: ResponseData::ScriptResult(res),
                    },
                    Err(_) => {
                        warn!(
                            "Notification task for user: {} dropped sender before returning \
                             result for script: {}",
                            user, script
                        );
                        Response {
                            success: false,
                            data: TIMEOUT_ERROR,
                        }
                    }
                })
            })
        }

        // Future that handles all user-specific commands(upload, download, delete, run).
        // It receives `Command`s from clients and executes them one at a time.
        let user_cmd_processor = extra_future::ForEach::new(cmd_rx, move |(tx, cmd)| {
            let shared = shared.clone();
            let user = user1.clone();
            ping_tx
                .clone()
                .send(())
                .map_err(|e| warn!("Failed to ping notification poller: {}", e))
                .and_then(|_| match cmd {
                    Command::UploadScript(name, path, props, args) => {
                        use hackmud_api::types::UpdateResponse;

                        let pool = shared.inner_ref().pool.clone();
                        let scripts_dir = format!("scripts/{}", user);
                        let script_file = format!("{}.js", name);
                        Box::new(pool.spawn_fn(move || {
                            path.map_or_else(
                                || {
                                    app_dirs2::app_dir(
                                        AppDataType::UserConfig,
                                        &HACKMUD_CLIENTD_APPINFO,
                                        &scripts_dir,
                                    ).map(|mut p| {
                                        p.push(script_file);
                                        p
                                    })
                                },
                                Ok,
                            ).and_then(|p| {
                                File::open(&p)
                                    .and_then(|mut f| {
                                        let mut s = String::new();
                                        if fs::metadata(&p)?.len() > SCRIPT_LIMIT {
                                            return Err(io::Error::new(
                                                io::ErrorKind::InvalidInput,
                                                format!(
                                                    "File at: {} execeeds the script limit of 100K",
                                                    p.display()
                                                ),
                                            ));
                                        }
                                        f.read_to_string(&mut s).map(|n| {
                                            trace!(
                                                "Script: uploading from: {} with size: {}",
                                                p.display(),
                                                n
                                            );
                                            (p, s)
                                        })
                                    })
                                    .map_err(|e| e.into())
                            })
                        }).then(move |res| match res {
                            Ok((path, code)) => if !code.is_empty() {
                                let c = &shared.inner_ref().hackmud_client;
                                let shared_inner = shared.clone();
                                shared.process_api_future(
                                    tx,
                                    c.update_script(user.clone(), name.clone(), args, code, props)
                                        .and_then(move |data| match data {
                                            UpdateResponse::UpdateStatus(status) => {
                                                assert_eq!(
                                                    status.success, false,
                                                    "UpdateStatus: Expected error message with \
                                                     success: false"
                                                );
                                                Either::A(future::ok(Response {
                                                    success: false,
                                                    data: ResponseData::Error(status.msg.into()),
                                                }))
                                            }
                                            UpdateResponse::UpdateResult(result) => {
                                                Either::A(future::ok(Response {
                                                    success: result.success,
                                                    data: ResponseData::UploadResult(result),
                                                }))
                                            }
                                            UpdateResponse::RunStatus(status) => {
                                                Either::B(script_result(
                                                    user,
                                                    name,
                                                    status.run_id,
                                                    &shared_inner,
                                                ))
                                            }
                                        }),
                                )
                            } else {
                                oneshot_send_response(
                                    tx,
                                    Response {
                                        success: false,
                                        data: ResponseData::Error(
                                            format!("Can't upload empty file: {}", path.display())
                                                .into(),
                                        ),
                                    },
                                );
                                Box::new(future::ok(()))
                            },
                            Err(e) => {
                                oneshot_send_response(
                                    tx,
                                    Response {
                                        success: false,
                                        data: ResponseData::Error(format!("{}", e).into()),
                                    },
                                );
                                Box::new(future::ok(()))
                            }
                        }))
                    }
                    Command::DeleteScript(name) => {
                        let c = &shared.inner_ref().hackmud_client;
                        shared.process_api_future(
                            tx,
                            c.delete_script(user, name).map(|status| Response {
                                success: status.success,
                                data: ResponseData::Message(status.msg.into()),
                            }),
                        )
                    }
                    Command::DownloadScript(name, path) => {
                        let c = &shared.inner_ref().hackmud_client;
                        let pool = shared.inner_ref().pool.clone();
                        let scripts_dir = format!("scripts/{}", user);
                        let script_file = format!("{}.remote.js", name);
                        shared.process_api_future(
                            tx,
                            c.download_script(user, name).and_then(move |resp| {
                                pool.spawn_fn(move || {
                                    path.map_or_else(
                                        || {
                                            app_dirs2::app_dir(
                                                AppDataType::UserConfig,
                                                &HACKMUD_CLIENTD_APPINFO,
                                                &scripts_dir,
                                            ).map(
                                                |mut p| {
                                                    p.push(script_file);
                                                    p
                                                },
                                            )
                                        },
                                        Ok,
                                    ).and_then(|p| {
                                        File::create(&p)
                                            .and_then(|mut f| {
                                                f.write_all(resp.js_prog.as_bytes())?;
                                                f.flush()?;
                                                trace!("Script downloaded to: {}", p.display());
                                                Ok(())
                                            })
                                            .map_err(|e| e.into())
                                    })
                                }).then(|res| {
                                    Ok(match res {
                                        Ok(()) => Response {
                                            success: true,
                                            data: ResponseData::Empty,
                                        },
                                        Err(e) => Response {
                                            success: false,
                                            data: ResponseData::Error(format!("{}", e).into()),
                                        },
                                    })
                                })
                            }),
                        )
                    }
                    Command::RunScript(name, args, cols) => {
                        let c = &shared.inner_ref().hackmud_client;
                        let shared_inner = shared.clone();
                        shared.process_api_future(
                            tx,
                            c.run_script(user.clone(), name.clone(), args, cols)
                                .and_then(move |status| {
                                    script_result(user, name, status.run_id, &shared_inner)
                                }),
                        )
                    }
                    cmd => unreachable!(
                        "User-command processor shouldn't receive command: {:?}",
                        cmd
                    ),
                })
        });

        // The state used by the poller running in a loop asynchornously.
        // `last_notif_t`, `last_cmd_t` is used to determine how long to sleep before polling again.
        // `last_active_t` is used to exit the user_task if no client is subscribed for a while.
        // `ping_fut` becomes ready when pinged by the command processor.
        struct PollState {
            last_notif_t: Instant,
            last_cmd_t: Instant,
            last_active_t: Instant,
            ping_fut: Option<StreamFuture<mpsc::Receiver<()>>>,
        }

        impl PollState {
            fn new(ping_rx: mpsc::Receiver<()>) -> Self {
                let now = Instant::now();
                Self {
                    last_notif_t: now,
                    last_cmd_t: now,
                    last_active_t: now,
                    ping_fut: Some(ping_rx.into_future()),
                }
            }
        }

        assert!(
            TIMER_TICK * 2 <= Duration::from_millis(500),
            "Minimum duration between poll should be greater than twice the timer tick"
        );

        let user1 = user.to_string();
        let shared = self.clone();
        let notification_poller = future::loop_fn(PollState::new(ping_rx), move |mut state| {
            // Ad-hoc sleep duration to not pressure the server while still being usable.
            fn next_sleep_time(state: &PollState) -> Duration {
                let cmd_elapsed = Instant::now().duration_since(state.last_cmd_t);
                let notif_elapsed = Instant::now().duration_since(state.last_notif_t);
                let active_elapsed = Instant::now().duration_since(state.last_active_t);
                let (minute, secs_20) = (Duration::from_secs(60), Duration::from_secs(20));
                if active_elapsed > minute * 2
                    || (cmd_elapsed > minute * 2 && notif_elapsed > minute * 2)
                {
                    secs_20
                } else if cmd_elapsed > minute && notif_elapsed > minute {
                    Duration::from_secs(10)
                } else if cmd_elapsed > secs_20 && notif_elapsed > secs_20 {
                    Duration::from_secs(5)
                } else if cmd_elapsed > Duration::from_secs(5) {
                    Duration::from_secs(1)
                } else {
                    Duration::from_millis(500)
                }
            }

            // It polls the given future sleeping for a max duration of `sleep_dur`.
            // It can be "woken up" when pinged by the command processor.
            fn with_sleep<F>(
                inner: &Inner,
                sleep_dur: Duration,
                fut: F,
                ping_fut: StreamFuture<mpsc::Receiver<()>>,
            ) -> Box<Future<Item = Loop<(), PollState>, Error = ()>>
            where
                F: future::IntoFuture<Item = PollState, Error = ()> + 'static,
            {
                trace!(
                    "Calculated sleep time: {}.{}",
                    sleep_dur.as_secs(),
                    sleep_dur.subsec_millis()
                );

                let sleep_fut = inner.timer.sleep(sleep_dur);
                Box::new(fut.into_future().and_then(move |mut state| {
                    let sleep_fut = if !sleep_fut.is_expired() {
                        let new_sleep_dur = next_sleep_time(&state);
                        let elapsed = sleep_dur - sleep_fut.remaining();
                        if elapsed >= new_sleep_dur {
                            Either::A(future::ok(()))
                        } else {
                            let sleep_time =
                                (new_sleep_dur - elapsed).max(Duration::from_millis(500));

                            trace!(
                                "Sleeping for a max duration of: {}.{}",
                                sleep_time.as_secs(),
                                sleep_time.subsec_millis()
                            );
                            Either::B(
                                sleep_fut
                                    .timer()
                                    .sleep(sleep_time)
                                    .map_err(|e| error!("Timer error: {} while polling", e)),
                            )
                        }
                    } else {
                        Either::A(future::ok(()))
                    };

                    sleep_fut
                        .select2(ping_fut)
                        .map(move |item| match item {
                            Either::A((_, ping_fut)) => Loop::Continue(PollState {
                                ping_fut: Some(ping_fut),
                                ..state
                            }),
                            Either::B(((item_opt, ping_rx), _)) => {
                                trace!("Sleep interrupted by new command");
                                if item_opt.is_some() {
                                    let now = Instant::now();
                                    state.last_cmd_t = now;
                                    state.last_active_t = now;
                                }
                                Loop::Continue(PollState {
                                    ping_fut: Some(ping_rx.into_future()),
                                    ..state
                                })
                            }
                        })
                        .map_err(|_| ())
                }))
            }

            // We set the `last_active_t`, if we have a client sending commands and/or subscribed.
            shared.inner_ref().user_tasks.get(&user1).map(|task| {
                if !task.subscribers.is_empty() {
                    state.last_active_t = Instant::now();
                }
            });

            // No activity for this user over some period means we stop this user_task.
            if Instant::now().duration_since(state.last_active_t) > Duration::from_secs(60) * 10 {
                return Box::new(future::ok(Loop::Break(())))
                    as Box<Future<Item = Loop<(), PollState>, Error = ()>>;
            }

            let ping_fut = state
                .ping_fut
                .take()
                .expect("ping channel receiver must be present while polling");
            let inner_ref = shared.inner_ref();
            if inner_ref.authenticated {
                let sleep_time = next_sleep_time(&state);

                let shared_poll = shared.clone();
                let user = user1.clone();
                let poll_timeout_fut = inner_ref.timer.timeout(
                    inner_ref
                        .hackmud_client
                        .poll(user.clone())
                        .map_err(ApiTimeoutError::Api),
                    REQUEST_TIMEOUT,
                );
                let poll_fut = poll_timeout_fut.then(move |res| match res {
                    Ok(notes) => {
                        let notifications: Vec<_> = notes
                            .into_iter()
                            .filter_map(|x| x)
                            .filter_map(|note| {
                                process_notification(note).unwrap_or_else(
                                    |(script_name, run_id, val)| {
                                        let mut tasks = RefMut::map(shared_poll.inner_mut(), |r| {
                                            &mut r.user_tasks
                                        });
                                        // XXX: can this ever fail?(in multithreaded executor?)
                                        let now_running = &mut tasks
                                            .get_mut(&user)
                                            .expect(&format!(
                                                "Usertask for user: {} must be present while \
                                                 polling",
                                                user
                                            ))
                                            .now_running;
                                        let result_done = now_running
                                            .as_ref()
                                            .map(|&(ref id, _)| run_id == id.as_str())
                                            .unwrap_or(false);

                                        if result_done {
                                            trace!(
                                                "Received ScriptResult: {:?} for id: {}",
                                                val,
                                                run_id
                                            );
                                            let result = ScriptResult {
                                                script_name,
                                                retval: val,
                                            };
                                            now_running.take().map(|(_, run_tx)| {
                                                run_tx.send(result).map_err(|result| {
                                                    warn!(
                                                        "Failed to send script result: {:?}",
                                                        result
                                                    )
                                                })
                                            });
                                        }
                                        None
                                    },
                                )
                            })
                            .collect();

                        let (mut last_notif_t, mut last_active_t) =
                            (state.last_notif_t, state.last_active_t);
                        let (ping_fut, last_cmd_t) = (state.ping_fut, state.last_cmd_t);

                        // We set `last_notif_t` when we have a non-empty list of notifications.
                        if notifications.is_empty() {
                            return Either::B(future::ok(PollState {
                                last_notif_t,
                                last_cmd_t,
                                last_active_t,
                                ping_fut,
                            }));
                        } else {
                            trace!("Received notifications: {:?}", notifications);
                            last_notif_t = Instant::now();
                        }

                        let json: Bytes = serde_json::to_vec(&notifications)
                            .expect("Json serialization mustn't fail")
                            .into();

                        // Broadcast to all the connected clients that enabled notification.
                        let senders: Vec<mpsc::Sender<Bytes>>;
                        {
                            let mut tasks =
                                RefMut::map(shared_poll.inner_mut(), |r| &mut r.user_tasks);
                            senders = tasks
                                .get_mut(&user)
                                .expect(&format!(
                                    "Usertask for user: {} must be present while polling",
                                    user
                                ))
                                .subscribers
                                .iter()
                                .filter_map(|(_, opt_tx)| opt_tx.as_ref().cloned())
                                .collect();
                        }
                        let fut = future::join_all(senders.into_iter().map(move |tx| {
                            tx.send(json.clone()).map(|_| ()).or_else(move |_| {
                                warn!("Failed to send notification to a client");
                                Ok(())
                            })
                        }));

                        Either::A(fut.map(move |_| PollState {
                            last_notif_t,
                            last_cmd_t,
                            last_active_t,
                            ping_fut,
                        }))
                    }
                    Err(e) => match e {
                        ApiTimeoutError::Timeout => {
                            error!("Request timed out while polling for user: {}", user);
                            Either::B(future::ok(state))
                        }
                        ApiTimeoutError::Api(e) => match e {
                            // We error out of the loop, only when a fatal error occurs.
                            ApiError::UnknownError(_)
                            | ApiError::JsonError(_)
                            | ApiError::VersionError(_) => {
                                error!("Fatal error: '{}' while polling for user: {}", e, user);
                                shared_poll.exit_server();
                                Either::B(future::err(()))
                            }
                            ApiError::RemoteError(s) => {
                                error!(
                                    "Failed to poll for user: {}, with remote error: {}, stopping \
                                     polling",
                                    user, s
                                );
                                Either::B(future::err(()))
                            }
                            // AuthError/HttpError is not fatal.
                            ApiError::AuthError(s) => {
                                error!(
                                    "Failed to poll for user: {}, with auth error: {}, stopping \
                                     polling",
                                    user, s
                                );
                                // Mark that we are now de-authenticated.
                                shared_poll.inner_mut().authenticated = false;
                                Either::B(future::ok(state))
                            }
                            ApiError::HttpError(e) => {
                                warn!("Failed to poll for user: {}, with http error: {}", user, e);
                                Either::B(future::ok(state))
                            }
                        },
                    },
                });
                with_sleep(&inner_ref, sleep_time, poll_fut, ping_fut)
            } else {
                trace!("Hackmud client not authenticated, Skipping poll and sleeping for a minute");
                with_sleep(&inner_ref, Duration::from_secs(20), Ok(state), ping_fut)
            }
        });

        let user_exit = user.to_string();
        // To stop polling on Termination events(somebody called SharedState::exit_server()).
        let exit_fut = self.inner_ref()
            .exit_signal
            .clone()
            .map(move |_| debug!("User-task for user: {} received exit signal", user_exit))
            .map_err(|_| ());

        let user_inner = user.to_string();
        let shared = self.clone();
        let final_fut = user_cmd_processor
            .select2(notification_poller)
            .then(move |res| {
                match res {
                    Ok(Either::B((_, mut cmd_fut))) | Err(Either::B((_, mut cmd_fut))) => {
                        cmd_fut.get_mut().close();
                        cmd_fut.cancel();
                        Either::A(cmd_fut)
                    }

                    Ok(Either::A(_)) => Either::B(future::ok(())),
                    Err(Either::A(_)) => Either::B(future::err(())),
                }.then(move |_| {
                    info!(
                        "User-task for user: {} exiting, removing from state",
                        user_inner
                    );
                    // If we have any live clients we set their user to None.
                    // This ensure live clients with retired users will need to set user.
                    // And they can't set to retired user anymore.
                    let alive: Vec<SocketAddr>;
                    {
                        let mut tasks = RefMut::map(shared.inner_mut(), |r| &mut r.user_tasks);
                        alive = tasks
                            .get_mut(&user_inner)
                            .expect("UserTask can't be empty after spawning one")
                            .subscribers
                            .keys()
                            .cloned()
                            .collect();
                    }
                    {
                        let mut clients = RefMut::map(shared.inner_mut(), |r| &mut r.clients);
                        for addr in alive {
                            clients.get_mut(&addr).map(|c| c.user.take());
                        }
                    }
                    // Remove ourselves when we exit, so a new one can be spawned.
                    shared.inner_mut().user_tasks.remove(&user_inner);
                    future::ok(())
                })
            })
            .select(exit_fut)
            .map(|_| ())
            .map_err(|_| ());

        info!("Spawning UserTask for user: {}", user);
        let handle = Some(oneshot::spawn(final_fut, &self.inner_ref().handle));
        self.inner_mut().user_tasks.insert(
            user.to_string(),
            UserTask {
                subscribers: HashMap::new(),
                cmd_tx,
                now_running: None,
                handle,
            },
        );
    }

    // Only produces error on fatal hackmud_api error and then signals termination.
    // All command processors(user/non-user) use this to process api futures for timeouts/errors
    // and then send the response to the connected client.
    fn process_api_future<F>(
        &self,
        tx: oneshot::Sender<Response>,
        fut: F,
    ) -> Box<Future<Item = (), Error = ()>>
    where
        F: Future<Item = Response, Error = ApiError> + 'static,
    {
        let shared = self.clone();
        Box::new(
            self.inner_ref()
                .timer
                .timeout(fut.map_err(ApiTimeoutError::Api), REQUEST_TIMEOUT)
                .then(move |res| match res {
                    Ok(val) => {
                        oneshot_send_response(tx, val);
                        future::ok(())
                    }
                    Err(err) => match err {
                        ApiTimeoutError::Timeout => {
                            oneshot_send_response(
                                tx,
                                Response {
                                    success: false,
                                    data: TIMEOUT_ERROR,
                                },
                            );
                            future::ok(())
                        }
                        ApiTimeoutError::Api(e) => {
                            let error = format!("{}", e).into();
                            match e {
                                ApiError::JsonError(_)
                                | ApiError::VersionError(_)
                                | ApiError::UnknownError(_) => {
                                    error!("Fatal error: '{}' while processing command", e);
                                    shared.exit_server();
                                    oneshot_send_response(
                                        tx,
                                        Response {
                                            success: false,
                                            data: ResponseData::Error(error),
                                        },
                                    );
                                    future::err(())
                                }
                                ApiError::AuthError(s) => {
                                    shared.inner_mut().authenticated = false;
                                    oneshot_send_response(
                                        tx,
                                        Response {
                                            success: false,
                                            data: ResponseData::AuthError(s.into()),
                                        },
                                    );
                                    future::ok(())
                                }
                                _ => {
                                    oneshot_send_response(
                                        tx,
                                        Response {
                                            success: false,
                                            data: ResponseData::Error(error),
                                        },
                                    );
                                    future::ok(())
                                }
                            }
                        }
                    },
                }),
        )
    }
}

// The closure state used in `for_each` by `Server::process_connection`.
struct CmdHandler {
    addr: SocketAddr,
    shared: SharedState,
}

impl CmdHandler {
    fn new(addr: &SocketAddr, shared: &SharedState) -> Self {
        Self {
            addr: *addr,
            shared: shared.clone(),
        }
    }

    // Runs a single command at a time until finished.
    fn process(
        &mut self,
        tx: mpsc::Sender<Bytes>,
        cmd: Command,
    ) -> Box<Future<Item = mpsc::Sender<Bytes>, Error = ()>> {
        // Sends the `cmd` as a `CmdMessage` to a (user/non-user)command processor and
        // waits until timeout or succeeds with a response.
        // The returning future errors when cmd_tx unavailable.
        fn run_cmd(
            cmd_tx: mpsc::Sender<CmdMessage>,
            cmd: Command,
        ) -> impl Future<Item = Response, Error = ()> {
            let (otx, orx) = oneshot::channel();
            cmd_tx
                .send((otx, cmd))
                .and_then(move |_| {
                    orx.then(|res| match res {
                        Ok(resp) => Ok(resp),
                        // Dropped by command processor.
                        Err(_) => Ok(Response {
                            success: false,
                            data: TIMEOUT_ERROR,
                        }),
                    })
                })
                .or_else(|_| {
                    error!("Fatal error: Non-user command channel send failure");
                    Err(())
                })
        }

        trace!("Received command: {:?} from client:({}) ", cmd, self.addr);
        match cmd {
            Command::ParseError(e) => mpsc_send_json(
                tx,
                Response {
                    success: false,
                    data: ResponseData::Error(e.into()),
                },
            ),
            Command::Help => mpsc_send_json(
                tx,
                Response {
                    success: true,
                    data: ResponseData::Message(HELP_STRING.into()),
                },
            ),
            Command::Status => {
                let inner_ref = self.shared.inner_ref();
                let client_state = inner_ref.clients.get(&self.addr).expect(&format!(
                    "Client: {} must exist in shared state while connection is active",
                    self.addr,
                ));

                mpsc_send_json(
                    tx,
                    Response {
                        success: true,
                        data: ResponseData::Status(ConnectionStatus {
                            authenticated: inner_ref.authenticated,
                            notifications: client_state.subscribed,
                            user: client_state.user.clone(),
                        }),
                    },
                )
            }
            Command::Exit => {
                // Exit command is a termination event.
                self.shared.exit_server();
                Box::new(future::err(()))
            }
            Command::AuthToken(s) => {
                self.shared.inner_mut().hackmud_client.set_auth_token(s);
                let shared = self.shared.clone();
                // We do a simple request to server to see if valid token.
                Box::new(
                    self.shared
                        .inner_mut()
                        .hackmud_client
                        .change_user("", false)
                        .then(move |res| {
                            let b = res.is_ok();
                            shared.inner_mut().authenticated = b;
                            mpsc_send_json(
                                tx,
                                Response {
                                    success: b,
                                    data: ResponseData::Empty,
                                },
                            )
                        }),
                )
            }
            cmd => {
                if !self.shared.inner_ref().authenticated {
                    return mpsc_send_json(
                        tx,
                        Response {
                            success: false,
                            data: ResponseData::AuthError(
                                "Hackmud client not authenticated. Please use \"#auth TOKEN\" \
                                 before issuing this command"
                                    .into(),
                            ),
                        },
                    );
                }
                match cmd {
                    Command::Help
                    | Command::Status
                    | Command::Exit
                    | Command::ParseError(_)
                    | Command::AuthToken(_) => {
                        unreachable!("Inner match should never process this command: {:?}", cmd)
                    }
                    Command::GetClientData
                    | Command::ListUsers
                    | Command::GetChatPass
                    | Command::RevokeChatToken
                    | Command::CreateUser(_)
                    | Command::RetireUser(_) => Box::new(
                        run_cmd(self.shared.inner_ref().non_user_cmd_tx.clone(), cmd)
                            .and_then(|resp| mpsc_send_json(tx, resp)),
                    ),
                    Command::SetUser(u) => {
                        let shared = self.shared.clone();
                        let addr = self.addr;
                        Box::new(
                            // Check if the user is valid for us.
                            run_cmd(
                                self.shared.inner_ref().non_user_cmd_tx.clone(),
                                Command::SetUser(u.clone()),
                            ).and_then(move |resp| {
                                if !resp.success {
                                    return mpsc_send_json(tx, resp);
                                }

                                // Some acrobatics required for borrow checker.
                                let sub: Option<
                                    mpsc::Sender<Bytes>,
                                >;
                                let old_user: Option<String>;
                                {
                                    let mut client_state = RefMut::map(shared.inner_mut(), |r| {
                                        r.clients.get_mut(&addr).expect(&format!(
                                            "Client: {} must exist in shared state while \
                                             connection is active",
                                            addr,
                                        ))
                                    });
                                    old_user = client_state.user.clone();
                                    client_state.user = Some(u.clone());

                                    // Preserve the subscription status.
                                    sub = if client_state.subscribed {
                                        Some(tx.clone())
                                    } else {
                                        None
                                    };
                                }

                                // Spawn the user task if it is not already running.
                                if !shared.inner_mut().user_tasks.contains_key(&u) {
                                    shared.create_user_task(&u);
                                }

                                // Remove this client from the old_user(if any) task.
                                let mut tasks =
                                    RefMut::map(shared.inner_mut(), |r| &mut r.user_tasks);
                                old_user.map(|s| {
                                    if s != u {
                                        tasks
                                            .get_mut(&s)
                                            .map(|task| task.subscribers.remove(&addr));
                                    }
                                });

                                if let Some(ref mut task) = tasks.get_mut(&u) {
                                    // Add to new user task.
                                    task.subscribers.insert(addr, sub);
                                    // send the response.
                                    mpsc_send_json(tx, resp)
                                } else {
                                    error!("task creation for user: {} failed, possible bug!!", u);
                                    mpsc_send_json(
                                        tx,
                                        Response {
                                            success: false,
                                            data: ResponseData::Error(
                                                format!("User: {} unavailable", u).into(),
                                            ),
                                        },
                                    )
                                }
                            }),
                        )
                    }
                    Command::EnableNotification(b) => {
                        let sub: Option<mpsc::Sender<Bytes>>;
                        let user: Option<String>;
                        {
                            let mut client_state = RefMut::map(self.shared.inner_mut(), |r| {
                                r.clients.get_mut(&self.addr).expect(&format!(
                                    "Client: {} must exist in shared state while connection is \
                                     active",
                                    self.addr,
                                ))
                            });
                            user = client_state.user.clone();
                            // Determine the subscription status.
                            sub = if b {
                                client_state.subscribed = true;
                                Some(tx.clone())
                            } else {
                                None
                            };
                        }
                        // If the connection has a user set already, add to the user_task.
                        if let Some(ref u) = user {
                            if !self.shared.inner_mut().user_tasks.contains_key(u) {
                                self.shared.create_user_task(u);
                            }
                            if let Some(ref mut task) =
                                self.shared.inner_mut().user_tasks.get_mut(u)
                            {
                                // Add to new user task.
                                task.subscribers.insert(self.addr, sub);
                            } else {
                                error!("task creation for user: {} failed, possible bug!!", u);
                                return mpsc_send_json(
                                    tx,
                                    Response {
                                        success: false,
                                        data: ResponseData::Error(
                                            format!("User: {} unavailable", u).into(),
                                        ),
                                    },
                                );
                            }
                        }
                        mpsc_send_json(
                            tx,
                            Response {
                                success: true,
                                data: ResponseData::Empty,
                            },
                        )
                    }
                    Command::UploadScript(_, _, _, _)
                    | Command::DownloadScript(_, _)
                    | Command::DeleteScript(_)
                    | Command::RunScript(_, _, _) => {
                        let user = self.shared
                            .inner_mut()
                            .clients
                            .get(&self.addr)
                            .expect(&format!(
                                "Client: {} must exist in shared state while connection is active",
                                self.addr,
                            ))
                            .user
                            .clone();
                        if let Some(ref u) = user {
                            // start the user_task if not already running.
                            if !self.shared.inner_mut().user_tasks.contains_key(u) {
                                self.shared.create_user_task(u);
                            }
                            let cmd_tx = self.shared
                                .inner_mut()
                                .user_tasks
                                .get_mut(u)
                                .map(|task| task.cmd_tx.clone());
                            if let Some(cmd_tx) = cmd_tx {
                                Box::new(
                                    // wait until the command processor finished processing our command.
                                    run_cmd(cmd_tx, cmd).and_then(|resp| mpsc_send_json(tx, resp)),
                                )
                            } else {
                                error!("task creation for user: {} failed, possible bug!!", u);
                                return mpsc_send_json(
                                    tx,
                                    Response {
                                        success: false,
                                        data: ResponseData::Error(
                                            format!("User: {} unavailable", u).into(),
                                        ),
                                    },
                                );
                            }
                        } else {
                            mpsc_send_json(
                                tx,
                                Response {
                                    success: false,
                                    data: ResponseData::Error(
                                        "User not set. Set user using \"#user USER\"".into(),
                                    ),
                                },
                            )
                        }
                    }
                }
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum LogOutput {
    Stdout,
    Stderr,
    LogDir,
}

#[derive(Debug)]
pub struct ServerConfig {
    addr: SocketAddr,
    log_config: fern::Dispatch,
}

impl ServerConfig {
    pub fn new(
        listen_addr: SocketAddr,
        log_level: LevelFilter,
        log_output: LogOutput,
    ) -> Result<Self, failure::Error> {
        let log_config = fern::Dispatch::new()
            .format(|out, message, record| {
                if let (Some(file), Some(line)) = (record.file(), record.line()) {
                    out.finish(format_args!(
                        "{} [{}: {}:{}][{}] {}",
                        format_rfc3339_seconds(SystemTime::now()),
                        record.target(),
                        file.rsplit('/').next().unwrap_or(""),
                        line,
                        record.level(),
                        message,
                    ))
                } else {
                    out.finish(format_args!(
                        "{} [{}][{}] {}",
                        format_rfc3339_seconds(SystemTime::now()),
                        record.target(),
                        record.level(),
                        message,
                    ))
                }
            })
            .chain::<fern::Output>(match log_output {
                LogOutput::Stdout => io::stdout().into(),
                LogOutput::Stderr => io::stderr().into(),
                LogOutput::LogDir => {
                    let log_dir = app_dirs2::app_dir(
                        AppDataType::UserConfig,
                        &HACKMUD_CLIENTD_APPINFO,
                        "logs",
                    ).map_err(|e| match e {
                        AppDirsError::Io(e) => e,
                        _ => io::Error::new(
                            io::ErrorKind::InvalidData,
                            "Default logging directory not available on this system",
                        ),
                    })?;
                    let (log_file, log_file_old) =
                        (log_dir.join("server.log"), log_dir.join("server.log.old"));
                    if fs::metadata(&log_file).map(|m| m.len()).unwrap_or(0) > LOGSIZE_LIMIT {
                        fs::rename(&log_file, &log_file_old)?
                    }
                    ::std::fs::OpenOptions::new()
                        .append(true)
                        .create(true)
                        .open(log_file)?
                        .into()
                }
            })
            .level(LevelFilter::Off)
            .level_for("hackmud_api", log_level)
            .level_for("hackmud_client", log_level);
        Ok(Self {
            addr: listen_addr,
            log_config,
        })
    }

    pub fn log_level_for<T: Into<Cow<'static, str>>>(self, module: T, level: LevelFilter) -> Self {
        Self {
            addr: self.addr,
            log_config: self.log_config.level_for(module, level),
        }
    }
}

impl Default for ServerConfig {
    fn default() -> Self {
        ServerConfig::new(
            DEFAULT_HACKMUD_CLIENTD_ADDR.parse().unwrap(),
            LevelFilter::Debug,
            LogOutput::Stderr,
        ).unwrap()
    }
}

pub struct Server {
    _lock_file: File,
    addr: SocketAddr,
    shared: SharedState,
    // immediately consumed when run() is called.
    non_user_cmd_rx: Option<mpsc::Receiver<(oneshot::Sender<Response>, Command)>>,
    reactor: RefCell<Core>,
}

impl Server {
    pub fn new(config: ServerConfig) -> Result<Server, failure::Error> {
        // Exclusive file lock to prevent other servers running simultaneously.
        let lock_file = fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(
                app_dirs2::app_root(AppDataType::UserConfig, &HACKMUD_CLIENTD_APPINFO)?
                    .join("server.lock"),
            )
            .with_context(|e| format!("Failed to create lock file: {}", e))?;
        lock_file
            .try_lock_exclusive()
            .with_context(|e| format!("Another server is already running: {}", e))?;

        config
            .log_config
            .apply()
            .with_context(|e| format!("Logger initialization error: {}", e))?;
        let core = Core::new()?;
        let executor = core.handle();
        let cert_path = std::env::var("DEBUG_CERT_PATH")
            .map(PathBuf::from)
            .or_else(|e| match e {
                std::env::VarError::NotUnicode(s) => Ok(PathBuf::from(s)),
                _ => Err(e),
            })
            .ok();
        let hackmud_client = if let Some(cert_path) = cert_path {
            ApiClient::debug_new(&executor, cert_path)
                .with_context(|e| format!("Failed to create hackmud client: {}", e))?
        } else {
            ApiClient::new(&executor)
        };
        let (cmd_tx, cmd_rx) = mpsc::channel(0);
        Ok(Server {
            addr: config.addr,
            _lock_file: lock_file,
            shared: SharedState::new(executor, hackmud_client, cmd_tx),
            non_user_cmd_rx: Some(cmd_rx),
            reactor: RefCell::new(core),
        })
    }

    pub fn run(mut self) -> Result<(), failure::Error> {
        info!("Starting the server.");
        let handle = self.shared.inner_ref().handle.clone();
        let cmd_rx = self.non_user_cmd_rx
            .take()
            .expect("non-user command receiver can't be empty");

        info!("Spawning non-user command processor.");
        let non_user_task_handle = oneshot::spawn(self.non_user_cmd_processor(cmd_rx), &handle);

        let (shared, shared_conn) = (self.shared.clone(), self.shared.clone());
        let listener = TcpListener::bind(&self.addr, &handle)?;
        let server = listener
            .incoming()
            .for_each(|(socket, _)| {
                let peer_addr = socket.peer_addr()?;
                shared_conn.inner_mut().clients.insert(
                    peer_addr,
                    ClientState {
                        user: None,
                        subscribed: false,
                        handle: None,
                    },
                );
                let handle = oneshot::spawn(self.process_connection(socket, peer_addr), &handle);
                shared_conn
                    .inner_mut()
                    .clients
                    .get_mut(&peer_addr)
                    .map(|c| c.handle = Some(handle));
                Ok(())
            })
            .select(
                // C-c gracefully shuts down the server.
                tokio_signal::ctrl_c(&handle)
                    .flatten_stream()
                    .into_future()
                    .map(move |_| {
                        info!("Received SIGINT");
                        shared.exit_server();
                    })
                    .map_err(|(e, _)| e),
            )
            .map(|_| ())
            .map_err(|(e, _)| e)
            .select(
                self.shared
                    .inner_ref()
                    .exit_signal
                    .clone()
                    .map(|_| ())
                    .map_err(|_| io::Error::new(io::ErrorKind::Other, "oneshot cancelled")),
            )
            .map_err(|(e, _)| e);

        let shared = self.shared.clone();
        self.reactor
            .borrow_mut()
            .run(server.then(move |res| {
                if let Err(e) = res {
                    error!("Server error: {}", e);
                }

                info!("Waiting for tasks to finish..");
                let tasks: Vec<_> = shared
                    .inner_mut()
                    .clients
                    .values_mut()
                    .filter_map(|c| c.handle.take())
                    .collect();

                let user_tasks: Vec<_> = shared
                    .inner_mut()
                    .user_tasks
                    .values_mut()
                    .filter_map(|u| u.handle.take())
                    .collect();
                stream::futures_unordered(
                    user_tasks
                        .into_iter()
                        .chain(tasks.into_iter())
                        .chain(vec![non_user_task_handle]),
                ).then(|res| {
                    // We don't want to stop if any future failed with an error.
                    res.or_else(|_| Ok(()))
                })
                    .for_each(|_| Ok(()))
                    .map_err(|_: ()| io::Error::new(io::ErrorKind::Other, "Unreachable!"))
            }))
            .with_context(|e| format!("Server run error: {}", e))?;
        info!("Exiting Server.\n");
        Ok(())
    }

    // Processes all non-user commands. Should be running for the entire lifetime of server.
    // Errors out only on termination event/fatal error.
    fn non_user_cmd_processor(
        &self,
        cmd_rx: mpsc::Receiver<CmdMessage>,
    ) -> impl Future<Item = (), Error = ()> {
        let shared = self.shared.clone();
        let exit_fut = shared
            .inner_ref()
            .exit_signal
            .clone()
            .map(|_| debug!("Non-user command processor received exit signal"))
            .map_err(|_| ());
        extra_future::ForEach::new(cmd_rx, move |(tx, cmd)| {
            let c = &shared.inner_ref().hackmud_client;
            match cmd {
                Command::GetClientData => shared.process_api_future(
                    tx,
                    c.client_data().map(|data| Response {
                        success: true,
                        data: ResponseData::ClientData(data),
                    }),
                ),
                Command::ListUsers => shared.process_api_future(
                    tx,
                    c.list_users().map(|info| Response {
                        success: true,
                        data: ResponseData::UserInfo(info),
                    }),
                ),
                Command::SetUser(u) => shared.process_api_future(
                    tx,
                    c.change_user(u, false).map(|status| Response {
                        success: status.available,
                        data: ResponseData::Message(status.msg.into()),
                    }),
                ),
                Command::GetChatPass => shared.process_api_future(
                    tx,
                    c.get_chat_pass().map(|pass| Response {
                        success: true,
                        data: ResponseData::Message(pass.pass.into()),
                    }),
                ),
                Command::RevokeChatToken => shared.process_api_future(
                    tx,
                    c.revoke_chat_token().map(|resp| Response {
                        success: resp.ok,
                        data: ResponseData::Empty,
                    }),
                ),
                Command::CreateUser(u) => shared.process_api_future(
                    tx,
                    c.change_user(u, true).map(|status| Response {
                        success: status.available,
                        data: ResponseData::Message(status.msg.into()),
                    }),
                ),
                Command::RetireUser(u) => shared.process_api_future(
                    tx,
                    c.retire_user(u).map(|status| Response {
                        success: status.success,
                        data: ResponseData::Message(status.msg.into()),
                    }),
                ),
                cmd => unreachable!(
                    "Non-user command processor shouldn't receive command: {:?}",
                    cmd
                ),
            }
        }).select2(exit_fut)
            .map_err(|_| ())
            .and_then(|val| match val {
                future::Either::B((_, mut fut)) => {
                    fut.get_mut().close();
                    fut.cancel();
                    future::Either::A(fut)
                }
                _ => future::Either::B(future::ok(())),
            })
    }

    // Reads `Command`s(using `ServerCodec`) underlying socket and
    // Writes `Notification`/`Response`s as json byte-strings.
    fn process_connection(
        &self,
        socket: TcpStream,
        peer_addr: SocketAddr,
    ) -> impl Future<Item = (), Error = ()> {
        use future::Either;

        socket.set_keepalive(Some(Duration::from_secs(5))).unwrap();

        let (wr, rd) = socket.framed(codec::ServerCodec::new()).split();
        // `Bytes` channel that is also used by notification poller of a user_task.
        let (tx, rx) = mpsc::channel(8);
        let mut cmd_handler = CmdHandler::new(&peer_addr, &self.shared);

        let exit_signal = self.shared.inner_ref().exit_signal.clone();
        // Delegates the processing of commands to `CmdHandler`.
        let reader = extra_future::Fold::new(
            rd.map_err(|e| warn!("socket read error: {}", e)),
            move |tx, cmd| cmd_handler.process(tx, cmd),
            tx.clone(),
        ).select2(exit_signal)
            .map_err(|_| ())
            .and_then(move |item| {
                let data = vec![
                    Notification {
                        timestamp: SystemTime::now()
                            .duration_since(UNIX_EPOCH)
                            .unwrap()
                            .as_secs(),
                        data: NotificationData::Exit,
                    },
                ];
                match item {
                    Either::B((_, mut fold_fut)) => {
                        debug!("Client:({}) received exit signal.", peer_addr);
                        fold_fut.cancel();
                        Either::A(fold_fut.then(|_| mpsc_send_json(tx, data).map(|_| ())))
                    }
                    _ => Either::B(mpsc_send_json(tx, data).map(|_| ())),
                }
            });

        // Forwards all incoming json encoded `Bytes` to the underlying sink(Encoder).
        let writer = rx.map_err(|_| {
            io::Error::new(
                io::ErrorKind::Other,
                "All senders dropped for the client writer",
            )
        }).forward(wr);

        let shared = self.shared.clone();
        reader
            .select2(writer)
            .then(move |res| match res {
                Ok(Either::A((_, mut wr_fut))) | Err(Either::A((_, mut wr_fut))) => {
                    // Close the underlying mpsc channel and process the queued items.
                    wr_fut
                        .stream_mut()
                        .map(|map_err_fut| map_err_fut.get_mut().close());
                    Either::A(
                        wr_fut
                            .map(|_| ())
                            .map_err(move |e| warn!("Client:({}) writer error: {}", peer_addr, e)),
                    )
                }
                Ok(Either::B(_)) => Either::B(future::ok(())),
                Err(Either::B((e, _))) => {
                    warn!("Client:({}) writer error: {}", peer_addr, e);
                    Either::B(future::err(()))
                }
            })
            .then(move |res| {
                // Remove the Sender if registered for notification.
                for task in shared.inner_mut().user_tasks.values_mut() {
                    task.subscribers.remove(&peer_addr);
                }
                // Remove from the alive clients list.
                shared.inner_mut().clients.remove(&peer_addr);
                debug!(
                    "Client:({}) exiting, removed from internal state",
                    peer_addr
                );
                res
            })
    }
}
