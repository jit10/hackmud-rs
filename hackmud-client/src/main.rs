extern crate failure;
extern crate hackmud_client;
extern crate log;

use std::env;
use std::process;
use std::net::SocketAddr;
use failure::Error;
use log::LevelFilter;
use hackmud_client::{LogOutput, Server, ServerConfig, DEFAULT_HACKMUD_CLIENTD_ADDR};

fn run() -> Result<(), Error> {
    let addr: SocketAddr = if let Some(arg1) = env::args().nth(1) {
        match arg1.as_str() {
            "-h" | "--help" => {
                println!(
                    r"Usage:
  hackmud-clientd [addr:proto] => listen on the given socket address/default address({})
  -h | --help => Print this message.
  -v | --version => Print the current version",
                    DEFAULT_HACKMUD_CLIENTD_ADDR
                );
                return Ok(());
            }
            "-v" | "--version" => {
                println!(
                    r"hackmud-clientd v{}",
                    option_env!("CARGO_PKG_VERSION").unwrap_or("unknown")
                );
                return Ok(());
            }
            _ => arg1.parse().ok().unwrap_or_else(|| {
                eprintln!(
                    "Invalid socket address specified, using default address: {}",
                    DEFAULT_HACKMUD_CLIENTD_ADDR
                );
                DEFAULT_HACKMUD_CLIENTD_ADDR.parse().unwrap()
            }),
        }
    } else {
        DEFAULT_HACKMUD_CLIENTD_ADDR.parse().unwrap()
    };

    let config = ServerConfig::new(addr, LevelFilter::Debug, LogOutput::LogDir)?;
    Server::new(config)?.run()
}

fn main() {
    match run() {
        Ok(_) => {
            process::exit(0);
        }
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    }
}
